# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import os.path
import sys
import logging


# Add testing solution sources paths
here = os.path.abspath(os.path.dirname(__file__))
holado_path = os.path.normpath(os.path.join(here, "..", "..", ".."))
sys.path.append( os.path.join(holado_path, "src") )
sys.path.append( os.path.join(holado_path, "tests", "behave") )

# Configure HolAdo
import holado
# Initialize HolAdo:
#    - log_level is set to INFO for initialization phase, it will be overwrite by level in logging config file
#    - log_on_console is True for initialization phase, it will be set to False when root log file will be defined
#    - logging config file
holado.initialize(logging_config_file=os.path.join(here, 'logging.conf'), log_level=logging.INFO, log_on_console=True)


# Import resources compiled Protobuf and gRPC packages
from holado.common.context.session_context import SessionContext
proto_gene_path = os.path.join(here, "resources", "proto", "generated")
SessionContext.instance().protobuf_messages.import_all_compiled_proto(os.path.join(proto_gene_path, "protobuf"))
# SessionContext.instance().grpc_services.import_all_compiled_proto(os.path.join(proto_gene_path, "proto-int"))



# Import generic environment methods
from holado_test.behave.behave_environment import *  # @UnusedWildImport

# Define project specific environment methods

