import os
import sys
import shutil

here = os.path.abspath(os.path.dirname(__file__))

test_path = os.path.join(here, "..", "..")
holado_path = os.path.join(here, "..", "..", "..")
sys.path.insert(0, os.path.join(holado_path, "src"))


from holado_protobuf.ipc.protobuf.protobuf_compiler import ProtobufCompiler
from holado_grpc.ipc.rpc.grpc_compiler import GRpcCompiler

def compile_protobuf_proto(remove_destination=True):
    # Define protoc path
#    protoc_exe_path = os.path.join(tsp_path, "dependencies", "protoc", "protoc-3.17.3-linux-x86_64", "bin", "protoc")
    protoc_exe_path = os.path.join(holado_path, "dependencies", "protoc", "protoc-21.5-linux-x86_64", "bin", "protoc")
    # protoc_exe_path = os.path.join(tsp_path, "dependencies", "protoc", "protoc-21.12-linux-x86_64", "bin", "protoc")
    
    # Define protobuf folder
    resources_proto_path = os.path.normpath(os.path.join(test_path, "resources", "proto"))
    
    # Define proto and generated paths
    proto_path = os.path.join(resources_proto_path, "definitions")
    destination_path = os.path.join(resources_proto_path, "generated")
    
    # Remove existing destination
    if remove_destination and os.path.exists(destination_path) and os.path.isdir(destination_path):
        shutil.rmtree(destination_path)
    
    protoc = ProtobufCompiler()
    protoc.protoc_exe_path = protoc_exe_path
    
    protoc.register_proto_path(os.path.join(proto_path, "protobuf"), os.path.join(destination_path, "protobuf"), os.path.join(proto_path, "protobuf", "tutorial"))
    protoc.register_proto_path(os.path.join(proto_path, "protobuf"), os.path.join(destination_path, "protobuf"), os.path.join(proto_path, "protobuf", "custom_types"))
    
    protoc.compile_all_proto()

def compile_grpc_proto(remove_destination=True):
    # Define protobuf folder
    resources_proto_path = os.path.normpath(os.path.join(test_path, "resources", "proto"))
    
    # Define proto and generated paths
    proto_path = os.path.join(resources_proto_path, "definitions")
    destination_path = os.path.join(resources_proto_path, "generated")
    
    # Remove existing destination
    if remove_destination and os.path.exists(destination_path) and os.path.isdir(destination_path):
        shutil.rmtree(destination_path)
    
    protoc = GRpcCompiler()
    
    protoc.register_proto_path(os.path.join(proto_path, "xxx"), os.path.join(destination_path, "xxx"), os.path.join(proto_path, "xxx", "rpc"))
    
    protoc.compile_all_proto()

if __name__ == "__main__":
    compile_protobuf_proto()
    #compile_grpc_proto(remove_destination=False)
    
    
