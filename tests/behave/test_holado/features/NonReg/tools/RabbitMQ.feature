@testing_solution
@rabbitmq
Feature: Test RabbitMQ module

    @go_nogo
    Scenario: Simple queue and one message

        ### PRECONDITIONS - BEGIN
        Given begin preconditions
        
        Given SERVER = new RabbitMQ server
        When run as docker the RabbitMQ server SERVER on ports (5673, 15673)

        Given CLIENT_1 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_1)
            | Name          | Value       |
            | 'host'        | 'localhost' |
            | 'port'        | 5673        |
        
        Given CLIENT_2 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_2)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given end preconditions
        ### PRECONDITIONS - END
        
        Given PUBLISHER = new publisher on queue 'test' (RMQ client: CLIENT_1)
        
        Given CONSUMER = new buffer consumer on queue 'test' (RMQ client: CLIENT_2)
        
        Given THREAD_ID = call steps in a thread
            """
            When start consuming (RMQ client: CLIENT_2)
            """
        #Given for thread THREAD_ID, call steps for interrupt
            #"""
            #When stop consuming (RMQ client: CLIENT_2)
            #"""
        
        When publish 'test message' (RMQ publisher: PUBLISHER)
        
        When wait consumer CONSUMER stops to receive messages (first timeout: 0.1 s)
        Then consumer CONSUMER received 1 messages:
            | 'test message' |
            

    Scenario: Simple queue and multiple messages

        ### PRECONDITIONS - BEGIN
        Given begin preconditions
        
        Given SERVER = new RabbitMQ server
        When run as docker the RabbitMQ server SERVER on ports (5673, 15673)

        Given CLIENT_1 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_1)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given CLIENT_2 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_2)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given end preconditions
        ### PRECONDITIONS - END
        
        Given PUBLISHER = new publisher on queue 'test' (RMQ client: CLIENT_1)
        
        Given CONSUMER = new buffer consumer on queue 'test' (RMQ client: CLIENT_2)
        
        Given THREAD_ID = call steps in a thread
            """
            When start consuming (RMQ client: CLIENT_2)
            """
        #Given for thread THREAD_ID, call steps for interrupt
            #"""
            #When stop consuming (RMQ client: CLIENT_2)
            #"""
        
        When publish 'test message 1' (RMQ publisher: PUBLISHER)
        When publish 'test message 2' (RMQ publisher: PUBLISHER)
        When publish 'test message 3' (RMQ publisher: PUBLISHER)
        When publish 'test message 4' (RMQ publisher: PUBLISHER)
        When publish 'test message 5' (RMQ publisher: PUBLISHER)
        
        When wait consumer CONSUMER stops to receive messages (first timeout: 0.1 s)
        Then consumer CONSUMER received 5 messages:
            | 'test message 1' |
            | 'test message 2' |
            | 'test message 3' |
            | 'test message 4' |
            | 'test message 5' |

            
    Scenario: Queue + Exchange

        ### PRECONDITIONS - BEGIN
        Given begin preconditions
        
        Given SERVER = new RabbitMQ server
        When run as docker the RabbitMQ server SERVER on ports (5673, 15673)

        Given CLIENT_1 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_1)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given CLIENT_2 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_2)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given end preconditions
        ### PRECONDITIONS - END
        
        Given PUBLISHER = new publisher (RMQ client: CLIENT_1)
            | Name                      | Value           |
            | 'queue.name'              | 'test'          |
            | 'exchange.name'           | 'test_exchange' |
            | 'exchange.exchange_type'  | 'topic'         |
        
        Given CONSUMER = new buffer consumer (RMQ client: CLIENT_2)
            | Name                      | Value           |
            | 'queue.name'              | 'test'          |
            | 'exchange.name'           | 'test_exchange' |
            | 'exchange.exchange_type'  | 'topic'         |
        
        Given THREAD_ID = call steps in a thread
            """
            When start consuming (RMQ client: CLIENT_2)
            """
        #Given for thread THREAD_ID, call steps for interrupt
            #"""
            #When stop consuming (RMQ client: CLIENT_2)
            #"""
        
        When publish 'test message 1' (RMQ publisher: PUBLISHER)
        When publish 'test message 2' (RMQ publisher: PUBLISHER)
        When publish 'test message 3' (RMQ publisher: PUBLISHER)
        When publish 'test message 4' (RMQ publisher: PUBLISHER)
        When publish 'test message 5' (RMQ publisher: PUBLISHER)
        
        When wait consumer CONSUMER stops to receive messages (first timeout: 0.1 s)
        Then consumer CONSUMER received 5 messages:
            | 'test message 1' |
            | 'test message 2' |
            | 'test message 3' |
            | 'test message 4' |
            | 'test message 5' |

                        
    Scenario: Queue + Exchange + Routing key

        ### PRECONDITIONS - BEGIN
        Given begin preconditions
        
        Given SERVER = new RabbitMQ server
        When run as docker the RabbitMQ server SERVER on ports (5673, 15673)

        Given CLIENT_1 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_1)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given CLIENT_2 = new RabbitMQ client with rapid close
        When connect with a blocking connection (RMQ client: CLIENT_2)
            | Name          | Value       |
            | 'port'        | 5673        |
        
        Given end preconditions
        ### PRECONDITIONS - END
        
        Given PUBLISHER_1 = new publisher (RMQ client: CLIENT_1)
            | Name                      | Value           |
            | 'queue.name'              | 'test'          |
            | 'exchange.name'           | 'test_exchange' |
            | 'exchange.exchange_type'  | 'topic'         |
            | 'routing_key'             | 'test_rk'       |
        
        Given PUBLISHER_2 = new publisher (RMQ client: CLIENT_1)
            | Name                      | Value           |
            | 'queue.name'              | 'test'          |
            | 'exchange.name'           | 'test_exchange' |
            | 'exchange.exchange_type'  | 'topic'         |
            | 'routing_key'             | 'test_rk_tmp'   |
        
        Given CONSUMER = new buffer consumer (RMQ client: CLIENT_2)
            | Name                      | Value           |
            | 'queue.name'              | 'test'          |
            | 'exchange.name'           | 'test_exchange' |
            | 'exchange.exchange_type'  | 'topic'         |
            | 'bind.routing_key'        | 'test_rk'       |
        
        Given THREAD_ID = call steps in a thread
            """
            When start consuming (RMQ client: CLIENT_2)
            """
        #Given for thread THREAD_ID, call steps for interrupt
            #"""
            #When stop consuming (RMQ client: CLIENT_2)
            #"""

        When publish 'test message tmp 1' (RMQ publisher: PUBLISHER_2)
        
        When publish 'test message 1' (RMQ publisher: PUBLISHER_1)

        When publish 'test message tmp 2' (RMQ publisher: PUBLISHER_2)

        When publish 'test message 2' (RMQ publisher: PUBLISHER_1)
        When publish 'test message 3' (RMQ publisher: PUBLISHER_1)
        When publish 'test message 4' (RMQ publisher: PUBLISHER_1)
        When publish 'test message 5' (RMQ publisher: PUBLISHER_1)

        When publish 'test message tmp 3' (RMQ publisher: PUBLISHER_2)
        
        When wait consumer CONSUMER stops to receive messages (first timeout: 0.1 s)
        Then consumer CONSUMER received 5 messages:
            | 'test message 1' |
            | 'test message 2' |
            | 'test message 3' |
            | 'test message 4' |
            | 'test message 5' |
            