@testing_solution
Feature: Steps in common_steps.py

    Scenario: wait time

        When wait 1 seconds
        
        When wait 0.55 seconds
        

    @wait_until
    Scenario: wait until

        When wait until '1' == '1' (accepted time: 1 s)
        When wait until '1' == '1' (accepted time: 1 s ; timeout: 2 s)
        When wait until '1' == '1' (accepted time: 1 s ; polling: 0.1 s)
        When wait until '1' == '1' (accepted time: 1 s ; timeout: 2 s ; polling: 0.1 s)
        
        Given next step shall fail on exception matching 'holado_core.common.exceptions.timeout_exception.TimeoutException\(Timeout \(2\.0 s\) when waiting '1' != '1''
        When wait until '1' != '1' (accepted time: 1 s ; timeout: 2 s)
        
        Given next step shall fail on exception matching 'holado_core.common.exceptions.functional_exception.FunctionalException\(Too long \(1.1\d+ s\) to wait wait 1.1 seconds \(accepted time: 1.0 s\)\)'
        When wait until wait 1.1 seconds (accepted time: 1 s ; timeout: 2 s)
        
        Given next step shall fail on exception matching 'A step doesn't exist in'
        When wait until missing step (accepted time: 1 s ; timeout: 2 s)

    Scenario: match pattern
    
        Then 'test (comment)' matches pattern 'est'
        Then 'test (comment)' matches pattern 'est \('
        Then 'test (comment)' matches pattern 'est \\('
        
        Then 'test (comment)\nauto' matches pattern '\nauto'
        Then 'test (comment)\nauto' matches pattern '\)\nauto'
        Then 'test (comment)\nauto' matches pattern '\\)\nauto'
    
