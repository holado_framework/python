@testing_solution
@grpc
Feature: Test gRPC module

    @go_nogo
    @need_update
    Scenario: Simple request

        ### PRECONDITIONS - BEGIN
        Given begin preconditions

        Given SERVER = start internal gRPC server
        When wait 1 seconds

        Given CLIENT = new internal gRPC client on service 'account.UserController'

        Given end preconditions
        ### PRECONDITIONS - END

        Given SERVICE_NAMES = service names (gRPC client: CLIENT)
        Then SERVICE_NAMES is list
            | 'account.UserController' |

        When RESULT = request 'account.UserController.List' (gRPC client: CLIENT)

        When TABLE = convert json RESULT to table with names as columns recursively
        Then table TABLE is
            | email           | groups | id  | username    |
            | 'auto@test.com' | []     | N/A | 'test_user' |


