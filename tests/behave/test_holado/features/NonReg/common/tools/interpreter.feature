@testing_solution
@TextInterpreter
Feature: Features related to TextInterpreter

    @HexToBytes
    Scenario: function HexToBytes

        Given HEX = '01569F'
        Given VALUE_1 = ${HexToBytes(HEX)}
        Then VALUE_1 == b'\x01\x56\x9F'

        Given VALUE_2 = ${HexToBytes('FFFF')}
        Then VALUE_2 == b'\xFF\xFF'

    @HexToInt
    Scenario: function HexToInt
        
        Given HEX = '040A'
        Given VALUE = ${HexToInt(HEX)}
        Then VALUE == 1034
        
        Given VALUE = ${HexToInt(040A)}
        Then VALUE == 1034
        
        Given VALUE = ${HexToInt('040A')}
        Then VALUE == 1034
        
    @MatchPattern
    Scenario: function MatchPattern
        
        Given TEXT = 'Hello TOTO !'
        Then TEXT == '${MatchPattern(.*TOTO.*)}'
        
        Then TEXT == '${MatchPattern([^ ]+ (?P<NAME>\w+).*)}'
        Then NAME == 'TOTO'
        
        
        Given TEXT = 'Hello !\nHow are you TOTO ?'
        
        Given next step shall fail on exception matching '.*holado_core.common.exceptions.verify_failure_exception.VerifyFailureException\(Match failure, value doesn't match pattern.*'
        Then TEXT == '${MatchPattern(.*(?P<NAME>\w+) \?)}'
        
        Then TEXT == '${MatchPattern(.*?(?P<NAME>\w+) \?, re.DOTALL)}'
        Then NAME == 'TOTO'
        
        
    @ToHex
    Scenario: function ToHex
        
        Given VALUE = 1034
        Given HEX = '${ToHex(VALUE)}'
        Then HEX == '040A'
        Given HEX = '${ToHex(VALUE, False)}'
        Then HEX == '040a'
        
        Given VALUE = 'TEST'
        Given HEX = '${ToHex(VALUE)}'
        Then HEX == '54455354'

        Given VALUE = b'\xF1\x4A'
        Given HEX = '${ToHex(VALUE)}'
        Then HEX == 'F14A'
        
        
        
        
        
        
