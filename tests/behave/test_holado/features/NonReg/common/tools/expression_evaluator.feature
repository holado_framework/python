@testing_solution
@expression_evaluator
Feature: Features related to expression_evaluator

    Scenario: Expressions in tables

        Given VAL_5 = 5
        Given VAL_9 = 9
        Given VAL_10 = 10
        Given VAL_11 = 11
        Given VAL_20 = 20
        
        #Given TABLE = table
            #| Name      | Value   |
            #| 'name'    | VAL     |
            #| 'name +'  | VAL + 1 |
            #| 'name -'  | VAL - 1 |
        Given TABLE = convert json '{"name": 10, "name +": 11, "name -": 9}' to name/value table with names uncollapsed

        Then table TABLE is
            | Name      | Value |
            | 'name'    | 10    |
            | 'name +'  | 11    |
            | 'name -'  | 9     |

        Then table TABLE is
            | Name      | Value       |
            | 'name'    | VAL_10      |
            | 'name +'  | ${VAL_10} + 1  |
            | 'name -'  | ${VAL_10} - 1  |

        Then table TABLE is
            | Name      | Value       |
            | 'name'    | ${VAL_9} + 1   |
            | 'name +'  | ${VAL_9} + 2   |
            | 'name -'  | ${VAL_11} - 2  |

        Then table TABLE is
            | Name      | Value         |
            | 'name'    | ${VAL_20} / 2    |
            | 'name +'  | ${VAL_5} * 2 + 1 |
            | 'name -'  | ${VAL_5} * 2 - 1 |
            
            
    Scenario: Expressions in assignment

        Given VAL = ${int(32)}
        Then VAL == 32

        Given VAL = ${32}
        Then VAL == 32

        Given VAL = ${128} >> 2
        Then VAL == 32
        
        Given X = 128
        Given VAL = ${X} >> 2 if ${X} >= 64 else ${X} >> 1
        Then VAL == 32
        
        Given X = 32
        Given VAL = ${X} >> 2 if ${X} >= 64 else ${X} >> 1
        Then VAL == 16
        
        Given X = 32
        Given VAL = ${X} + 1
        Then VAL == 33
        
        
    @timedelta
    Scenario: timedelta
        ### PRECONDITIONS - BEGIN
        Given begin preconditions

        Given DT_1 = datetime now
        Given DT_2 = DT_1 + 3 seconds
        Given EXPECTED = ${datetime.timedelta(seconds=3)}
        
        Given end preconditions
        ### PRECONDITIONS - END
        
        Given DELTA = ${DT_2} - ${DT_1}
        Then DELTA == EXPECTED

    Scenario: Expressions in for and if
        ### PRECONDITIONS - BEGIN
        Given begin preconditions

        Given VAL_5 = 5
        Given VAL_9 = 9
        Given VAL_10 = 10
        Given VAL_11 = 11
        Given VAL_20 = 20
        
        Given TABLE = convert json '{"name": 10, "name +": 11, "name -": 9}' to name/value table with names uncollapsed
        
        Given end preconditions
        ### PRECONDITIONS - END

        Given for N in range(0,2):
            Given VAL_5_1 = ${VAL_5} - 1
            Given VAL_5_N = ${VAL_5} - ${N}
            
            Then VAL_5_1 == 4
            Then table TABLE is
                | Name      | Value       |
                | 'name'    | VAL_10      |
                | 'name +'  | ${VAL_10} + 1  |
                | 'name -'  | ${VAL_10} - 1  |
                
            Given if ${N} == 0:
                Then VAL_5_N == 5
                
                Then table TABLE is
                    | Name      | Value       |
                    | 'name'    | VAL_10      |
                    | 'name +'  | ${VAL_10} + 1  |
                    | 'name -'  | ${VAL_10} - 1  |
            Given else:
                Then VAL_5_N == 4
                
                Then table TABLE is
                    | Name      | Value       |
                    | 'name'    | VAL_10      |
                    | 'name +'  | ${VAL_10} + ${N} |
                    | 'name -'  | ${VAL_10} - ${N} |
            Given end if
        Given end for
        
        