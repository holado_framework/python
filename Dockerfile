# Install requirements
#FROM python:3.8 AS build
#FROM python:3.8-alpine3.13 AS build
FROM python:3.11-alpine3.17 AS build

COPY requirements_docker.txt .

RUN apk update \
    && apk --no-cache --update add build-base \
    && apk --update add alpine-sdk \
    && apk add libffi-dev \
    && apk add postgresql-dev gcc python3-dev musl-dev \
    && pip install --user -r requirements_docker.txt


# Install sources
#FROM python:3.8-slim
#FROM python:3.8-alpine3.13 AS runtime
FROM python:3.11-alpine3.17 AS runtime

# Copy from build
#COPY --from=build /root/.local /root/.local
#ENV PATH=/root/.local:/root/.local/bin:$PATH
COPY --from=build /root/.local /home/user/.local
RUN chmod -R 755 /home/user
ENV PYTHONUSERBASE=/home/user/.local
ENV PATH=/home/user/.local:/home/user/.local/bin:$PATH

# Add libraries
RUN apk update \
    && apk --no-cache add libstdc++ \
    && apk --no-cache add postgresql-dev gcc python3-dev musl-dev

# Add tools
RUN apk --no-cache add bash \
    && apk --no-cache add nano \
    && apk --no-cache add curl

WORKDIR /code

COPY . ./testing-solution/python

