# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_value.common.tables.comparators.table_2_value_table_comparator import Table2ValueTable_Comparator
from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_rabbitmq.tools.rabbitmq import rabbitmq_client
from holado_core.common.tables.table_manager import TableManager
from holado_value.common.tables.value_table_manager import ValueTableManager
from holado_core.common.exceptions.functional_exception import FunctionalException
import logging
from holado_core.common.threading.threadsmanager import ThreadsManager
from holado_core.common.tools.tools import Tools
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


if rabbitmq_client.RMQClient.is_available():

    def __get_session_context():
        return SessionContext.instance()
    
    def __get_scenario_context():
        return __get_session_context().get_scenario_context()
    
    def __get_text_interpreter():
        return __get_scenario_context().get_text_interpreter()
    
    def __get_variable_manager():
        return __get_scenario_context().get_variable_manager()
    
    def __get_protobuf_messages():
        return SessionContext.instance().protobuf_messages
    
    
    def __convert_step_args_2_RabbitMQClient_kwargs(step_args):
        res = {}
        args = dict(step_args)
        if 'queue.name' in args:
            res['queue'] = args.pop('queue.name')
        if Tools.has_sub_kwargs(args, 'queue.'):
            res['queue_args'] = Tools.pop_sub_kwargs(args, 'queue.')
        if 'exchange.name' in args:
            res['exchange'] = args.pop('exchange.name')
        if Tools.has_sub_kwargs(args, 'exchange.'):
            res['exchange_args'] = Tools.pop_sub_kwargs(args, 'exchange.')
        if Tools.has_sub_kwargs(args, 'bind.'):
            res['bind_args'] = Tools.pop_sub_kwargs(args, 'bind.')
        res.update(args)
        return res
    
    
    @Given(u"(?P<var_name>\w+) = new RabbitMQ client")
    def step_impl(context, var_name):
        obj = rabbitmq_client.RMQClient()
        __get_variable_manager().register_variable(var_name, obj)
    
    @Given(u"(?P<var_name>\w+) = new RabbitMQ client with rapid close")
    def step_impl(context, var_name):
        obj = rabbitmq_client.RMQClient()
        obj.rapide_close = True
        __get_variable_manager().register_variable(var_name, obj)
    
    @When(u"connect with a blocking connection \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_client):
        """
        Create a new connection to RabbitMQ Client. Connection parameters are defined in a table.
        Credentials:
        To define connection credentials, define username and password values
        """

        if hasattr(context, "table"):
            table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
            connection_parameters_kwargs = ValueTableManager.convert_name_value_table_2_dict(table)
        else:
            connection_parameters_kwargs = {}
        
        client = __get_variable_manager().get_variable_value(var_client)
        client.connect_blocking(**connection_parameters_kwargs)
    
    @Given(u"(?P<var_name>\w+) = new publisher on queue (?P<queue>'[^']*'%?|[^' ]+) \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, queue, var_client):
        queue = StepTools.evaluate_scenario_parameter(queue)
        execute_steps("""
            Given {} = new publisher (RMQ client: {})
                | Name         | Value |
                | 'queue.name' | '{}'  |
            """.format(var_name, var_client, queue) )
    
    @Given(u"(?P<var_name>\w+) = new publisher \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, var_client):
        table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
        args = ValueTableManager.convert_name_value_table_2_dict(table)
        kwargs = __convert_step_args_2_RabbitMQClient_kwargs(args)
        
        client = __get_variable_manager().get_variable_value(var_client)
        pub = client.new_publisher(**kwargs)
        
        __get_variable_manager().register_variable(var_name, pub)
    
    @Given(u"(?P<var_name>\w+) = new buffer consumer on queue (?P<queue>'[^']*'%?|[^' ]+) \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, queue, var_client):
        queue = StepTools.evaluate_scenario_parameter(queue)
        execute_steps("""
            Given {} = new buffer consumer (RMQ client: {})
                | Name         | Value |
                | 'queue.name' | '{}'  |
            """.format(var_name, var_client, queue) )
    
    @Given(u"(?P<var_name>\w+) = new buffer consumer \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, var_client):
        table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
        args = ValueTableManager.convert_name_value_table_2_dict(table)
        kwargs = __convert_step_args_2_RabbitMQClient_kwargs(args)
        
        client = __get_variable_manager().get_variable_value(var_client)
        con = client.new_buffer_consumer(**kwargs)
        
        __get_variable_manager().register_variable(var_name, con)
    
    @Given(u"start consuming in a thread \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_client):
        # execute_steps(u"""
        #     Given __THREAD_ID_CONSUMING__ = start consuming in a thread (RMQ client: {var_client})
        #     Given wait until client is consuming (RMQ client: {var_client}) (accepted time: 20 s)
        #     Given for thread __THREAD_ID_CONSUMING__, call steps for interrupt
        #         \"""
        #         When stop consuming (RMQ client: {var_client})
        #         \"""
        #     """.format(var_client=var_client))
        
        # The wait of 0.01 seconds is done to avoid the case where two execute_steps are done in the same time in two different threads
        execute_steps(u"""
            Given __THREAD_ID_CONSUMING__ = start consuming in a thread (RMQ client: {var_client})
            Given wait 0.01 seconds
            Given wait until client is consuming (RMQ client: {var_client}) (accepted time: 5 s)
            """.format(var_client=var_client))
    
    @Given(u"(?P<var_name>\w+) = start consuming in a thread \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, var_client):
        # Start consuming in a small delay
        execute_steps(u"""
            Given {var_name} = call steps in a thread
                \"""
                When start consuming (RMQ client: {var_client})
                \"""
            """.format(var_name=var_name, var_client=var_client))
        
        # Register the thread allowed to start consuming 
        thread_name = __get_variable_manager().get_variable_value(var_name)
        thread_id = ThreadsManager.get_thread(thread_name).ident
        
        client = __get_variable_manager().get_variable_value(var_client)
        client.consuming_thread_id = thread_id
    
    @When(u"start consuming \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_client):
        client = __get_variable_manager().get_variable_value(var_client)
        client.start_consuming()
    
    @Then(u"client is consuming \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_client):
        client = __get_variable_manager().get_variable_value(var_client)
        if not client.is_consuming:
            raise FunctionalException(f"RMQ client is not consuming")
        
    @When(u"stop consuming \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, var_client):
        client = __get_variable_manager().get_variable_value(var_client)
        client.stop_consuming()
    
    @When(u"publish (?P<msg>'[^']*'%?|[^' ]+) \(RMQ publisher: (?P<var_pub>\w+)\)")
    def step_impl(context, msg, var_pub):
        msg = StepTools.evaluate_scenario_parameter(msg)
        pub = __get_variable_manager().get_variable_value(var_pub)
        pub.publish(msg)
    
    @When(u"wait consumer (?P<var_con>\w+) stops to receive messages \(first timeout: (?P<first_timeout_sec>\d+(?:.\d+)?) s\)")
    def step_impl(context, var_con, first_timeout_sec):
        con = __get_variable_manager().get_variable_value(var_con)
        con.wait_end_message_reception(first_timeout_seconds=float(first_timeout_sec))
    
    @When(u"wait consumer (?P<var_con>\w+) stops to receive messages \(first timeout: (?P<first_timeout_sec>\d+(?:.\d+)?) s ; window: (?P<window_sec>\d+(?:.\d+)?) s\)")
    def step_impl(context, var_con, first_timeout_sec, window_sec):
        con = __get_variable_manager().get_variable_value(var_con)
        con.wait_end_message_reception(first_timeout_seconds=float(first_timeout_sec), window_seconds=float(window_sec))
    
    @When(u"wait consumer (?P<var_con>\w+) stops to receive messages \(first timeout: (?P<first_timeout_sec>\d+(?:.\d+)?) s ; window: (?P<window_sec>\d+(?:.\d+)?) s ; polling: (?P<polling_sec>\d+(?:.\d+)?) s\)")
    def step_impl(context, var_con, first_timeout_sec, window_sec, polling_sec):
        con = __get_variable_manager().get_variable_value(var_con)
        con.wait_end_message_reception(first_timeout_seconds=float(first_timeout_sec), window_seconds=float(window_sec), polling_seconds=float(polling_sec))
    
    @Then(u"consumer (?P<var_con>\w+) doesn't receive any message \(timeout: (?P<timeout_sec>\d+(?:.\d+)?) s ; polling: (?P<polling_sec>\d+(?:.\d+)?) s\)")
    def step_impl(context, var_con, timeout_sec, polling_sec):
        con = __get_variable_manager().get_variable_value(var_con)
        nb = con.wait_end_message_reception(first_timeout_seconds=float(timeout_sec), window_seconds=float(polling_sec)/2, polling_seconds=float(polling_sec), raise_exception=False)
        if nb > 0:
            raise FunctionalException(f"Consumer has received {nb} messages")
    
    @Then(u"consumer (?P<var_con>\w+) received (?P<nb_msg>.+) messages")
    def step_impl(context, var_con, nb_msg):
        nb_msg = StepTools.evaluate_scenario_parameter(nb_msg)
        con = __get_variable_manager().get_variable_value(var_con)
    
        if len(con.messages) != nb_msg:
            raise FunctionalException(f"The number of received messages is {len(con.messages)} (expected: {nb_msg}). Received messages:\n    {(chr(10)+'    ').join((str(m[3]) for m in con.messages))}")
    
    @Then(u"consumer (?P<var_con>\w+) received string messages")
    def step_impl(context, var_con):
        con = __get_variable_manager().get_variable_value(var_con)
        table = BehaveStepTools.convert_step_table_2_value_table(context.table)
    
        messages = [m[3].decode('utf-8') for m in con.messages]
        obtained = TableManager.convert_list_2_column_table(messages)
        
        comparator = Table2ValueTable_Comparator()
        comparator.equals(obtained, table)
    
    @Step(u"(?P<var_name>\w+) = number of received messages \(RMQ consumer: (?P<var_con>\w+)\)")
    def step_impl(context, var_name, var_con):
        con = __get_variable_manager().get_variable_value(var_con)
        
        res = len(con.messages)

        __get_variable_manager().register_variable(var_name, res)
    
    @Step(u"(?P<var_name>\w+) = received messages as (?P<proto_type_str>'[^']*'%?|[^' ]+) Protobuf objects \(RMQ consumer: (?P<var_con>\w+)\)")
    def step_impl(context, var_name, proto_type_str, var_con):
        con = __get_variable_manager().get_variable_value(var_con)
        
        proto_type_str = StepTools.evaluate_scenario_parameter(proto_type_str)
    
        res = []
        for ind, m in enumerate(con.messages):
            logger.debug(f"Received message [{ind}]: {m[3]}")
            msg = __get_session_context().protobuf_messages.new_message(proto_type_str)
            msg.ParseFromString(m[3])
            res.append(msg)                                                                                         
            # logger.debug(f"+++++ New message: type={type(msg)} ; dir={dir(msg)}")
        __get_variable_manager().register_variable(var_name, res)
        
    @Step(u"(?P<var_name>\w+) = pop first received message as (?P<proto_type_str>'[^']*'%?|[^' ]+) Protobuf object \(RMQ consumer: (?P<var_con>\w+)\)")
    def step_impl(context, var_name, proto_type_str, var_con):
        con = __get_variable_manager().get_variable_value(var_con)
        
        proto_type_str = StepTools.evaluate_scenario_parameter(proto_type_str)
        
        message = con.pop_first_message()
        logger.debug(f"Pop first message: {message[3]}")
        res = __get_session_context().protobuf_messages.new_message(proto_type_str)
        res.ParseFromString(message[3])
        # logger.debug(f"+++++ New message: type={type(msg)} ; dir={dir(msg)}")
        
        __get_variable_manager().register_variable(var_name, res)
        
    @Given(u"reset stored messages in consumer (?P<var_con>\w+)")
    def step_impl(context, var_con):
        con = __get_variable_manager().get_variable_value(var_con)
        con.reset_messages()
        
        
    
    @When(u"purge queue (?P<queue>'[^']*'%?|[^' ]+) \(RMQ client: (?P<var_client>\w+)\)")
    def step_impl(context, queue, var_client):
        queue = StepTools.evaluate_scenario_parameter(queue)
        client = __get_variable_manager().get_variable_value(var_client)
        client.channel.queue_purge(queue)
        
