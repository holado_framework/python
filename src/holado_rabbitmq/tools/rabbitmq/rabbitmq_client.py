
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import time
from _datetime import datetime
import logging
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.tools.tools import Tools
import gc
from typing import NamedTuple
import threading
from holado_core.common.threading import traceback
from holado.common.handlers.object import DeleteableObject
from pika.exceptions import StreamLostError
from holado.common.handlers.enums import ObjectState

logger = logging.getLogger(__name__)

try:
    import pika  # @UnresolvedImport @UnusedImport
    import pika.adapters.blocking_connection
    from pika.adapters.blocking_connection import _CallbackResult
    from pika import exceptions
    with_pika = True
except Exception as exc:
    logger.debug(f"RMQClient is not available. Initialization failed on error: {exc}")
    with_pika = False

if with_pika:
    
    class RMQBlockingChannel(pika.adapters.blocking_connection.BlockingChannel):
        """
        Add features to pika.adapters.blocking_connection.BlockingChannel:
          - Rapid close: possibility to rapidly close connection without data consistency verification.
                         It is useful when using consumers for monitoring, like in testing context.
          - Add time_limit parameter to start_consuming method.
        """
        def __init__(self, channel_impl, connection, rapid_close=False):
            super().__init__(channel_impl, connection)
            self.__rapid_close = rapid_close
            
        def start_consuming(self, time_limit=None):
            """
            Override start_consuming method to add time_limit parameter.
            """
            if time_limit is None:
                if not self.__rapid_close:
                    # Use pika implementation
                    return super().start_consuming()
                else:
                    # To ensure a rapid close, time_limit must be 0 rather than None
                    time_limit = 0
                
            # Check if called from the scope of an event dispatch callback
            with self.connection._acquire_event_dispatch() as dispatch_allowed:
                if not dispatch_allowed:
                    raise exceptions.ReentrancyError(
                        'start_consuming may not be called from the scope of '
                        'another BlockingConnection or BlockingChannel callback')
    
            self._impl._raise_if_not_open()
    
            # Process events as long as consumers exist on this channel
            while self._consumer_infos:
                # This will raise ChannelClosed if channel is closed by broker
                self._process_data_events(time_limit=time_limit)

    class RMQBlockingConnection(pika.adapters.blocking_connection.BlockingConnection):
        """
        Add features to pika.adapters.blocking_connection.BlockingConnection:
          - Rapid close: possibility to rapidly close connection without data consistency verification.
                         It is useful when using consumers for monitoring, like in testing context.
        """
        def __init__(self, parameters=None, _impl_class=None, rapid_close=False):
            super().__init__(parameters, _impl_class)
            self.__rapid_close = rapid_close

        def channel(self, channel_number=None):
            """
            Override channel method to use RMQBlockingChannel instead of BlockingChannel.
            The implementation is a copy of BlockingConnection.channel.
            """
            with _CallbackResult(self._OnChannelOpenedArgs) as opened_args:
                impl_channel = self._impl.channel(
                    channel_number=channel_number,
                    on_open_callback=opened_args.set_value_once)
    
                # Create our proxy channel
                channel = RMQBlockingChannel(impl_channel, self, rapid_close=self.__rapid_close)
    
                # Link implementation channel with our proxy channel
                impl_channel._set_cookie(channel)
    
                # Drive I/O until Channel.Open-ok
                channel._flush_output(opened_args.is_ready)
    
            return channel

        def _flush_output(self, *waiters):
            if self.__rapid_close and self.is_closing:
                logger.debug(f"Connection is closing, bypass output flush for rapid close")
            else:
                super()._flush_output(*waiters)
                

class RMQClient(DeleteableObject):
    @classmethod
    def is_available(cls):
        return with_pika
    
    def __init__(self):
        super().__init__('RMQClient')
        
        self.__connection_kwargs = None
        self.__publisher_and_kwargs = []
        self.__consumer_and_kwargs = []
        self.__buffer_consumer_and_kwargs = []
        
        self.__connection = None
        self.__channel = None
        
        self.__consuming_thread_id = None
        self.__is_consuming = False
        self.__is_stopping_consuming = False
        self.__rapid_close = False
    
    def _delete_object(self):
        try:
            if self.__channel and self.__channel.is_open:
                # Cancel each consumer
                # for obj_kwargs in self.__consumer_and_kwargs:
                #     consumer = obj_kwargs.object
                #     self.__channel.basic_cancel(consumer.consumer_tag)
                # for obj_kwargs in self.__buffer_consumer_and_kwargs:
                #     consumer = obj_kwargs.object
                #     self.__channel.basic_cancel(consumer.consumer_tag)
                
                # Stop consuming
                if self.is_consuming:
                    logger.debug(f"Deleting RabbitMQClient: Stopping consuming...")
                    self.stop_consuming()
                
                # Close channel
                if self.__channel.is_open:
                    logger.debug(f"Deleting RabbitMQClient: Closing channel...")
                    self.__channel.close()
                
            # Close connection
            if self.__connection and self.__connection.is_open:
                logger.debug(f"Deleting RabbitMQClient: Closing connection...")
                self.close_connection()
        except StreamLostError as exc:
            if self.object_state == ObjectState.Deleting:
                pass
            else:
                raise exc
        
    @property
    def connection(self):
        return self.__connection
    
    @property
    def channel(self):
        return self.__channel
    
    @property
    def is_consuming(self):
        return self.__is_consuming
    
    @property
    def consuming_thread_id(self):
        return self.__consuming_thread_id
    
    @consuming_thread_id.setter
    def consuming_thread_id(self, thread_id):
        self.__consuming_thread_id = thread_id
    
    @property
    def rapid_close(self):
        return self.__rapid_close
    
    @rapid_close.setter
    def rapid_close(self, rapid_close):
        self.__rapid_close = rapid_close
    
    def connect_blocking(self, **connection_parameters_kwargs):
        if Tools.has_sub_kwargs(connection_parameters_kwargs, "authentication."):
            authentication = Tools.pop_sub_kwargs(connection_parameters_kwargs, "authentication.")
            if 'user' in authentication:
                if type(authentication['user']) is tuple:
                    connection_parameters_kwargs['credentials'] = pika.PlainCredentials(authentication['user'][0], authentication['user'][1])
                else:
                    raise FunctionalException(f"When authenticating by user, the value has to be in format: ('{{USER}}', '{{PASSWORD}}')  (obtained: {authentication['user']})")
            else:
                raise TechnicalException(f"Unmanaged authentication type '{authentication.keys()}' (possible authentication types: 'user'")

        self.__connection_kwargs = connection_parameters_kwargs
        connection_parameters = pika.ConnectionParameters(**connection_parameters_kwargs)
        
        last_exception = None
        connected = False
        for _ in range(300):
            try:
                self.__connection = RMQBlockingConnection(connection_parameters, rapid_close=self.rapid_close)
                connected = True
                break
            except pika.exceptions.IncompatibleProtocolError as exc:
                is_same_exception = str(last_exception) == str(exc) if last_exception else False
                if not last_exception or not is_same_exception:
                    logger.exception("Retry after possible temporary incompatible protocol errors while trying to connect with a blocking connection")
                    last_exception = exc
                elif is_same_exception:
                    logger.warning("Retry after same exception")
                time.sleep(0.1)
            except Exception as exc:
                is_same_exception = str(last_exception) == str(exc) if last_exception else False
                if not last_exception or not is_same_exception:
                    logger.exception("Retry after unexpected exception while trying to connect with a blocking connection")
                    last_exception = exc
                elif is_same_exception:
                    logger.warning("Retry after same exception")
                time.sleep(0.1)
        if not connected:
            raise TechnicalException(f"Failed to connect with a blocking connection and parameters [{connection_parameters}]. Last error was (more details in report): {last_exception}")
        
        self.__channel = self.__connection.channel()

    def close_connection(self):
        if self.__connection is None or not self.__connection.is_open:
            raise TechnicalException("Client is not connected")
        
        try:
            self.__connection.close()
        except Exception as exc:  # @UnusedVariable
            #TODO: When this warning is logged during self.__del__, the log is cleared before, thus it is commented
            # logger.warn(f"Error catched while closing RabbitMQ client connection:\n{Tools.represent_exception(exc)}")
            # pass
            raise exc
        
    def __new_object_kwargs(self, obj, **kwargs):
        res = NamedTuple('ObjectKwargs', object=object, kwargs=dict)
        res.object = obj
        res.kwargs = kwargs
        return res

    def new_publisher(self, queue, queue_args=None, exchange="", exchange_args=None, routing_key=None):
        if exchange != "":
            self.__exchange_declare(exchange, exchange_args)
        self.__queue_declare(queue, queue_args)
        if routing_key:
            res = RMQPublisher(self, exchange, routing_key)
        else:
            res = RMQPublisher(self, exchange, queue)
            
        self.__publisher_and_kwargs.append( self.__new_object_kwargs(res, queue=queue, queue_args=queue_args, exchange=exchange, exchange_args=exchange_args, routing_key=routing_key) )
        return res
        
    def new_consumer(self, queue, message_callback, queue_args=None, exchange="", exchange_args=None, bind_args=None):
        if self.is_consuming:
            raise FunctionalException("Not allowed to create a new consumer while consuming is started")
        queue_name = self.__prepare_new_consumer(queue, queue_args, exchange, exchange_args, bind_args)
        res = RMQConsumer(self, queue_name, message_callback)
        self.__consumer_and_kwargs.append( self.__new_object_kwargs(res, queue=queue, message_callback=message_callback, queue_args=queue_args, exchange=exchange, exchange_args=exchange_args, bind_args=bind_args) )
        return res
        
    def new_buffer_consumer(self, queue, queue_args=None, exchange="", exchange_args=None, bind_args=None):
        if self.is_consuming:
            raise FunctionalException("Not allowed to create a new consumer while consuming is started")
        queue_name = self.__prepare_new_consumer(queue, queue_args, exchange, exchange_args, bind_args)
        res = RMQBufferConsumer(self, queue_name)
        self.__buffer_consumer_and_kwargs.append( self.__new_object_kwargs(res, queue=queue, queue_args=queue_args, exchange=exchange, exchange_args=exchange_args, bind_args=bind_args) )
        return res
        
    def start_consuming(self):
        # Workaround: 
        #    Sometimes "When start consuming" is called rather than "Then client is consuming" 
        #    cf implementation of step @Given(u"start consuming in a thread \(RMQ client: (?P<var_client>\w+)\)")
        #    Workaround: register the thread_id allowed to start consuming.
        #    Note: A possible reason is that execute_steps is not thread safe. If it's true, the new implementation
        #          of the step with the wait of 0.01 seconds has resolved the problem, and thus this workaround is not needed anymore.
        if self.consuming_thread_id is not None:
            thread_id = threading.current_thread().ident
            if thread_id != self.consuming_thread_id:
                logger.error(f"Only thread {self.consuming_thread_id} is allowed to start consuming. Tried to start consuming in thread {thread_id} with traceback:\n{traceback.represent_stack(indent=4)}")
                return
        
        logger.debug(f"Beginning start consuming...")
        self.__is_consuming = True
        try:
            self.channel.start_consuming()
        except Exception as exc:
            if self.__is_stopping_consuming:
                if isinstance(exc, pika.exceptions.StreamLostError) \
                        or isinstance(exc, AttributeError) and "NoneType' object has no attribute 'clear'" in str(exc) \
                        or isinstance(exc, AssertionError):
                    logger.info(f"Caught exception in consuming thread while stopping consuming: {exc}")
                else:
                    logger.warn(f"Caught unexpected exception in consuming thread while stopping consuming: {Tools.represent_exception(exc)}")
            else:
                raise exc
        finally:
            # In all cases, self.__is_consuming must began False, otherwise stopping processing is broken in case of "raise exc"
            self.__is_consuming = False
            logger.debug(f"Finished start consuming")
        
    def stop_consuming(self):
        logger.debug(f"Beginning stop consuming...")
        self.__is_stopping_consuming = True
        try:
            # self.__stop_consuming_by_channel_stop_consuming()
            self.__stop_consuming_by_deleting_channel_consumer_infos()
            
            gc.collect()
            
            # Wait end of consuming
            if self.__is_consuming:
                logger.info("Waiting consuming thread is stopped...")
                #TODO: put timeout
                while self.__is_consuming:
                    time.sleep(0.1)
                    gc.collect()
                logger.info("Finished waiting consuming thread is stopped")
        finally:
            self.__is_stopping_consuming = False
            logger.debug(f"Finished stop consuming")
        
    def __stop_consuming_by_channel_stop_consuming(self):
        try:
            self.channel.stop_consuming()
        except Exception as exc:
            if isinstance(exc, pika.exceptions.StreamLostError) \
                    or isinstance(exc, AttributeError) and "NoneType' object has no attribute 'clear'" in str(exc) \
                    or isinstance(exc, AssertionError):
                logger.info(f"Caught exception while executing consuming stopping method: {exc}")
            else:
                logger.warn(f"Caught exception while executing consuming stopping method: {Tools.represent_exception(exc)}")
                
    def __stop_consuming_by_deleting_channel_consumer_infos(self):
        for consumer_tag in list(self.channel._consumer_infos.keys()):
            del self.channel._consumer_infos[consumer_tag]
            # Schedule termination of connection.process_data_events using a
            # negative channel number
#                self.connection._request_channel_dispatch(-self.channel.channel_number)
        self.connection._request_channel_dispatch(-self.channel.channel_number)
        
    def __prepare_new_consumer(self, queue, queue_args=None, exchange="", exchange_args=None, bind_args=None):
        if exchange != "":
            self.__exchange_declare(exchange, exchange_args)
        
        if queue != "":
            self.__queue_declare(queue, queue_args)
            queue_name = queue
        else:
            q_args = dict(queue_args) if queue_args else {}
            q_args['exclusive'] = True
            result = self.__queue_declare(queue, q_args)
            queue_name = result.method.queue
            
        if exchange != "":
            self.__queue_bind(queue_name, exchange, bind_args)
            
        return queue_name

    def __exchange_declare(self, exchange, exchange_args=None):
        kwargs = {}
        arguments = None
        if exchange_args:
            arguments = dict(exchange_args)
            for name in ['exchange_type', 'passive', 'durable', 'auto_delete', 'internal']:
                if name in arguments:
                    kwargs[name] = arguments.pop(name)
        return self.channel.exchange_declare(exchange, arguments=arguments, **kwargs)
        
    def __queue_declare(self, queue, queue_args=None):
        kwargs = {}
        arguments = None
        if queue_args:
            arguments = dict(queue_args)
            for name in ['passive', 'durable', 'exclusive', 'auto_delete']:
                if name in arguments:
                    kwargs[name] = arguments.pop(name)
        return self.channel.queue_declare(queue, arguments=arguments, **kwargs)
        
    def __queue_bind(self, queue, exchange, bind_args=None):
        kwargs = {}
        arguments = None
        if bind_args:
            arguments = dict(bind_args)
            for name in ['routing_key']:
                if name in arguments:
                    kwargs[name] = arguments.pop(name)
        return self.channel.queue_bind(queue, exchange, arguments=arguments, **kwargs)
        
class RMQPublisher(object):
    def __init__(self, client, exchange, routing_key):
        self.__client = client
        self.__exchange = exchange
        self.__routing_key = routing_key
        
    def publish(self, body, **kwargs):
        self.__client.channel.basic_publish(exchange=self.__exchange, routing_key=self.__routing_key, body=body, **kwargs)
        
class RMQConsumer(object):
    def __init__(self, client, queue, message_callback):
        self.__client = client
        self.__queue = queue
        self.__message_callback = message_callback
        
        self.__consumer_tag = self.__client.channel.basic_consume(queue=self.__queue, on_message_callback=self.__message_callback, auto_ack=True)
    
    @property
    def queue(self):
        return self.__queue
    
    @property
    def consumer_tag(self):
        return self.__consumer_tag
        
class RMQBufferConsumer(object):
    def __init__(self, client, queue):
        self.__consumer = RMQConsumer(client, queue, self.__message_callback)
        
        self.__messages = []
        self.__messages_lock = threading.Lock()
    
    @property
    def nb_messages(self):
        with self.__messages_lock:
            return len(self.__messages)
    
    @property
    def messages(self):
        with self.__messages_lock:
            return list(self.__messages)
    
    @property
    def consumer_tag(self):
        return self.__consumer.consumer_tag
        
    def __message_callback(self, channel, method, properties, body):
        with self.__messages_lock:
            self.__messages.append( (channel, method, properties, body) )
            # logger.debug(f"[Consumer '{self.__consumer.queue}'] New message (total: {len(self.__messages)}): {channel=} ; {method=} ; {properties=} ; {body=}")
            logger.debug(f"[Consumer '{self.__consumer.queue}'] New message (total: {len(self.__messages)}): {body}")
        
    #TODO EKL: add a first_accepted_seconds and a accepted_window_seconds to be able to raise an exception that first or next message arrived but too late
    #TODO EKL: change use of polling_seconds to something like a timer, rather than waiting polling_seconds
    def wait_end_message_reception(self, first_timeout_seconds, window_seconds = None, polling_seconds = None, raise_exception = True):
        if window_seconds is None:
            window_seconds = first_timeout_seconds / 10
            if window_seconds < 0.01:
                window_seconds = 0.01       # a window period below 10 ms is usually not efficient in testing context
        if polling_seconds is None:
            min_window = min(first_timeout_seconds, window_seconds)
            polling_seconds = min_window / 100       # 1% of error on window detection
            if polling_seconds > 0.1:
                polling_seconds = 0.1       # a polling period over 100 ms is usually not efficient in testing context
        
        nb_msg = 0
        
        # Wait first message
        dt_begin = datetime.now()
        dt_last_poll = dt_begin
        while (dt_last_poll - dt_begin).total_seconds() < first_timeout_seconds:
            nb = self.nb_messages
            dt_last_poll = datetime.now()
            if nb > 0:
                logger.debug(f"[Consumer '{self.__consumer.queue}'] Received first messages: total={nb}")
                nb_msg = nb
                break
            time.sleep(polling_seconds)
        if self.nb_messages == 0:
            if raise_exception:
                raise FunctionalException(f"No message was received (timeout: {first_timeout_seconds} seconds)")
            else:
                return 0
        
        # Wait end of reception
        dt_last_receive = dt_last_poll
        while (dt_last_poll - dt_last_receive).total_seconds() < window_seconds:
            nb = self.nb_messages
            dt_last_poll = datetime.now()
            if nb > nb_msg:
                logger.debug(f"[Consumer '{self.__consumer.queue}'] Received messages: total={nb} ; new={nb-nb_msg}")
                nb_msg = nb
                dt_last_receive = dt_last_poll
            time.sleep(polling_seconds)
        
        return self.nb_messages
    
    def reset_messages(self):
        with self.__messages_lock:
            logger.debug(f"[Consumer '{self.__consumer.queue}'] Reset messages")
            self.__messages = []
        
    def pop_first_message(self):
        with self.__messages_lock:
            res = self.__messages.pop(0)
            logger.debug(f"[Consumer '{self.__consumer.queue}'] Pop first message (remaining: {len(self.__messages)}): {res}")
            return res
    