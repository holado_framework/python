
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import threading
import logging
import sys
import os.path
from pathlib import Path
import configparser
from holado_logging.common.logging.log_config import LogConfig
from holado_core.common.exceptions.technical_exception import TechnicalException


logger = logging.getLogger(__name__)


class LogManager(object):

    def __init__(self):
        self.__loggers_levels = []
        
        self.__config_file_path = None
        
        self.on_console = False
        self.__console_handler = None
        
        self.in_file = True
        self.__files_lock = threading.Lock()
        self.__file_names = []
        self.__file_handlers = {}
        self.__root_file_name = None
        self.__root_file_handler = None
        self.__root_file_handler_active = False
        
        # Initialize format according python version
        if sys.version_info > (3, 2):
            # self.format = '{asctime:s} | {thread:-5d} | {levelname:5s} | {module:35s} | {message:s}'
            self.format = '{asctime:s} | {thread:-5d} | {levelname:5s} | {name:50s} | {message:s}'
            self.style = '{'
        else: 
            self.format = '%(asctime)s | %(thread)-5d | %(levelname)5s | %(module)35s | %(message)s'
            self.style = '%'
        
    def initialize(self, log_on_console=True):
        """
        Initialize log manager.
        If log_on_console is True, logs are published on console until a new configuration by calling method set_config
        """
        if log_on_console:
            self.on_console = True
            self.__console_handler = self.__new_console_handler()
            logging.basicConfig(format=self.format, style=self.style, level=LogConfig.default_level, handlers=[self.__console_handler])
        else:
            logging.basicConfig(format=self.format, style=self.style, level=LogConfig.default_level, handlers=[])
        
    def set_config_file_path(self, file_path):
        self.__config_file_path = file_path
        
        config = configparser.ConfigParser()
        config.read(self.__config_file_path)
        LogConfig.default_level = config.get("logger_root", "level")
        
        if config.has_section("loggers_levels"):
            self.__loggers_levels = config.items(section="loggers_levels")
    
    def has_log_file(self, file_name):
        # with self.__files_lock:
            return file_name in self.__file_names or file_name == self.__root_file_name
    
    def set_root_log_file(self, file_name):
        with self.__files_lock:
            if file_name is not None and len(file_name) > 0:
                self.__root_file_name = file_name
                
    def reset_log_files(self):
        with self.__files_lock:
            self.__file_names.clear()
                
    def add_log_file(self, file_name):
        from holado_core.common.exceptions.technical_exception import TechnicalException
        
        with self.__files_lock:
            if file_name is not None and len(file_name) > 0:
                if file_name == self.__root_file_name:
                    raise TechnicalException(f"Log file '{file_name}' is already set as root log file")
                if file_name in self.__file_names:
                    raise TechnicalException(f"Log file '{file_name}' is already set")
                self.__file_names.append(file_name)
    
    def remove_log_file(self, file_name):
        with self.__files_lock:
            if file_name is not None and len(file_name) > 0 and file_name in self.__file_names:
                self.__file_names.remove(file_name)
        
    def __new_console_handler(self):
        res = logging.StreamHandler()
        res.setFormatter(logging.Formatter(fmt=self.format, style=self.style))
        return res
    
    def set_config(self):
        # print(f"Set logging config: {LogConfig.default_level=} ; {self.on_console=} ; {self.in_file=} ; {self.__root_file_name=}")

        # if self.__config_file_path is not None:
        #     logging.config.fileConfig(self.__config_file_path, defaults=None, disable_existing_loggers=False)
        
        logger_ = logging.getLogger()

        # Update log destination to console
        if self.on_console:
            if self.__console_handler is not None:
                logger.debug("Log destination already set to console.")
            else:
                logger.debug("Adding log destination to console.")
                self.__console_handler = self.__new_console_handler()
                logger_.addHandler(self.__console_handler)
        else:
            if self.__console_handler is not None:
                logger.debug("Removing log destination to console.")
                logger_.removeHandler(self.__console_handler)
                self.__console_handler = None
                
        # Update log destination to files
        with self.__files_lock:
            # Remove old log files
            # if self.__root_file_name:
            #     self.remove_root_file_handler(do_reset=True)
            for file_name in list(self.__file_handlers.keys()):
                if file_name not in self.__file_names:
                    self.remove_file_handler(file_name, do_remove_log_file=False)
            
            # Add new log files
            if self.__root_file_name and not self.__root_file_handler:
                self.add_root_file_handler()
            for file_name in self.__file_names:
                if file_name not in list(self.__file_handlers.keys()):
                    self.add_file_handler(file_name)
                
        
        # level
        if logger_.getEffectiveLevel() != LogConfig.default_level:
            logger_.setLevel(LogConfig.default_level)
    
        # Loggers levels
        for name, level in self.__loggers_levels:
            if not name.startswith("#"):
                logging.getLogger(name).setLevel(level)
                
    def set_level(self, log_level):
        if isinstance(log_level, str):
            if hasattr(logging, log_level):
                log_level = getattr(logging, log_level)
            else:
                raise TechnicalException(f"Unexpected log level string '{log_level}'")
            
        LogConfig.default_level = log_level
        self.set_config()
        
    def add_root_file_handler(self):
        from holado_core.common.exceptions.technical_exception import TechnicalException
        
        logger_ = logging.getLogger()
            
        if not self.__root_file_name:
            raise TechnicalException("Root log file is not defined")
            
        if self.__root_file_handler is None:
            logger_.info("Creating file handler to root file '{}'.".format(self.__root_file_name))
            Path(os.path.dirname(self.__root_file_name)).mkdir(parents=True, exist_ok=True)
            self.__root_file_handler = logging.FileHandler(self.__root_file_name, mode='w', encoding='utf8')
            self.__root_file_handler.setFormatter(logging.Formatter(fmt=self.format, style=self.style))
            
        logger_.addHandler(self.__root_file_handler)
        self.__root_file_handler_active = True
        
    def remove_root_file_handler(self, do_reset=False):
        logger_ = logging.getLogger()
        if self.__root_file_handler:
            logger_.info(f"Removing log destination to root file '{self.__root_file_name}'.")
            logger_.removeHandler(self.__root_file_handler)
            self.__root_file_handler_active = False
            if do_reset:
                self.__root_file_handler = None
        
    def add_file_handler(self, file_name, logger_=None):
        if logger_ is None:
            logger_ = logging.getLogger()
            
        # In case this method is called outside "set_config" method, add file_name as a configured log_file
        if not self.has_log_file(file_name):
            self.add_log_file(file_name)
            
        if file_name in self.__file_handlers:
            logger_.debug("Log destination already set to file '{}'.".format(file_name))
        else:
            logger_.info("Adding log destination to file '{}'.".format(file_name))
            Path(os.path.dirname(file_name)).mkdir(parents=True, exist_ok=True)
            file_handler = logging.FileHandler(file_name, mode='w', encoding='utf8')
            file_handler.setFormatter(logging.Formatter(fmt=self.format, style=self.style))
            self.__file_handlers[file_name] = file_handler
            logger_.addHandler(file_handler)
                
    def remove_file_handler(self, file_name, logger_=None, do_remove_log_file=True):
        from holado_core.common.exceptions.technical_exception import TechnicalException
        
        if logger_ is None:
            logger_ = logging.getLogger()
            
        if file_name not in self.__file_handlers:
            raise TechnicalException(f"Not set log destination to file '{file_name}'")
        
        logger_.info("Removing log destination to file '{}'.".format(file_name))
        logger_.removeHandler(self.__file_handlers[file_name])
        del self.__file_handlers[file_name]
        
        if do_remove_log_file:
            self.remove_log_file(file_name)
            
    def enter_log_file(self, file_name, do_remove_root_file_handler=True):
        if not self.has_log_file(file_name):
            self.add_log_file(file_name)
            
        self.add_file_handler(file_name)
        if do_remove_root_file_handler:
            self.remove_root_file_handler(do_reset=False)
            
    def leave_log_file(self, file_name, do_remove_log_file=True):
        if not self.__root_file_handler_active:
            self.add_root_file_handler()
        self.remove_file_handler(file_name, do_remove_log_file=do_remove_log_file)
    
    
    