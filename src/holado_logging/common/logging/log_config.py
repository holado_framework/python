
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging


# logger = logging.getLogger(__name__)


class LogConfig(object):
    TLogger = None
    TManager = None
    default_level = logging.INFO
    
    @classmethod
    def configure(cls, log_level=None):
        from holado_logging.common.logging.test_logger import TestLogger
        
        if log_level:
            cls.default_level = log_level
        TestLogger.default_message_size_limit = 10000
        cls.TLogger = TestLogger
        cls.TManager = logging.Manager
        
        # Configure logging
        cls.configure_logging()
    
    @classmethod
    def configure_logging(cls):
        #TODO EKL: make loggers configuration optional
        # Configure loggers
        from holado_logging.common.logging.test_logger import TestRootLogger
        logging.root = TestRootLogger(cls.default_level)
        logging.Logger.root = logging.root
        logging.Logger.manager = cls.TManager(cls.TLogger.root)
        
        logging.setLoggerClass(cls.TLogger)
        
        # Add TRACE logging level
        cls.add_logging_level_trace()
    
    @classmethod
    def add_logging_level_trace(cls):
        if not cls.has_logging_level("TRACE"):
            cls.add_logging_level("TRACE", 5, None)
        
    
    @classmethod
    def has_logging_level(cls, levelName):
        return hasattr(logging, levelName)
    
    @classmethod
    def add_logging_level(cls, levelName, levelNum, methodName=None):
        """
        This method was implemented and shared by the author of library haggis (https://haggis.readthedocs.io).
        
        Comprehensively adds a new logging level to the `logging` module and the
        currently configured logging class.
    
        `levelName` becomes an attribute of the `logging` module with the value
        `levelNum`. `methodName` becomes a convenience method for both `logging`
        itself and the class returned by `logging.getLoggerClass()` (usually just
        `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
        used.
    
        To avoid accidental clobberings of existing attributes, this method will
        raise an `AttributeError` if the level name is already an attribute of the
        `logging` module or if the method name is already present 
    
        Example
        -------
        >>> addLoggingLevel('TRACE', logging.DEBUG - 5)
        >>> logging.getLogger(__name__).setLevel("TRACE")
        >>> logging.getLogger(__name__).trace('that worked')
        >>> logging.trace('so did this')
        >>> logging.TRACE
        5
    
        """
        if not methodName:
            methodName = levelName.lower()
    
        if hasattr(logging, levelName):
            raise AttributeError('{} already defined in logging module'.format(levelName))
        if hasattr(logging, methodName):
            raise AttributeError('{} already defined in logging module'.format(methodName))
        if hasattr(logging.getLoggerClass(), methodName):
            raise AttributeError('{} already defined in logger class'.format(methodName))
    
        # This method was inspired by the answers to Stack Overflow post
        # http://stackoverflow.com/q/2183233/2988730, especially
        # http://stackoverflow.com/a/13638084/2988730
        def logForLevel(self, message, *args, **kwargs):
            if self.isEnabledFor(levelNum):
                self._log(levelNum, message, args, **kwargs)
        def logToRoot(message, *args, **kwargs):
            logging.log(levelNum, message, *args, **kwargs)
    
        logging.addLevelName(levelNum, levelName)
        setattr(logging, levelName, levelNum)
        setattr(logging.getLoggerClass(), methodName, logForLevel)
        setattr(logging, methodName, logToRoot)
    
    
    
    