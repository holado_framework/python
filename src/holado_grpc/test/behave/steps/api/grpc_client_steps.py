# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_value.common.tables.value_table_manager import ValueTableManager
from holado_grpc.api.rpc.grpc_client import GRpcClient
from behave.model_describe import ModelDescriptor
import logging
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


if GRpcClient.is_available():

    def __get_session_context():
        return SessionContext.instance()
    
    def __get_scenario_context():
        return __get_session_context().get_scenario_context()
    
    def __get_text_interpreter():
        return __get_scenario_context().get_text_interpreter()
    
    def __get_variable_manager():
        return __get_scenario_context().get_variable_manager()
    
    def __get_grpc_services():
        return __get_session_context().grpc_services

        
    @Given(u"(?P<var_name>\w+) = new gRPC client")
    def step_impl(context, var_name):
        table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
        kwargs = ValueTableManager.convert_name_value_table_2_dict(table)
        
        client = __get_session_context().grpc_manager.new_client(None, **kwargs)
        
        __get_variable_manager().register_variable(var_name, client)
        
    @Given(u"(?P<var_name>\w+) = service names \(gRPC client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, var_client):
        client = __get_variable_manager().get_variable_value(var_client)
        service_names = client.internal_client.service_names
        __get_variable_manager().register_variable(var_name, service_names)
        
    @When(u"(?P<var_name>\w+) = request (?P<service_method>'[^']*'|[^' ]+) \(gRPC client: (?P<var_client>\w+)(?: ; with Protobuf (?P<with_proto_request>request)?(?: & )?(?P<with_proto_response>response)?)?\)")
    def step_impl(context, var_name, service_method, var_client, with_proto_request, with_proto_response):
        service_method = StepTools.evaluate_scenario_parameter(service_method)
        client = __get_variable_manager().get_variable_value(var_client)

        if hasattr(context, "table") and context.table is not None:
            if with_proto_request is not None:
                service_fullname, method_name = service_method.rsplit('.', 1)
                rendered_table = ModelDescriptor.describe_table(context.table, "    ")
                method_descriptor = __get_grpc_services().get_method_descriptor(service_fullname, method_name)
                execute_steps(u"""
                    When __METHOD_REQUEST__ = new Protobuf object of type '{type_fullname}'
                    {table}
                    """.format(type_fullname=method_descriptor.input_type.full_name, table=rendered_table))
                request_data = __get_variable_manager().get_variable_value("__METHOD_REQUEST__")
            else:
                table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
                request_data = ValueTableManager.convert_name_value_table_2_json_object(table)
        else:
            request_data = {}
        
        service_name, method_name = service_method.rsplit('.', 1)
        result = client.request(service_name, method_name, request_data, raw_output=with_proto_response is not None)
        
        __get_variable_manager().register_variable(var_name, result)




    
