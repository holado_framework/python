# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
import os.path
from holado_core.tools.abstracts.blocking_command_service import BlockingCommandService
import logging

logger = logging.getLogger(__name__)


def __get_session_context():
    return SessionContext.instance()

def __get_scenario_context():
    return __get_session_context().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()


@Given(u"(?P<var_name>\w+) = start internal gRPC server")
def step_impl(context, var_name):
    here = os.path.abspath(os.path.dirname(__file__))
    api_grpc_path = os.path.normpath(os.path.join(here, "..", "..", "..", "..", "..", "..", "..", "tests", "behave", "test_holado", "tools", "django", "api_grpc"))
    manage_path = os.path.join(api_grpc_path, "manage.py")
    cmd = f"python {manage_path} grpcrunserver"
    
    obj = BlockingCommandService("internal gRPC server", cmd)
    obj.auto_stop = True
    obj.start()
    
    __get_variable_manager().register_variable(var_name, obj)
    
    # Update imported grpc messages and services
    proto_path = os.path.join(api_grpc_path, "api_grpc", "api1", "proto")
    __get_session_context().protobuf_messages.import_all_compiled_proto(proto_path)
    __get_session_context().grpc_services.import_all_compiled_proto(proto_path)
    
    
@Given(u"(?P<var_name>\w+) = new internal gRPC client on service (?P<service_name>'[^']*'|[^' ]+)")
def step_impl(context, var_name, service_name):
    execute_steps(u"""
        Given {var_name} = new gRPC client
            | Name                      | Value           |
            | 'endpoint'                | '0.0.0.0:50051' |
            | 'service'                 | {service_name}  |
        """.format(var_name=var_name, service_name=service_name))
