
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado.common.context.session_context import SessionContext

logger = logging.getLogger(__name__)

try:
    import grpc_requests
    import grpc
    with_grpc_requests = True
except Exception as exc:
    logger.debug(f"GRpcClient is not available. Initialization failed on error: {exc}")
    with_grpc_requests = False

# Activation of logs in gRPC library (activated logs are thrown in stdout, it doesn't follow logging settings)
# os.environ["GRPC_VERBOSITY"] = "DEBUG"
# os.environ["GRPC_TRACE"] = "http"
# os.environ["GRPC_TRACE"] = "all"


class GRpcClient(object):
    """
    gRPC client
    
    It is implemented internally with grpc_requests, that manages reflection clients and stub clients.
    """
    
    @classmethod
    def is_available(cls):
        return with_grpc_requests
    
    def __init__(self, name, endpoint, **kwargs):
        self.__name = name
        self.__endpoint = endpoint
        self.__kwargs = kwargs
        self.__client = grpc_requests.client.get_by_endpoint(endpoint, **kwargs)

    @staticmethod
    def __get_protobuf_converter():
        return SessionContext.instance().protobuf_converter
    
    @property
    def name(self):
        return self.__name
    
    @property    
    def internal_client(self) -> grpc_requests.client.Client:
        return self.__client
    
    def request(self, service, method, request, raw_output=False, **kwargs):
        """
        :param request: request data in json or proto format
        :param raw_output: if method should return a proto object or a json oject (default: False)
        :returns: if raw_output==True, returns a proto object, else returns a json object with proto data
        """
        logger.info(f"Requesting {service}.{method} with data [{request}] (raw_output: {raw_output} ; kwargs:{kwargs})")
        try:
            # Ask always raw_output=True, so that we get a Protobuf instance, and then conversion is done if needed
            res_proto = self.internal_client.request(service, method, request, raw_output=True, **kwargs)
        except Exception as exc:
            if "status" in str(exc):
                raise FunctionalException(str(exc)) from exc
            else:
                raise TechnicalException(str(exc)) from exc
        
        # Manage result conversion if needed
        # Note: this step is done manually since grpc_requests has some limitations when raw_output=False:
        #     - Field with default values are not set in json result
        #     - Some field types are badly managed (ex: uint64 fields appear as string in json)
        if raw_output == True:
            if isinstance(res_proto, grpc._channel._MultiThreadedRendezvous):
                return list(res_proto)
            else:
                return res_proto
        else:
            if isinstance(res_proto, grpc._channel._MultiThreadedRendezvous):
                res = [GRpcClient.__get_protobuf_converter().convert_protobuf_object_to_json_object(cur_res) for cur_res in res_proto]
            else:
                res = GRpcClient.__get_protobuf_converter().convert_protobuf_object_to_json_object(res_proto)
            return res
            
            
            