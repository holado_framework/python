
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_test.scenario.step_tools import StepTools
from holado_value.common.tables.value_table import ValueTable
from holado.common.context.session_context import SessionContext

logger = logging.getLogger(__name__)



class BehaveStepTools(StepTools):
    
    @classmethod    
    def get_step_multiline_text(cls, context, raise_exception_if_none=True, log_level=logging.DEBUG):
        """
        Get the step multiline text interpreted.
        Note: parameter context can be the step context or the step object.
        """
        res = None
        if hasattr(context, "text") and context.text is not None:
            res = context.text.strip()
            res = cls._get_text_interpreter().interpret(res, log_level=log_level)
            
        if res is None and raise_exception_if_none:
            raise FunctionalException(f"Multiline text is required under this step\n  context: {context}")
        
        return res
        
    
    @classmethod    
    def get_step_table(cls, context):
        """
        Get the step table interpreted.
        Note: parameter context can be the step context or the step object.
        """
        if hasattr(context, "table") and context.table is not None:
            if cls.has_table_header(context.table):
                res = cls.convert_step_table_2_value_table_with_header(context.table)
            else:
                res = cls.convert_step_table_2_value_table(context.table)
            return res
        else:
            return None
        
    @classmethod        
    def _get_table_header(cls, table):
        return table.headings

    @classmethod
    def _get_table_rows(cls, table):
        return table
 
    @classmethod
    def convert_step_table_2_value_table(cls, table):
        logger.trace(f"Converting step table to scenario table (step table = {table})")

        res = ValueTable()
        
        # Set header as row
        previous_row = None
        srow = cls._convert_step_table_row_2_value_table_row(cls._get_table_header(table), previous_row)
        res.add_row(row=srow)
        
        # Set body
        previous_row = srow
        for row in cls._get_table_rows(table):
            srow = cls._convert_step_table_row_2_value_table_row(row, previous_row)
            res.add_row(row=srow)
            previous_row = srow
            
        # Manage one cell table referencing a variable containing a table
        res = cls._extract_inner_table_if_present(res)
            
        logger.debug(f"Converting step table to scenario table (step table = {table}) => {res}")
        return res
        
        
        
