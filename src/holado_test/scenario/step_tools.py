
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from holado.common.context.session_context import SessionContext
import logging
import re
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_scripting.common.tools.variable_manager import VariableManager
from holado_core.common.tools.tools import Tools
from holado_value.common.tables.value_table import ValueTable
from holado_value.common.tables.value_table_with_header import ValueTableWithHeader
from holado_core.common.tables.table_with_header import TableWithHeader
from holado_value.common.tables.converters.value_table_converter import ValueTableConverter
from holado_core.common.tables.table import Table
from holado_core.common.tables.table_row import TableRow
from holado_core.common.tables.table_cell import TableCell
from holado_value.common.tables.value_table_row import ValueTableRow
from holado_value.common.tables.value_table_cell import ValueTableCell
from holado_value.common.tools.value_types import ValueTypes
from holado_value.common.tables.comparators.table_2_value_table_with_header_comparator import Table2ValueTable_WithHeaderComparator
from holado_value.common.tables.comparators.table_2_value_table_comparator import Table2ValueTable_Comparator
from holado_core.common.tables.table_manager import TableManager

logger = logging.getLogger(__name__)



class StepTools(object):
    
    __pattern_quoted_parameter = re.compile("^'(.*)'(%?)$")
    #__pattern_or = re.compile("^(.*) or (.*)$")
    #__pattern_and = re.compile("^(.*) and (.*)$")
    #__pattern_comparison = re.compile("^(.*) (==|!=|<|<=|>=|>) (.*)$")
    
    @staticmethod
    def _get_scenario_context():
        return SessionContext.instance().get_scenario_context()

    @staticmethod
    def _get_expression_evaluator():
        return StepTools._get_scenario_context().get_expression_evaluator()

    @staticmethod
    def _get_text_interpreter():
        return StepTools._get_scenario_context().get_text_interpreter()

    @staticmethod
    def _get_variable_manager() -> VariableManager:
        return StepTools._get_scenario_context().get_variable_manager()

    @staticmethod
    def unescape_string(text):
        #res = bytes(text, "utf-8").decode("unicode_escape")
        res = text.encode('latin-1').decode('unicode-escape')
        
        logger.trace("unescaping string [{}] -> [{}]".format(text, res))
        return res
    
    @staticmethod
    def extract_string_value(text_param):
        return StepTools._get_expression_evaluator().extract_string_value(text_param)
    
    @staticmethod
    def evaluate_string_parameter(text_param):
        res = StepTools._get_expression_evaluator().evaluate_expression(text_param, do_interpret=False, do_eval=False, log_level=logging.TRACE)  # @UndefinedVariable
        if Tools.do_log_if_objects_are_different(text_param, res):
            logger.debug("evaluate_string_parameter: [{}] -> [{}]".format(text_param, res))
        return res
    
    @staticmethod
    def evaluate_scenario_parameter(text_param, log_level=logging.DEBUG):
        # res = None
        # r_match = StepTools.__pattern_quoted_parameter.match(text_param)
        # if r_match:
        #     value = StepTools.unescape_string(r_match.group(1))
        #     if r_match.group(2) == "%":
        #         res = StepTools.__get_text_interpreter().interpret(value, True)
        #     else:
        #         res = StepTools.__get_text_interpreter().interpret(value)
        # else:
        #     i_text_param = StepTools.__get_text_interpreter().interpret(text_param)
        #     if i_text_param == "None":
        #         res = None
        #     elif Converter.is_boolean(i_text_param):
        #         res = Converter.to_boolean(i_text_param)
        #     elif Converter.is_integer(i_text_param):
        #         res = int(i_text_param)
        #     elif Converter.is_float(i_text_param):
        #         res = float(i_text_param)
        #     elif StepTools._get_variable_manager().exists_variable(expression=i_text_param):
        #         res = StepTools._get_variable_manager().get_value(i_text_param)
        #     else:
        #         res = i_text_param
        res = StepTools._get_expression_evaluator().evaluate_expression(text_param, log_level=logging.TRACE)  # @UndefinedVariable
        if Tools.do_log_if_objects_are_different(text_param, res):
            logger.log(log_level, f"evaluate_scenario_parameter: [{text_param}] -> [{res}] (type: {type(res)})")
        return res
 
    @staticmethod
    def evaluate_list_scenario_parameter(text_param, param_name):
        res = StepTools.evaluate_scenario_parameter(text_param)
        if isinstance(res, str):
            regex = re.compile(r"^\s*\[(.*)\]\s*$")
            m = regex.match(res)
            if m:
                param_names = m.group(1).split(',')
                res = [StepTools.evaluate_scenario_parameter(pn.strip()) for pn in param_names]
            else:
                raise FunctionalException(f"Parameter '{param_name}' must be specified in a list format (ex: \"['Name1', 'Name2',...]\")")
        if Tools.do_log_if_objects_are_different(text_param, res):
            logger.debug("evaluate_list_scenario_parameter for parameter '{}': {} -> {}".format(param_name, text_param, res))
        return res
    
    @classmethod    
    def get_step_multiline_text(cls, *args, **kwargs):
        """
        Get the step multiline text interpreted.
        Note: parameter context can be the step context or the step object.
        """
        raise NotImplementedError
        
    
    @classmethod    
    def get_step_table(cls, *args, **kwargs):
        """
        Get the step table interpreted.
        Note: parameter context can be the step context or the step object.
        """
        raise NotImplementedError
        
    
    @classmethod
    def convert_step_table_2_value_table(cls, table):
        logger.trace(f"Converting step table to scenario table (step table = {table})")

        res = ValueTable()
        
        # Set body
        previous_row = None
        for row in cls._get_table_rows(table):
            srow = cls._convert_step_table_row_2_value_table_row(row, previous_row)
            res.add_row(row=srow)
            previous_row = srow
            
        # Manage one cell table referencing a variable containing a table
        res = cls._extract_inner_table_if_present(res)
            
        logger.debug(f"Converting step table to scenario table (step table = {table}) => {res}")
        return res
        
    @classmethod        
    def convert_step_table_2_value_table_with_header(cls, table) -> ValueTableWithHeader :
        logger.trace(f"Converting step table to scenario table with header (step table = {table})")
        if table is None:
            raise FunctionalException("This step needs a table")

        res = ValueTableWithHeader()
        
        # Set header
        res.header = cls._convert_step_table_header_2_table_header(cls._get_table_header(table))
        
        # Set body
        previous_row = None
        for row in cls._get_table_rows(table):
            srow = cls._convert_step_table_row_2_value_table_row(row, previous_row)
            res.add_row(row=srow)
            previous_row = srow
            
        # Manage one cell table referencing a variable containing a table
        res = cls._extract_inner_table_if_present(res)
            
        logger.debug(f"Converting step table to scenario table with header (step table = {table}) => {res}")
        return res

    @classmethod
    def _extract_inner_table_if_present(cls, table):
        res = table
        if isinstance(table, TableWithHeader):
            if res.nb_columns == 1 and res.nb_rows == 0:
                cell_content = res.header.get_cell(0).content
                if cls._get_variable_manager().exists_variable(variable_name=cell_content):
                    value = cls._get_variable_manager().get_variable_value(cell_content)
                    if isinstance(value, ValueTableWithHeader):
                        res = value
                    elif isinstance(value, TableWithHeader):
                        res = ValueTableConverter.convert_table_with_header_2_value_table_with_header(value)
                    elif isinstance(value, Table):
                        raise FunctionalException(f"The table in variable '{cell_content}' is expected to be with header")
        elif isinstance(table, Table):        
            if res.nb_columns == 1 and res.nb_rows == 1:
                value = res.get_row(0).get_cell(0).value
                if isinstance(value, ValueTable):
                    res = value
                elif isinstance(value, Table):
                    res = ValueTableConverter.convert_table_2_value_table(value)
                    
        if res != table:
            logger.debug(f"Extracted inner table: {table} => {res}")
        return res
            
    @classmethod
    def _convert_step_table_header_2_table_header(cls, header):
        logger.trace("Converting step table header to table header")

        res = TableRow()
        
        for hcell in header:
            cell = TableCell(hcell)
            
            # TODO: manage merged header cells
            
            res.add_cell(cell=cell)
           
        return res
        
    @classmethod
    def _convert_step_table_row_2_value_table_row(cls, row, previous_row):
        logger.trace("Converting behave table row to scenario table row")

        res = ValueTableRow()
        
        for cell in row:
            scell = ValueTableCell(cell)
            logger.trace("Converting cell [{}] -> scenario table cell (content=[{}] ; value_type='{}')".format(cell, scell.content, scell.value_type.name))
            
            # TODO: manage merged cells
            
            res.add_cell(cell=scell)
           
        return res
    

    @classmethod
    def has_table_header(cls, table):
        """
        Return True if scenario table has a header (for testing solution).
        For testing solution, table header names are all texts without quotes.
        """
        internal_header = cls._get_table_header(table)
        if internal_header is None:
            return False
        
        for cell in internal_header:
            scell = ValueTableCell(cell)
            if scell.value_type not in [ValueTypes.Symbol]:
                return False
        
        return True

    @classmethod
    def _has_table_header(cls, table):
        """Return True if scenario table has a header (for internal tool interpreting scenario text)."""
        return cls._get_table_header(table) is not None

    @classmethod
    def _get_table_header(cls, table):
        """Return iterable object on scenario table header (for internal tool interpreting scenario text)."""
        raise NotImplementedError()

    @classmethod
    def _get_table_rows(cls, table):
        """Return iterable object on scenario table rows (for internal tool interpreting scenario text)."""
        raise NotImplementedError()
        
    @classmethod
    def represent_step_table(cls, table, indent=0):
        res_list = []
        
        header = cls._get_table_header(table)
        if header:
            res_list.append("| " + " | ".join(header) + " |")
        
        for row in cls._get_table_rows(table):
            res_list.append("| " + " | ".join(row) + " |")
            
        return Tools.indent_string(indent, "\n".join(res_list))
        

    @classmethod
    def then_table_is(cls, table_obtained, step_table, raise_exception=True):
        is_with_header = TableManager.is_table_with_header(table_obtained)
        if is_with_header:
            table_expected = cls.convert_step_table_2_value_table_with_header(step_table)
        else:
            table_expected = cls.convert_step_table_2_value_table(step_table)
        
        if is_with_header:
            comparator = Table2ValueTable_WithHeaderComparator()
        else:
            comparator = Table2ValueTable_Comparator()
        try:
            return comparator.equals(table_obtained, table_expected, is_obtained_vs_expected=True, raise_exception=raise_exception)
        except FunctionalException as exc:
            raise FunctionalException(f"Tables are different (obtained = table 1 ; expected = table 2):\n{Tools.indent_string(4, exc.message)}") from exc

    @classmethod
    def then_table_contains(cls, table_obtained, step_table, raise_exception=True):
        is_with_header = TableManager.is_table_with_header(table_obtained)
        if is_with_header:
            table_expected = cls.convert_step_table_2_value_table_with_header(step_table)
        else:
            table_expected = cls.convert_step_table_2_value_table(step_table)
        
        if is_with_header:
            comparator = Table2ValueTable_WithHeaderComparator()
        else:
            comparator = Table2ValueTable_Comparator()
            
        try:
            return comparator.contains_rows(table_obtained, table_expected, is_obtained_vs_expected=True, raise_exception=raise_exception)
        except FunctionalException as exc:
            raise FunctionalException(f"Tables are different (obtained = table 1 ; expected = table 2):\n{Tools.indent_string(4, exc.message)}") from exc
    
