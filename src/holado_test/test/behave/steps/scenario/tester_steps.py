# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
import logging
from holado_core.common.tables.table import Table
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


def __get_report_manager():
    return SessionContext.instance().report_manager

def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()


__logger_is_configured = False
def __configure_logger():
    global __logger_is_configured
    if not __logger_is_configured:
        file_path = __get_report_manager().get_path("logs", "tester.log")
        SessionContext.instance().log_manager.add_file_handler(file_path, logger)
        __logger_is_configured = True

@Step(u"tester logs '(?P<msg>.*)'")
def step_impl(context, msg):
    __configure_logger()
    
    msg = StepTools.evaluate_scenario_parameter(msg)
    
    # Add multiline text if present
    text = BehaveStepTools.get_step_multiline_text(context, raise_exception_if_none=False, log_level=logging.TRACE)  # @UndefinedVariable
    if text is not None:
        msg += '\n' + text
    
    # Add table if present
    table = BehaveStepTools.get_step_table(context)
    if table:
        msg += '\n' + table.represent(4)
        
    logger.info(msg)
    
@Step(u"(?P<var_name>\w+) = tester represents (?P<param>.*)")
def step_impl(context, var_name, param):
    __configure_logger()
    
    value = StepTools.evaluate_scenario_parameter(param)
    if isinstance(value, Table):
        text = value.represent()
    else:
        text = str(value)
        
    __get_variable_manager().register_variable(var_name, text)
    


