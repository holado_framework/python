# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


import logging
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_core.common.block.function import Function
from holado_test.common.context.scenario_context import ScenarioContext
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


def __get_scenario_context() -> ScenarioContext:
    return SessionContext.instance().get_scenario_context()



@Given("at end of scenario, call steps")
def step_impl(context):
    steps = BehaveStepTools.get_step_multiline_text(context)
    
    func = Function(execute_steps, steps)
    __get_scenario_context().add_post_process(func)





