
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging

logger = logging.getLogger(__name__)



class DynamicTextManager(object):
    """
    @summary: Dynamic text manager.
    For every text it manages, it modifies the texts by adding a dynamic suffix.
    The suffix is computed from the timestamp of the moment when the text is registered to be dynamic.

    """
    
    def __init__(self, unique_value_manager):
        self.__texts = {}
        self.__unique_value_manager = unique_value_manager
        
    def set_text(self, key, value):
        self.__texts[key] = value
        
    def get(self, text, is_dynamic):
        if is_dynamic:
            return self.get_text(text)
        else:
            return text
        
    def get_text(self, text):
        if self.has_text(text):
            return self.__texts[text]
        elif len(text) > 0:
            return self.new_text(text)
        else:
            return text
        
    def has_text(self, text):
        return text in self.__texts
    
    def new_text(self, text):
        # Compute text with unique suffix
        suffix = self.__unique_value_manager.new_string(padding_length=6)
        res = text + suffix

        # Store dynamic text
        self.set_text(text, res)
        
        logger.debug("New dynamic text: [{}] -> [{}]".format(text, res))
        return res

