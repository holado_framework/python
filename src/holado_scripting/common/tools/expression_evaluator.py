
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import object
import logging
from holado_value.common.tools.value_types import ValueTypes
from holado_core.common.exceptions.technical_exception import TechnicalException
import re
import importlib
from holado_core.common.tools.converters.converter import Converter
import types

logger = logging.getLogger(__name__)


class ExpressionEvaluator(object):

    DYNAMIC_SYMBOL = u"%";
    UNIQUE_SYMBOL = u"#";
    NOT_APPLICABLE_SYMBOL = u"N/A";
    NONE_SYMBOL = u"None";

    def __init__(self):
        self.__dynamic_text_manager = None
        self.__text_interpreter = None
        self.__unique_value_manager = None
        self.__variable_manager = None
            
    def initialize(self, dynamic_text_manager, unique_value_manager, text_interpreter, variable_manager):
        self.__dynamic_text_manager = dynamic_text_manager
        self.__text_interpreter = text_interpreter
        self.__unique_value_manager = unique_value_manager
        self.__variable_manager = variable_manager
            
    def evaluate_expression(self, expression, unescape_string_method=None, do_interpret=True, do_eval=True, log_level=logging.DEBUG):
        value_type, value, value_to_eval = self.extract_expression_information(expression, unescape_string_method)
        if value_to_eval is not None:
            res = self.evaluate_expression_of_information(value_type, value_to_eval, do_interpret=do_interpret, do_eval=do_eval)
        else:
            res = value
        logger.log(log_level, f"Evaluate expression [{expression}] (do_interpret={do_interpret}, do_eval={do_eval}) => ({value_type}, {value}, '{value_to_eval}') => [{res}] (type: {type(res)})")
        return res

    def extract_expression_information(self, expression, unescape_string_method=None):
        return self.__extract_expression_information(expression, unescape_string_method=unescape_string_method)

    def __extract_expression_information(self, expression, unescape_string_method=None):
        if expression is None:
            return (ValueTypes.Null, None, None)
        elif isinstance(expression, bool):
            return (ValueTypes.Boolean, expression, None)
        elif isinstance(expression, int):
            return (ValueTypes.Integer, expression, None)
        elif isinstance(expression, float):
            return (ValueTypes.Float, expression, None)
        elif isinstance(expression, str):
            return self.__extract_expression_information_str(expression, unescape_string_method = unescape_string_method)
        else:
            return (ValueTypes.Generic, expression, None)

    def __extract_expression_information_str(self, expression, unescape_string_method=None):
        if unescape_string_method is None:
            from holado_test.scenario.step_tools import StepTools
            unescape_string_method = StepTools.unescape_string
            
        try:
            expr_content = expression.strip()

            # Manage usual types    
            if expr_content == ExpressionEvaluator.NONE_SYMBOL:
                return (ValueTypes.Null, None, expr_content)
            elif expr_content == ExpressionEvaluator.NOT_APPLICABLE_SYMBOL:
                return (ValueTypes.NotApplicable, None, expr_content)
            elif Converter.is_boolean(expr_content):
                return (ValueTypes.Boolean, Converter.to_boolean(expr_content), None)
            elif Converter.is_integer(expr_content):
                return (ValueTypes.Integer, int(expr_content), None)
            elif Converter.is_float(expr_content):
                return (ValueTypes.Float, float(expr_content), None)
            
            # Manage string or dynamic string
            is_dynamic = expr_content.endswith(ExpressionEvaluator.DYNAMIC_SYMBOL)
            is_unique = expr_content.endswith(ExpressionEvaluator.UNIQUE_SYMBOL)
            if is_dynamic:
                is_string = expr_content.startswith("'") and expr_content.endswith("'" + ExpressionEvaluator.DYNAMIC_SYMBOL)
            elif is_unique:
                is_string = expr_content.startswith("'") and expr_content.endswith("'" + ExpressionEvaluator.UNIQUE_SYMBOL)
            else:
                is_string = expr_content.startswith("'") and expr_content.endswith("'")
            if is_string:
                if is_dynamic:
                    text = expr_content[1:-2]
                    if unescape_string_method is not None:
                        text = unescape_string_method(text)
                    return (ValueTypes.DynamicString, None, text)
                elif is_unique:
                    text = expr_content[1:-2]
                    if unescape_string_method is not None:
                        text = unescape_string_method(text)
                    return (ValueTypes.UniqueString, None, text)
                else:
                    text = expr_content[1:-1]
                    if unescape_string_method is not None:
                        text = unescape_string_method(text)
                    return (ValueTypes.String, None, text)
                
            # # Manage evaluated values
            # try:
            #     value_evaluated = eval(expr_content)
            #     evaluated = True
            # except (SyntaxError, NameError, ValueError):
            #     evaluated = False
            # if evaluated:
            #     if isinstance(value_evaluated, bool):
            #         value_type = ValueTypes.Boolean
            #     elif isinstance(value_evaluated, int):
            #         value_type = ValueTypes.Integer
            #     elif isinstance(value_evaluated, float):
            #         value_type = ValueTypes.Float
            #     elif isinstance(value_evaluated, str):
            #         value_type = ValueTypes.String
            #     else:
            #         value_type = ValueTypes.Generic
            #     return (value_type, value_evaluated)
                    
            # Else consider content as a symbol
            return (ValueTypes.Symbol, None, expr_content)
        except Exception as exc:
            raise TechnicalException(f"Error while extracting expression information in expression [{expression}] (type: {type(expression)})") from exc
        
    def evaluate_expression_of_information(self, value_type, value, raise_on_variable_expression_error=True, do_interpret=True, do_eval=True):
        logger.trace(f"Evaluating expression of information ({value_type.name}, {value})")
        res = self.__evaluate_expression_of_information(value_type, value, raise_on_variable_expression_error, do_interpret, do_eval)
        logger.debug(f"Evaluate expression of information ({value_type.name}, {value}) => [{res}] (type: {type(res)})")
        return res
        
    def __evaluate_expression_of_information(self, value_type, value, raise_on_variable_expression_error=True, do_interpret=True, do_eval=True):
        if value_type in [ValueTypes.Null]:
            return None
        elif value_type in [ValueTypes.Boolean, ValueTypes.Integer, ValueTypes.Float, ValueTypes.Generic]:
            return value
        elif value_type in [ValueTypes.String, ValueTypes.DynamicString, ValueTypes.UniqueString]:
            return self.__evaluate_expression_of_information_string(value_type, value, do_interpret)
        elif value_type in [ValueTypes.Symbol]:
            return self.__evaluate_expression_of_information_symbol(value, raise_on_variable_expression_error, do_interpret, do_eval)
        else:
            return value
        
    def __evaluate_expression_of_information_string(self, value_type, value, do_interpret=True):
        res = self.__text_interpreter.interpret(value) if do_interpret else value
        if isinstance(res, bytes):
            res = res.decode('utf-8')
        elif not isinstance(res, str):
            res = str(res)
            
        if value_type == ValueTypes.DynamicString:
            return self.__dynamic_text_manager.get(res, True)
        elif value_type == ValueTypes.UniqueString:
            return res + self.__unique_value_manager.new_string(padding_length=6)
        else:
            return res
        
    def __evaluate_expression_of_information_symbol(self, value, raise_on_variable_expression_error=True, do_interpret=True, do_eval=True):
        # Replace explicit interpret parts
        locals_ = {}
        if do_interpret:
            if do_eval:
                res, locals_ = self.__text_interpreter._interpret_sections(value)
            else:
                res = self.__text_interpreter.interpret(value)
        else:
            res = value
        
        # Evaluate variable expression
        if isinstance(res, str) and len(res) > 0:
            if self.__variable_manager.exists_variable(res):
                return self.__variable_manager.get_variable_value(res)
            elif self.__variable_manager.is_variable_expression(res):
                try:
                    result = self.__variable_manager.eval_variable_expression(res)
                except Exception as exc:
                    if raise_on_variable_expression_error:
                        raise exc
                    else:
                        logger.error(f"Error while evaluating variable expression [{res}]: {exc}")
                else:
                    res = result
                    # If symbol is a variable expression, don't evaluate the result of the variable
                    return res
        
        # Evaluate expression
        if do_eval and (isinstance(res, str) or isinstance(res, bytes)):
            while True:
                try:
                    result = None
                    if isinstance(res, str):
                        # Try first by replacing double semicolon
                        try:
                            txt_to_eval = res.replace("{{", "{").replace("}}", "}")
                            result = eval(txt_to_eval, globals(), locals_)
                        except SyntaxError as exc:
                            # Retry without replacing double semicolon
                            pass
                        
                    if result is None:
                        result = eval(res, globals(), locals_)
                except NameError as exc:
                    # logger.trace(f"Error while expression evaluation:\n    exception: {Tools.represent_exception(exc)}\n    globals: {globals()}\n    locals: {locals_}")
                    # In case the name concerns a library, try to import the library
                    m = re.match("name '(.*)' is not defined", str(exc))
                    is_imported = False
                    if m:
                        module_name = m.group(1)
                        # if module_name in sys.modules:
                        #     del sys.modules[module_name]
                        # if module_name in sys.modules:
                        #     raise TechnicalException(f"Module '{module_name}' is already imported. List of imported modules: {sys.modules}")
                        if module_name in locals_:
                            raise TechnicalException(f"Module '{module_name}' is already imported (locals_: {locals_})")
                        
                        logger.trace(f"Importing module '{module_name}'")
                        try:
                            module = importlib.import_module(module_name)
                            locals_[module_name] = module
                        except Exception as exc2:
                            logger.trace(f"Failed to import module '{module_name}': {exc2}")
                        else:
                            logger.trace(f"Imported module '{module_name}' for expression evaluation")
                            is_imported = True
                            
                    # If no import is done
                    if not is_imported:
                        logger.debug(f"Error while evaluating expression [{res}]: {exc}")
                        break
                except Exception as exc:
                    logger.debug(f"Error while evaluating expression [{res}]: [{type(exc)}] {exc}")
                    break
                else:
                    if not isinstance(result, types.ModuleType):
                        res = result
                    break
                
        # logger.debug(f"Evaluate symbol [{value}] => [{res}] (type: {type(res)})")
        return res
    
    def extract_string_value(self, expression, unescape_string_method=None):
        value_type, _, value_str = self.extract_expression_information(expression, unescape_string_method)
        if value_type in [ValueTypes.String, ValueTypes.DynamicString, ValueTypes.UniqueString]:
            res = value_str
        else:
            res = expression
        logger.debug(f"Extract string value [{expression}] => ({value_type}, '{value_str}') => [{res}] (type: {type(res)})")
        return res

