
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.exceptions.functional_exception import FunctionalException
import re
from holado.common.handlers.object import Object, DeleteableObject
from holado_core.common.tools.tools import Tools

logger = logging.getLogger(__name__)


class VariableManager(DeleteableObject):
    """
    Manage variables as couples of (name, value).
    
    Variable names can contain any character except: ".", "["
    """

    def __init__(self, parent=None):
        super().__init__("variable manager")
        
        self.__parent = parent
        self.__unique_value_manager = None
        self.__variables = {}
        
    def initialize(self, unique_value_manager):
        self.__unique_value_manager = unique_value_manager
    
    @property
    def parent(self):
        return self.__parent
    
    def _delete_object(self):
        # Delete all variables
        names = set(self.__variables.keys())
        logger.debug(f"Removing {len(names)} variables: {names}")
        for index, name in enumerate(names):
            if name in self.__variables:
                value = self.__variables[name]
                logger.debug(f"Removing variable {index+1}/{len(names)}: '{name}' (type: {type(value)})")
                if isinstance(value, DeleteableObject):
                    try:
                        value.delete_object()
                    except Exception:
                        logger.exception(f"Catched exception while deleting object '{name}' (type: {type(value)})")
                    else:
                        logger.debug(f"Deleted variable '{name}' (type: {type(value)})")
                
                del self.__variables[name]
        logger.debug(f"Finished to remove {len(names)} variables: {names}")
                
        # # Clear variables
        # self.__variables.clear()
        
    def new_local_variable_name(self):
        return f"__VAR{self.__unique_value_manager.new_string(do_log=False)}__"
    
    def new_variable_name(self):
        res = None
        while True:
            res = self.new_local_variable_name()
            if not self.exists_variable(variable_name=res):
                break
        return res
    
    def register_variable(self, variable_name, value, accept_expression=True):
        logger.trace(f"Setting variable: {variable_name}=[{value}] (type: {type(value)})...")
        
        names = variable_name.rsplit('.', 1)
        if accept_expression and len(names) > 1:
            if not self.exists_variable(expression=names[0]):
                self.register_variable(names[0], Object())
            var = self.get_value(names[0], through_patent=False)
            setattr(var, names[1], value)
            logger.info(f"Set variable expression: {variable_name}=[{value}] (type: {type(value)})")
        else:
            self.__variables[variable_name] = value
            logger.info(f"Set variable: {variable_name}=[{value}] (type: {type(value)})")

    def get_variable_value(self, variable_name, attributes = [], through_parent=True):
        # Get variable value
        if self.exists_variable(variable_name, through_parent=False):
            res = self.__variables[variable_name]
        elif through_parent and self.parent:
            return self.parent.get_variable_value(variable_name, attributes)
        else:
            raise FunctionalException("Variable '{}' has not been found. Missing initialisation ?".format(variable_name))
        
        # If needed, call attributes
        for attr in attributes:
            res = getattr(res, attr)
        
        return res

    def get_value(self, expression, through_patent=True):
        res = None
        
        if self.is_variable_expression(expression, through_parent=through_patent):
            try:
                res = self.eval_variable_expression(expression, through_parent=through_patent)
            except (SyntaxError, NameError):
                raise FunctionalException("Expression '{}' is not evaluable. Is an initialization missing ?".format(expression))
        else:
            res = self.get_variable_value(expression, through_parent=through_patent)
        
        return res

    def set_value(self, expression, value, through_parent=True):
        if self.is_variable_expression(expression, through_parent=through_parent):
            try:
                self.exec_set_variable_expression(expression, value, through_parent=through_parent)
            except (SyntaxError, NameError):
                raise FunctionalException("Expression '{}' is not evaluable. Is an initialization missing ?".format(expression))
        else:
            self.register_variable(expression, value)

    def exists_variable(self, variable_name=None, expression=None, through_parent=True):
        if expression is not None:
            expression = expression.strip()
            m_operator = re.search("[.[]", expression)
            if m_operator:
                variable_name = expression[:m_operator.start()]
                # Return directly False if found operator is not an operator 
                if m_operator.group(0) not in ['.', '[']:
                    return False
            else:
                variable_name = expression
        
        res = variable_name in self.__variables
        if not res and through_parent and self.parent:
            res = self.parent.exists_variable(variable_name=variable_name)
        return res
    
    def is_variable_expression(self, expression, through_parent=True):
        try:
            var_name, expr_rest = self.__extract_expression_name(expression)
            return len(var_name) > 0 and self.exists_variable(var_name, through_parent=through_parent) and len(expr_rest) > 0
        except SyntaxError:
            return False
    
    def eval_variable_expression(self, expression, through_parent=True):
        var_name, expr_rest = self.__extract_expression_name(expression)
        var_value = self.get_variable_value(var_name, through_parent=through_parent)
        new_var_name = f"__TMP_VAR_{self.__regular_variable_name(var_name)}__"
        locals()[new_var_name] = var_value
        new_expr = f"{new_var_name}{expr_rest}"
        try:
            return eval(new_expr)
        except Exception as exc:
            try:
                var_repr = Tools.represent_object(var_value)
            except:
                var_repr = str(var_value)
            Tools.raise_same_exception_type(exc, f"{exc}\nVariable '{var_name}': {var_repr}", add_from=True)
    
    def exec_set_variable_expression(self, expression, value, through_parent=True):
        var_name, expr_rest = self.__extract_expression_name(expression)
        var_value = self.get_variable_value(var_name, through_parent=through_parent)
        new_var_name = f"__TMP_VAR_{self.__regular_variable_name(var_name)}__"
        locals()[new_var_name] = var_value
        
        if isinstance(value, str):
            value_str = value.replace("'", "\'")
            new_expr = f"{new_var_name}{expr_rest}='{value_str}'"
        else:   #TODO EKL: raise exception for non primitive types
            new_expr = f"{new_var_name}{expr_rest}={value}"
        exec(new_expr)
    
    def __regular_variable_name(self, name):
        return "".join([c if re.match('\w', c) else '_' for c in name])
    
    # def set_variable_expression(self, expression, value):
    #     var_name, expr_rest = self.__extract_expression_name(expression)
    #     if len(expr_rest) == 0:
    #         self.register_variable(expression, value)
    #     else:
    #         var_value = self.get_variable_value(var_name)
    #
    #     while len(expr_rest) > 0:
    #

    
    def __extract_expression_name(self, expression):
        regex_name = re.compile("^([^.[]+)(.*)")
        match = regex_name.match(expression)
        if not match:
            raise SyntaxError(f"Expression [{expression}] is not a variable expression")
        return match.group(1), match.group(2)
        
        
        
        
        
