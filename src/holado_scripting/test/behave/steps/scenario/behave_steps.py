# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


import logging
from holado.common.context.session_context import SessionContext
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_test.scenario.step_tools import StepTools
# In this specific step file, it's the given/when/then/step of behave that are used, not custom ones
# from behave import *  # @UnusedWildImport
from holado_test.behave.behave import *
from holado_core.common.block.scope_steps import ScopeSteps, ScopeForSteps,\
    ScopeWhileSteps
from holado_core.common.tools.converters.converter import Converter
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_block_manager():
    return __get_scenario_context().block_manager

def __get_scope_manager():
    return __get_scenario_context().scope_manager

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()



def __verify_context_doesnt_already_expect_an_exception(context):
    if context.expected_exception_str:
        msg = 'Already expecting failure on exception {}'.format(context.expected_exception_str)
        # context.expected_exception_str = None
        # context.expected_exception_pattern = None
        # context.in_expected_exception_step = False
        raise FunctionalException(msg)
    elif context.expected_exception_pattern:
        msg = "Already expecting failure on exception matching '{}'".format(context.expected_exception_pattern)
        # context.expected_exception_str = None
        # context.expected_exception_pattern = None
        # context.in_expected_exception_step = False
        raise FunctionalException(msg)

def __set_expected_exception(context, expected_exception_str = None, expected_exception_pattern=None):
    context.expected_exception_str = expected_exception_str
    context.expected_exception_pattern = expected_exception_pattern
    context.in_expected_exception_step = True
    context.expected_exception_step_level = __get_scope_manager().scope_level("steps")

@Given('next step shall fail on exception (?!matching)(?P<exc_str>.*)')
def step_impl(context, exc_str):
    exc_str = __get_text_interpreter().interpret(exc_str)
    
    __verify_context_doesnt_already_expect_an_exception(context)
    __set_expected_exception(context, expected_exception_str = exc_str)

@Given("next step shall fail on exception matching '(?P<exc_str>.*)'")
def step_impl(context, exc_str):
    exc_str = __get_text_interpreter().interpret(exc_str)
    exc_str_unescaped = StepTools.unescape_string(exc_str)
    
    __verify_context_doesnt_already_expect_an_exception(context)
    __set_expected_exception(context, expected_exception_pattern = exc_str_unescaped)

@Given('next step shall fail on following exception(?! matching)')
def step_impl(context):
    exc_str = BehaveStepTools.get_step_multiline_text(context)
    
    __verify_context_doesnt_already_expect_an_exception(context)
    __set_expected_exception(context, expected_exception_str = exc_str)

@Given('next step shall fail on following exception matching')
def step_impl(context):
    exc_str = BehaveStepTools.get_step_multiline_text(context)
    exc_str_unescaped = StepTools.unescape_string(exc_str)
    
    __verify_context_doesnt_already_expect_an_exception(context)
    __set_expected_exception(context, expected_exception_pattern = exc_str_unescaped)



###################################################################################################
##  Steps managing loops
###################################################################################################
## /!\ While @For decorator is not working (cf testing_behave/scenario/behave.py), use instead "@Given for" 


#################################################
##  Steps to manage "for" loops

# @For('(?P<iter_var_name>\w+) in (?P<iterable_var_name>\w+):')
@Given('for (?P<iter_var_name>\w+) in (?P<iterable_expression>.+):', accept_scope_in_define=False)
def step_impl(context, iter_var_name, iterable_expression):
    if __get_block_manager().is_in_define:
        __get_block_manager().increase_define_level()
    else:
        iterable = StepTools.evaluate_scenario_parameter(iterable_expression)
        
        scope_name = context.step.name
        
        if __get_block_manager().has_scope(scope_name):
            __get_block_manager().delete_scope(scope_name)
        __begin_define_scope_for_steps(scope_name, iter_var_name, iterable)
        
        context.define_scope_name = scope_name

def __begin_define_scope_for_steps(scope_name, iter_var_name, iterable):
    lambda_scope_type = lambda ID, name: ScopeForSteps(ID=ID, name=name, iter_var_name=iter_var_name, iterable=iterable, variable_manager=__get_variable_manager(), scope_manager=__get_scope_manager())
    __get_block_manager().begin_define_scope(scope_name, lambda_scope_type)
    
# @For('end')
@Given('end for', accept_scope_in_define=False)
def step_impl(context):
    # TODO EKL: add verification that sub define is a 'for'
    if __get_block_manager().is_in_sub_define:
        __get_block_manager().decrease_define_level()
    else:
        scope_name = context.define_scope_name
        del context.define_scope_name
        __get_block_manager().end_define_scope(scope_name)
        __get_block_manager().process_scope(scope_name)



#################################################
##  Steps to manage "while" loops

@Given('while (?P<condition_expression>.+):', accept_scope_in_define=False)
def step_impl(context, condition_expression):
    if __get_block_manager().is_in_define:
        __get_block_manager().increase_define_level()
    else:
        scope_name = context.step.name
        
        if __get_block_manager().has_scope(scope_name):
            __get_block_manager().delete_scope(scope_name)
        __begin_define_scope_while_steps(scope_name, condition_expression)
        
        context.define_scope_name = scope_name

def __begin_define_scope_while_steps(scope_name, condition_expression):
    lambda_scope_type = lambda ID, name: ScopeWhileSteps(ID=ID, name=name, condition_expression=condition_expression, scope_manager=__get_scope_manager())
    __get_block_manager().begin_define_scope(scope_name, lambda_scope_type)
    
@Given('end while', accept_scope_in_define=False)
def step_impl(context):
    # TODO EKL: add verification that sub define is a 'while'
    if __get_block_manager().is_in_sub_define:
        __get_block_manager().decrease_define_level()
    else:
        scope_name = context.define_scope_name
        del context.define_scope_name
        __get_block_manager().end_define_scope(scope_name)
        __get_block_manager().process_scope(scope_name)



#################################################
##  Steps to manage "if / else if /else"

@Given("if (?P<condition_expression>.+):", accept_condition=False)
def step_impl(context, condition_expression):
    __get_scope_manager().increase_condition_level()
    cond = StepTools.evaluate_scenario_parameter(condition_expression)
    if isinstance(cond, bool) and cond or Converter.is_boolean(cond) and Converter.to_boolean(cond):
        __get_scope_manager().enter_in_valid_condition()

@Given("else if (?P<condition_expression>.+):", accept_condition=False)
def step_impl(context, condition_expression):
    __get_scope_manager().leave_condition()
    if not __get_scope_manager().had_valid_condition():
        cond = StepTools.evaluate_scenario_parameter(condition_expression)
        if isinstance(cond, bool) and cond or Converter.is_boolean(cond) and Converter.to_boolean(cond):
            __get_scope_manager().enter_in_valid_condition()

@Given("else:", accept_condition=False)
def step_impl(context):
    __get_scope_manager().leave_condition()
    if not __get_scope_manager().had_valid_condition():
        __get_scope_manager().enter_in_valid_condition()

@Given("end if", accept_condition=False)
def step_impl(context):
    # TODO EKL: add verification that sub define is a 'if'
    __get_scope_manager().decrease_condition_level()



#################################################
##  Steps to define functions

@Given("define function (?P<function_name>.+):", accept_scope_in_define=False)
def step_impl(context, function_name):
    if __get_block_manager().is_in_define:
        __get_block_manager().increase_define_level()
    else:
        scope_name = StepTools.evaluate_string_parameter(function_name)
    
        context.define_scope_name = scope_name
        
        if __get_block_manager().has_scope(scope_name):
            __get_block_manager().delete_scope(scope_name)
        __begin_define_scope_steps(scope_name)

def __begin_define_scope_steps(scope_name):
    lambda_scope_type = lambda ID, name: ScopeSteps(ID=ID, name=name)
    __get_block_manager().begin_define_scope(scope_name, lambda_scope_type)

@Given('end function', accept_scope_in_define=False)
def step_impl(context):
    if __get_block_manager().is_in_sub_define:
        __get_block_manager().decrease_define_level()
    else:
        __get_block_manager().end_define_scope(context.define_scope_name)

@Step("call function (?P<function_name>.+)")
def step_impl(context, function_name):
    scope_name = StepTools.evaluate_string_parameter(function_name)
    __get_block_manager().process_scope(scope_name)


