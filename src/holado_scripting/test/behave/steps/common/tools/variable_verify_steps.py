# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_core.common.tables.table_manager import TableManager
from holado_value.common.tables.comparators.table_2_value_table_comparator import Table2ValueTable_Comparator
from holado_core.common.tools.comparators.comparator import CompareOperator
from holado_core.common.tools.comparators.datetime_comparator import DatetimeComparator
from holado_core.common.tables.table import Table
from holado_core.common.tools.comparators.object_comparator import ObjectComparator
from holado_core.common.tables.table_with_header import TableWithHeader
from holado_value.common.tables.value_table_with_header import ValueTableWithHeader
from holado_value.common.tables.comparators.table_2_value_table_with_header_comparator import Table2ValueTable_WithHeaderComparator
from holado_core.common.tables.comparators.table_with_header_comparator import TableWithHeaderComparator
from holado_value.common.tables.value_table import ValueTable
from holado_core.common.tables.comparators.table_comparator import TableComparator
from holado_core.common.tools.comparators.string_comparator import StringComparator
import logging
from holado_core.common.exceptions.verify_failure_exception import VerifyFailureException
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_unique_value_manager():
    return SessionContext.instance().unique_value_manager

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()




# @Then(u'\'{object_string}\' is {type_name:S}')
# def step_impl(context, object_string, type_name):
#     obj = __get_text_interpreter().interpret(object_string)
# 
#     # Get class type
#     class_ = Tools.get_class_for_type_name(type_name)
#     
#     if isinstance(obj, class_):
#         if isinstance(obj, basestring):
#             # Verify scenario table structure
#             if len(context.table.headings) != 1 or len(context.table.rows) != 1:
#                 raise FunctionalException("For a string, scenario table must be a one column table with header and only one row")
#             
#             # Verify strings
#             cell = ValueTableCell(context.table.rows[0].cells[0])
#             expected = cell.value
#             if obj != expected:
#                 raise FunctionalException("String value is wrong (expected: '{}' ; obtained: '{}')".format(expected, obj))
#         else:
#             raise NotImplementedError(u"Check content of type '{}'".format(Tools.get_object_class_fullname(obj)))
#     else:
#         raise FunctionalException("Object is of type '{}' (expected type: '{}')".format(Tools.get_object_class_fullname(obj), type_name))



#@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) == (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+)")
@Then(u"(?P<value_1>.+) == (?P<value_2>(?:(?! \(as ).)+)")
def step_impl(context, value_1, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)

    if isinstance(val1, Table) and isinstance(val2, Table):
        if isinstance(val1, TableWithHeader) and isinstance(val2, TableWithHeader):
            if isinstance(val2, ValueTableWithHeader):
                comparator = Table2ValueTable_WithHeaderComparator()
            else:
                comparator = TableWithHeaderComparator()
        else:
            if isinstance(val2, ValueTable):
                comparator = Table2ValueTable_Comparator()
            else:
                comparator = TableComparator()
    elif isinstance(val1, str) and isinstance(val2, str):
        comparator = StringComparator()
    else:
        comparator = ObjectComparator()
        
    comparator.equals(val1, val2)

@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) != (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+)")
def step_impl(context, value_1, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)

    comparator = ObjectComparator()
    comparator.compare(val1, CompareOperator.Different, val2)

@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) < (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+)")
def step_impl(context, value_1, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)

    comparator = ObjectComparator()
    comparator.compare(val1, CompareOperator.Inferior, val2)

@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) <= (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+)")
def step_impl(context, value_1, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)

    comparator = ObjectComparator()
    comparator.compare(val1, CompareOperator.InferiorOrEqual, val2)

@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) > (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+)")
def step_impl(context, value_1, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)

    comparator = ObjectComparator()
    comparator.compare(val1, CompareOperator.Superior, val2)

@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) >= (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+)")
def step_impl(context, value_1, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)

    comparator = ObjectComparator()
    comparator.compare(val1, CompareOperator.SuperiorOrEqual, val2)

@Then(u"(?P<value_1>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) (?P<operator>==|!=|<|<=|>|>=) (?P<value_2>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) \(as datetimes\)")
def step_impl(context, value_1, operator, value_2):  # @DuplicatedSignature
    val1 = StepTools.evaluate_scenario_parameter(value_1)
    val2 = StepTools.evaluate_scenario_parameter(value_2)
    operator = CompareOperator(operator)

    comparator = DatetimeComparator()
    comparator.compare(val1, operator, val2)



@Then(u"(?P<value>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) is list")
def step_impl(context, value):  # @DuplicatedSignature
    val = StepTools.evaluate_scenario_parameter(value)
    if not isinstance(val, list):
        raise VerifyFailureException(f"Value [{val}] (from expression [{value}]) is not a list (type: {type(val)})")
    
    if hasattr(context, "table") and context.table is not None:
        table = BehaveStepTools.convert_step_table_2_value_table(context.table)
        obtained = TableManager.convert_list_2_column_table(val)
        comparator = Table2ValueTable_Comparator()
        comparator.equals(obtained, table)

@Then(u"(?P<value>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) is empty list")
def step_impl(context, value):  # @DuplicatedSignature
    val = StepTools.evaluate_scenario_parameter(value)
    if not isinstance(val, list):
        raise VerifyFailureException(f"Value [{val}] (from expression [{value}]) is not a list (type: {type(val)})")
    
    if len(val) > 0:
        raise VerifyFailureException(f"List is not empty, it contains {len(val)} elements: [{val}] (from expression [{value}])")

@Then(u"(?P<value>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) is in (?P<iterable_str>.+)")
def step_impl(context, value, iterable_str):  # @DuplicatedSignature
    val = StepTools.evaluate_scenario_parameter(value)
    iterable = StepTools.evaluate_scenario_parameter(iterable_str)
    res = val in iterable
    if not res:
        raise VerifyFailureException(f"Value [{val}] is not in {iterable}")


# @Then(u"dictionary (?P<value>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) is")
# def step_impl(context, value):  # @DuplicatedSignature
#     val = StepTools.evaluate_scenario_parameter(value)
#     if not isinstance(val, dict):
#         raise VerifyFailureException(f"Value [{val}] (from expression [{value}]) is not a dictionary (type: {type(val)})")
#
#     if hasattr(context, "table") and context.table is not None:
#         table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
#         ValueTableManager.verify_table_is_x_table(table, "Key", "Value")
#         table.sort(names=['Key'])
#
#         obtained = TableManager.convert_dict_2_name_value_table(val)
#         obtained.sort(names=['Name'])
#
#         comparator = Table2ValueTable_Comparator()
#         comparator.equals(obtained, table)

@Then(u"(?P<value>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) is text")
def step_impl(context, value):  # @DuplicatedSignature
    obtained = StepTools.evaluate_scenario_parameter(value)
    if not isinstance(obtained, str):
        raise VerifyFailureException(f"Value [{obtained}] (from expression [{value}]) is not a text (type: {type(obtained)})")
    
    text = BehaveStepTools.get_step_multiline_text(context)
    if text is not None:
        comparator = StringComparator()
        comparator.equals(obtained, text)



