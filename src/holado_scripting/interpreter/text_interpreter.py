
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from holado_core.common.exceptions.verify_failure_exception import VerifyFailureException
import logging
from holado_scripting.interpreter.functions.verify_function_extract_in import VerifyFunctionExtractIn
from holado_scripting.interpreter.functions.function_dynamic_value import FunctionDynamicValue
from builtins import object
import re
from holado_scripting.interpreter.exceptions.not_existing_function_text_interpreter_exception import NotExistingFunctionTextInterpreterException
from holado_scripting.interpreter.functions.function_cast import FunctionCast
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_scripting.interpreter.functions.verify_function_match_pattern import VerifyFunctionMatchPattern
from holado_scripting.interpreter.functions.function_to_base_64 import FunctionToBase64
from holado_scripting.interpreter.functions.function_to_hex import FunctionToHex
from holado_scripting.interpreter.functions.function_hex_to_int import FunctionHexToInt
from holado_scripting.interpreter.functions.function_to_bytes import FunctionToBytes
from holado_scripting.interpreter.functions.function_hex_to_bytes import FunctionHexToBytes
from holado_core.common.tools.tools import Tools

logger = logging.getLogger(__name__)


class TextInterpreter(object):

    def __init__(self):
        self.__variable_manager = None
        self.__expression_evaluator = None
        
        self.__managed_functions = {}
        self.__managed_verify_functions = {}
        
        # self.__regex_to_be_interpreted = re.compile(r"^\$\{((?:(?!\$\{)[^\}])*)\}$")
        self.__regex_to_be_interpreted = re.compile(r"^\$\{(.*)\}$")
        # self.__regex_named_group = re.compile(r"\(\?<([a-zA-Z][a-zA-Z0-9]*)>")
        self.__regex_function = re.compile(r"^(\w+)\((.*)\)$")
        self.__regex_to_be_interpreted_with_function = re.compile(r"^\$\{(\w+)\((.*)\)\}$")
        
        self.__process_log_match_failure = True
        
    def initialize(self, var_manager, dynamic_text_manager, expression_evaluator):
        self.__variable_manager = var_manager
        self.__expression_evaluator = expression_evaluator
        
        self.__register_functions(dynamic_text_manager)

    def is_to_interpret(self, text):
        # r_match = self.__regex_to_be_interpreted.match(text)
        # if r_match:
        #     return True
        #
        # indexes = self.__search_interpret_section(text)
        # return indexes is not None
        section_indexes = self.__search_all_interpret_sections(text)
        return len(section_indexes) > 0
    
    def interpret(self, str_to_interpret, log_level=logging.DEBUG):
        """
        @summary: Interpret given text by replacing all occurrences of "${...}" by corresponding values.
        @param str_to_interpret: String - Text to interpret
        @return: Interpreted result
        """
        
        if str_to_interpret is None:
            return None
        
        logger.trace("Interpreting '{}'...".format(str_to_interpret))
        
        # Interpret and get result as object
        res, _ = self.__interpret(str_to_interpret)

        if Tools.do_log_if_objects_are_different(str_to_interpret, res):
            logger.log(log_level, "Interpreting '{}' -> [{}] (type: {})".format(str_to_interpret, res, type(res)))
            
        return res

    def _interpret_sections(self, str_to_interpret, log_level=logging.DEBUG):
        """
        @summary: Interpret given text by replacing all occurrences of "${...}" by variables containing corresponding values.
        @param str_to_interpret: String - Text to interpret
        @return: Interpreted result
        """
        
        if str_to_interpret is None:
            return None
        
        logger.trace("Interpreting sections in '{}'...".format(str_to_interpret))
        
        # Interpret and get result as object
        res, locals_ = self.__interpret(str_to_interpret, replace_sections_by_variables=True)

        if Tools.do_log_if_objects_are_different(str_to_interpret, res):
            logger.log(log_level, "Interpreting sections in '{}' -> [{}] (type: {})".format(str_to_interpret, res, type(res)))
            
        return res, locals_

    def verify(self, obtained, expected, do_check_case = True, raise_exception = True):
        res = True
        
        logger.trace(f"Verifying obtained '{obtained}' with expected '{expected}'...")
        
        # Check if expected is a verify function
        if expected is not None:
            result = self.__evaluate_function_verify(expected, obtained, raise_exception)
            if result is not None:
                return result
            
        # Interpret expected
        interpreted_expected = self.interpret(expected)
        
        # Verify
        if interpreted_expected != obtained:
            # Build message
            msg = "Match failure: '{}' is different from '{}'".format(obtained, interpreted_expected)
            if interpreted_expected != expected:
                msg += " (from '{}')".format(expected)
                
            # log
            self._log_match_failure(msg)
            
            # Manage raise
            if raise_exception:
                raise VerifyFailureException(msg)
            else:
                res = False
            
        logger.debug(f"Verify obtained '{obtained}' with expected '{expected}' => [{res}]")
        return res

    def __interpret(self, str_to_interpret, replace_sections_by_variables=False):
        res = None
        locals_ = {}
        
        # If the string is to be interpreted
        section_indexes = self.__search_all_interpret_sections(str_to_interpret)
        if (0,len(str_to_interpret)) in section_indexes:
            r_match = self.__regex_to_be_interpreted.match(str_to_interpret)
            if r_match:
                str_under_interpret = r_match.group(1).strip()
                res = self.__interpret_text(str_under_interpret)
                # Text is not interpretable ; expect it will be the case later
                if res == str_under_interpret:
                    res = str_to_interpret
                elif isinstance(res, str) and self.__has_verify_function(res):
                    res = f"${{{res}}}"
            else:
                raise TechnicalException(f"Failed to find interpret section whereas its indexes were found")
        else:
            res, locals_ = self.__find_and_interpret_sections(str_to_interpret, replace_sections_by_variables=replace_sections_by_variables)
            
        return res, locals_
    
    def __interpret_text(self, text):
        # Manage functions
        if self.__has_function(text):
            return self.__evaluate_function(text)
        
        # Manage evaluation
        return self.__expression_evaluator.evaluate_expression(text, log_level=logging.TRACE)  # @UndefinedVariable
    
    def __find_and_interpret_sections(self, str_to_interpret, replace_sections_by_variables=False):
        res = str_to_interpret
        locals_ = {}
        
        offset = 0
        excluded_indexes = []
        while True:
            indexes = self.__search_interpret_section(res, offset, excluded_indexes)
            if indexes is None:
                # Try again from beginning (in case of nested sections)
                indexes = self.__search_interpret_section(res, 0, excluded_indexes)
                if indexes is not None:
                    offset = 0
                else:
                    # No more section to replace
                    break
                                
            ind_beg, ind_end = indexes
            sub_str = res[ind_beg:ind_end]
            
            # Interpret sub-section
            interpreted_value = self.interpret(sub_str)
            
            # Manage sections that aren't interpretable
            interpreted_str = f"{interpreted_value}"
            if interpreted_str == sub_str:
                excluded_indexes.append((ind_beg, ind_end))
                offset = ind_end
                continue
            
            # Define section replacement
            if replace_sections_by_variables and type(interpreted_value) not in [str, int, float, bytes]:
                var_name = self.__variable_manager.new_local_variable_name()
                locals_[var_name] = interpreted_value
                section_replace_str = var_name
            else:
                section_replace_str = interpreted_str
            
            # Replace by interpreted value
            res = f"{res[0:ind_beg]}{section_replace_str}{res[ind_end:]}"
            
            # Prepare next replacement
            offset = ind_beg + len(section_replace_str)
                
        return res, locals_
    
    def __search_all_interpret_sections(self, str_to_interpret):
        m = None
        match_failed = False
        exc_msgs = []
        
        # Try to use regex to group "${" with "}" and "{" with "}", but without skipping "{{" and "}}"
        reg = self.__build_regex_matching_interpret_sections(str_to_interpret, do_match_semicolon=True, do_skip_double_semicolon=False)
        try:
            m = re.match(reg, str_to_interpret, re.DOTALL)
        except Exception as exc:
            exc_msgs.append(f"Error while matching string to built regex:\n    regex: [{reg}]\n   string: [{str_to_interpret}]\n    error: {exc}")
            match_failed = True
            
        # If failed, try to use regex to group "${" with "}" and "{" with "}", and skipping "{{" and "}}"
        if match_failed:
            match_failed = False
            reg = self.__build_regex_matching_interpret_sections(str_to_interpret, do_match_semicolon=True, do_skip_double_semicolon=True)
            try:
                m = re.match(reg, str_to_interpret, re.DOTALL)
            except Exception as exc:
                exc_msgs.append(f"Error while matching string to built regex:\n    regex: [{reg}]\n   string: [{str_to_interpret}]\n    error: {exc}")
                match_failed = True
            
        # If failed, try to use regex to group "${" with "}", but do not group "{" with "}"
        if match_failed:
            match_failed = False
            reg = self.__build_regex_matching_interpret_sections(str_to_interpret, do_match_semicolon=False)
            try:
                m = re.match(reg, str_to_interpret, re.DOTALL)
            except Exception as exc:
                exc_msgs.append(f"Error while matching string to built regex:\n    regex: [{reg}]\n   string: [{str_to_interpret}]\n    error: {exc}")
                match_failed = True
            
        if match_failed:
            msgs = Tools.indent_string(4, '\n'.join(exc_msgs))
            raise TechnicalException("Failed to search interpret sections for string [{str_to_interpret}]:\n{msgs}"\
                                     .format(str_to_interpret=str_to_interpret, msgs=msgs))
        elif m is None:
            raise TechnicalException(f"Failed to build a working regex:\n    regex: [{reg}]\n   string: [{str_to_interpret}]")
        
        # Find all "${"
        section_start_indexes = [m.start() for m in re.finditer('\$\{', str_to_interpret)]
        
        # Intersect both search
        return [r for r in m.regs[1:] if r[0] in section_start_indexes]
    
    def __build_regex_matching_interpret_sections(self, str_to_interpret, do_match_semicolon=True, do_skip_double_semicolon=False):
        res = str_to_interpret
        res = res.replace('(','.')\
                 .replace(')','.')\
                 .replace('${','(..')
        if do_skip_double_semicolon:
            res = res.replace('{{', '..')
            res = res.replace('}}','..')
        if do_match_semicolon:
            res = res.replace('}','.)')\
                     .replace('{','(.')
        res = re.sub('[^()]', '.', res)
        return res
        
    
    def __search_interpret_section(self, text, start=0, excluded_indexes=None):
        if excluded_indexes is None:
            excluded_indexes = []
        
        offset = start
        ind_beg = None
        ind_end = None
        
        while True:
            # Find first section delimiters after offset
            if ind_beg is None:
                ind_beg = text.find("${", offset)
                if ind_beg < 0:
                    return None
            if ind_end is None:
                ind_end = text.find("}", ind_beg)
                if ind_end < 0:
                    return None
            
            # Find closest begin separator to end separator
            ind = text.find("${", ind_beg + 2)
            if ind > 0 and ind < ind_end:
                # Continue with next begin section
                ind_beg = ind
                continue
            
            # If section not in excluded ones, section is found
            if (ind_beg, ind_end+1) not in excluded_indexes:
                return (ind_beg, ind_end+1)
                
            # Try again with new offset
            offset = ind_end + 1
            ind_beg = None
            ind_end = None
        
        raise TechnicalException("Unexpected case")
        
    def __has_function(self, text):
        r_match = self.__regex_function.match(text)
        if r_match:
            test_function = self.__get_function(r_match.group(1))
            return test_function is not None
        return False
        
    def __has_verify_function(self, text):
        r_match = self.__regex_function.match(text)
        if r_match:
            verify_function = self.__get_verify_function(r_match.group(1))
            return verify_function is not None
        return False
        
    def __evaluate_function(self, fct_str):
        func = None
        r_match = self.__regex_function.match(fct_str)
        if r_match:
            func_name = r_match.group(1)
            func = self.__get_function(func_name)
        if func is None:
            raise NotExistingFunctionTextInterpreterException("Cannot find a function in '{}'".format(fct_str))
        
        args = self.__get_args(r_match.group(2))
        
        logger.trace(f"Evaluating function '{func_name}' with args {args}")
        res = func.apply(args)
        logger.debug(f"Evaluate function [{fct_str}] => [{func_name}({', '.join(args)})] => [{res}] (type: {type(res)})")
        return res
    
    def __evaluate_function_verify(self, expected, obtained, raise_exception):
        verify_function = None
        r_match = self.__regex_to_be_interpreted_with_function.match(expected)
        if r_match:
            func_name = r_match.group(1)
            verify_function = self.__get_verify_function(func_name)
            # logger.trace("Verifying -> is verify function '{}': {}".format(func_name, verify_function))
        if verify_function is None:
            return None

        if func_name == "ExtractIn":
            args = [r_match.group(2)]
        else:
            args = self.__get_args(r_match.group(2))
            
        logger.trace(f"Evaluating verify function '{func_name}' with args {args} on obtained '{obtained}'")
        res = verify_function.apply(obtained, args, raise_exception=raise_exception)
        logger.debug(f"Evaluate verify function [{expected}] => [{func_name}({', '.join(args)})] => [{res}] (type: {type(res)})")
        return res
                
    def __get_args(self, args_str):
        return args_str.split(',')
        
    def __get_function(self, func_name):
        if func_name in self.__managed_functions:
            return self.__managed_functions[func_name]
        else:
            return None
        
    def __get_verify_function(self, func_name):
        if func_name in self.__managed_verify_functions:
            return self.__managed_verify_functions[func_name]
        else:
            return None
        
    def __register_functions(self, dynamic_text_manager):
        self.__register_function("int", FunctionCast(int, self, self.__variable_manager))
        self.__register_function("float", FunctionCast(float, self, self.__variable_manager))
        self.__register_function("str", FunctionCast(str, self, self.__variable_manager))
        self.__register_function("DynamicValue", FunctionDynamicValue(dynamic_text_manager))
        self.__register_function("HexToBytes", FunctionHexToBytes(self.__expression_evaluator))
        self.__register_function("HexToInt", FunctionHexToInt(self, self.__variable_manager))
        self.__register_function("ToBase64", FunctionToBase64(self, self.__variable_manager))
        self.__register_function("ToBytes", FunctionToBytes(self, self.__variable_manager))
        self.__register_function("ToHex", FunctionToHex(self, self.__variable_manager))
        
        self.__register_verify_function("MatchPattern", VerifyFunctionMatchPattern(self.__variable_manager))
        self.__register_verify_function("ExtractIn", VerifyFunctionExtractIn(self.__variable_manager))

    def __register_function(self, function_name, function):
        self.__managed_functions[function_name] = function

    def __register_verify_function(self, function_name, function):
        self.__managed_verify_functions[function_name] = function

    def _log_match_failure(self, msg):
        if self.__process_log_match_failure:
            logger.debug("--- INTERPRETER --- {}".format(msg))
        
    
