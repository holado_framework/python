# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_value.common.tables.value_table_manager import ValueTableManager
from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_protobuf.ipc.protobuf.protobuf_messages import ProtobufMessages
from holado_value.common.tools.value_types import ValueTypes
import logging
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


if ProtobufMessages.is_available():
    
    # TODO: check if needed
    import google.protobuf.pyext
    
    def __get_protobuf_converter():
        return SessionContext.instance().protobuf_converter
    
    def __get_protobuf_messages():
        return SessionContext.instance().protobuf_messages
    
    def __get_scenario_context():
        return SessionContext.instance().get_scenario_context()
    
    def __get_text_interpreter():
        return __get_scenario_context().get_text_interpreter()
    
    def __get_variable_manager():
        return __get_scenario_context().get_variable_manager()
    
    def __get_session_context():
        return SessionContext.instance()
    
    
    @Step(u"(?P<var_name>\w+) = convert list (?P<list_var_name>\w+) to list of Protobuf field (?P<attribute_name>'[^']*'%?|[^' ]+)")
    def step_impl(context, var_name, list_var_name, attribute_name):  # @DuplicatedSignature
        attr_name = StepTools.evaluate_scenario_parameter(attribute_name)
        list_obj = __get_variable_manager().get_variable_value(list_var_name)
        
        res = []
        for index, obj in enumerate(list_obj):
            if hasattr(obj, attr_name):
                attr_val = getattr(obj, attr_name)
                if ProtobufMessages.is_object_repeated(attr_val):
                    attr_val = [v for v in attr_val]
                # logger.debug(f"Result list - add field value [{attr_val}] (type: {type(attr_val)} ; dir: {dir(attr_val)})")
                res.append(attr_val)
            else:
                raise FunctionalException(f"In list, object of index {index} hasn't attribute '{attr_name}'")
        
        __get_variable_manager().register_variable(var_name, res)
    
    @Given(u"(?P<var_name>\w+) = convert list (?P<list_var_name>\w+) to merged list of Protobuf repeated field (?P<attribute_name>'[^']*'%?|[^' ]+)")
    def step_impl(context, var_name, list_var_name, attribute_name):  # @DuplicatedSignature
        attr_name = StepTools.evaluate_scenario_parameter(attribute_name)
        list_obj = __get_variable_manager().get_variable_value(list_var_name)
        
        res = []
        for index, obj in enumerate(list_obj):
            if hasattr(obj, attr_name):
                attr_val = getattr(obj, attr_name)
                if not ProtobufMessages.is_object_repeated(attr_val):
                    raise FunctionalException(f"In list, object of index {index} has field '{attr_name}' but it isn't repeated")
                
                logger.debug(f"In list, object of index {index} has field '{attr_name}' repeated {len(attr_val)} times")
                for val in attr_val:
                    # logger.debug(f"Result list - add field value [{val}] (type: {type(val)} ; dir: {dir(val)})")
                    res.append(val)
            else:
                raise FunctionalException(f"In list, object of index {index} hasn't attribute '{attr_name}'")
        
        __get_variable_manager().register_variable(var_name, res)
    
    
    @Given(u'(?P<var_name>\w+) = convert list (?P<list_obj_str>\w+) to table with Protobuf fields as columns')
    def step_impl(context, var_name, list_obj_str):
        list_obj = __get_variable_manager().get_variable_value(list_obj_str)
        
        res = __get_protobuf_converter().create_table_with_protobuf_fields_as_columns(list_obj, recursive=False)
    
        __get_variable_manager().register_variable(var_name, res)
    
    @Given(u'(?P<var_name>\w+) = convert list (?P<list_obj_str>\w+) to table with Protobuf fields as columns recursively')
    def step_impl(context, var_name, list_obj_str):
        list_obj = __get_variable_manager().get_variable_value(list_obj_str)
        
        res = __get_protobuf_converter().create_table_with_protobuf_fields_as_columns(list_obj, recursive=True)
    
        __get_variable_manager().register_variable(var_name, res)
    
    @Given(u'(?P<var_name>\w+) = convert list (?P<list_obj_str>\w+) to table with Protobuf fields as columns recursively and repeated fields uncollapsed')
    def step_impl(context, var_name, list_obj_str):
        list_obj = __get_variable_manager().get_variable_value(list_obj_str)
        
        res = __get_protobuf_converter().create_table_with_protobuf_fields_as_columns(list_obj, recursive=True, uncollapse_repeated=True)
    
        __get_variable_manager().register_variable(var_name, res)
    
     
    @Step(u"(?P<var_name>\w+) = new Protobuf object of type (?P<proto_type_str>'[^']*'%?|[^' ]+)")
    def step_impl(context, var_name, proto_type_str):
        proto_type_str = StepTools.evaluate_scenario_parameter(proto_type_str)
        table = BehaveStepTools.get_step_table(context)
        
        res = __get_session_context().protobuf_messages.new_message(proto_type_str)
        if table:
            __fill_protobuf_fields_with_name_value_table(res, table)
            
        __get_variable_manager().register_variable(var_name, res)  

    def __fill_protobuf_fields_with_name_value_table(obj, table):
        # Verify table structure
        ValueTableManager.verify_table_is_name_value_table(table)
        
        for row in table.rows:
            if row.get_cell(1).value_type not in [ValueTypes.NotApplicable]:
                logger.debug("Converting row (%s)", row)
                
                name = row.get_cell(0).value
                value = row.get_cell(1).value
                
                __get_protobuf_messages().set_object_field_value(obj, name, value)

    @Step(u"(?P<var_name>\w+) = serialize Protobuf object (?P<proto_type_str>'[^']*'%?|[^' ]+)")
    def step_impl(context, var_name, proto_type_str):
        obj = StepTools.evaluate_scenario_parameter(proto_type_str)
        res = obj.SerializeToString()
        __get_variable_manager().register_variable(var_name, res) 
       
    @Step(u"(?P<var_name>\w+) = unserialize string (?P<ser_str>'[^']*'%?|[^' ]+) as (?P<proto_type_str>'[^']*'%?|[^' ]+) Protobuf object")
    def step_impl(context, var_name, ser_str, proto_type_str):
        ser_str = StepTools.evaluate_scenario_parameter(ser_str)
        proto_type_str = StepTools.evaluate_scenario_parameter(proto_type_str)
    
        obj = __get_session_context().protobuf_messages.new_message(proto_type_str)
        obj.ParseFromString(ser_str)

        __get_variable_manager().register_variable(var_name, obj)
    
    @Given(u"(?P<var_name>\w+) = convert Protobuf object (?P<obj_var_name>[^' ]+) to json object")
    def step_impl(context, var_name, obj_var_name):  # @DuplicatedSignature
        obj = StepTools.evaluate_scenario_parameter(obj_var_name)
        res = __get_protobuf_converter().convert_protobuf_object_to_json_object(obj)
        __get_variable_manager().register_variable(var_name, res)
    
    @Given(u"(?P<var_name>\w+) = convert Protobuf object (?P<obj_var_name>[^' ]+) to name/value table")
    def step_impl(context, var_name, obj_var_name):  # @DuplicatedSignature
        obj = StepTools.evaluate_scenario_parameter(obj_var_name)
        res = __get_protobuf_converter().convert_protobuf_object_to_name_value_table(obj, recursive=False, uncollapse_repeated=False)
        __get_variable_manager().register_variable(var_name, res)

    @Given(u"(?P<var_name>\w+) = convert Protobuf object (?P<obj_var_name>[^' ]+) to name/value table with names uncollapsed")
    def step_impl(context, var_name, obj_var_name):  # @DuplicatedSignature
        obj = StepTools.evaluate_scenario_parameter(obj_var_name)
        res = __get_protobuf_converter().convert_protobuf_object_to_name_value_table(obj, recursive=True, uncollapse_repeated=False)
        __get_variable_manager().register_variable(var_name, res)

    @Given(u"(?P<var_name>\w+) = convert Protobuf object (?P<obj_var_name>[^' ]+) to name/value table with names and repeated uncollapsed")
    def step_impl(context, var_name, obj_var_name):  # @DuplicatedSignature
        obj = StepTools.evaluate_scenario_parameter(obj_var_name)
        res = __get_protobuf_converter().convert_protobuf_object_to_name_value_table(obj, recursive=True, uncollapse_repeated=True)
        __get_variable_manager().register_variable(var_name, res)

    @Given(u"(?P<var_name>\w+) = value of Protobuf enum (?P<enum_fullname>'[^']*'|[^' ]+)")
    def step_impl(context, var_name, enum_fullname):  # @DuplicatedSignature
        enum_fullname = StepTools.evaluate_scenario_parameter(enum_fullname)
        res = __get_protobuf_messages().get_enum_value(fullname=enum_fullname)
        __get_variable_manager().register_variable(var_name, res)

    @Given(u"(?P<var_name>\w+) = name of value (?P<enum_value>'[^']*'|[^' ]+) of Protobuf enum type (?P<enum_type_fullname>'[^']*'|[^' ]+)")
    def step_impl(context, var_name, enum_value, enum_type_fullname):  # @DuplicatedSignature
        enum_value = StepTools.evaluate_scenario_parameter(enum_value)
        enum_type_fullname = StepTools.evaluate_scenario_parameter(enum_type_fullname)
        res = __get_protobuf_messages().get_enum_name(enum_value=enum_value, enum_type_fullname=enum_type_fullname)
        __get_variable_manager().register_variable(var_name, res)

    @Given(u"import Protobuf enum type (?P<enum_type_fullname>'[^']*'|[^' ]+)")
    def step_impl(context, enum_type_fullname):  # @DuplicatedSignature
        enum_type_fullname = StepTools.evaluate_scenario_parameter(enum_type_fullname)
        res = __get_protobuf_messages().get_enum_type(enum_type_fullname=enum_type_fullname)
        __get_variable_manager().register_variable(res.DESCRIPTOR.name, res)
        __get_variable_manager().register_variable(res.DESCRIPTOR.full_name, res)

    @Given(u"import values of Protobuf enum type (?P<enum_type_fullname>'[^']*'|[^' ]+)")
    def step_impl(context, enum_type_fullname):  # @DuplicatedSignature
        enum_type_fullname = StepTools.evaluate_scenario_parameter(enum_type_fullname)
        et = __get_protobuf_messages().get_enum_type(enum_type_fullname=enum_type_fullname)
        for name, value in et.DESCRIPTOR.values_by_name.items():
            __get_variable_manager().register_variable(name, value.number)

    @Given(u"import values of Protobuf enum type (?P<enum_type_fullname>'[^']*'|[^' ]+) with prefix (?P<prefix>'[^']*'%?|[^' ]+)")
    def step_impl(context, enum_type_fullname, prefix):  # @DuplicatedSignature
        enum_type_fullname = StepTools.evaluate_scenario_parameter(enum_type_fullname)
        prefix = StepTools.evaluate_scenario_parameter(prefix)
        et = __get_protobuf_messages().get_enum_type(enum_type_fullname=enum_type_fullname)
        for name, value in et.DESCRIPTOR.values_by_name.items():
            __get_variable_manager().register_variable(prefix+name, value.number)



