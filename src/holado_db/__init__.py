# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


def dependencies():
    return ['holado_scripting']

def register():
    from holado.common.context.session_context import SessionContext
    
    from holado_db.tools.db.db_manager import DBManager
    SessionContext.instance().services.register_service("db_manager", lambda: DBManager(None),
                        lambda m: m.initialize(lambda: SessionContext.instance().variable_manager, lambda: SessionContext.instance().query_manager) )
    
    from holado_db.tools.db.query.query_manager import QueryManager
    SessionContext.instance().services.register_service("query_manager", lambda: QueryManager(None), lambda m: m.initialize())


