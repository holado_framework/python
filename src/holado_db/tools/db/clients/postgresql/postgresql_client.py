
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_db.tools.db.clients.base.db_client import DBClient
from holado_core.common.tables.table_with_header import TableWithHeader

logger = logging.getLogger(__name__)

try:
    import psycopg
    with_psycopg = True
except Exception as exc:
    logger.debug(f"PostgreSQLClient is not available. Initialization failed on error: {exc}")
    with_psycopg = False


class PostgreSQLClient(DBClient):
    @classmethod
    def is_available(cls):
        return with_psycopg

    def __init__(self, name, connect_kwargs):
        super().__init__(name if name else 'PostgreSQL', connect_kwargs)
        
    def _connect(self, **kwargs):
        return psycopg.connect(**kwargs)
        
    def _get_sql_placeholder(self):
        return "%s"
        
    def exist_table(self, table_name):
        result = self.execute(f"SELECT tablename FROM pg_tables WHERE schemaname = 'public' AND tablename = '{table_name}'")
        return result is not None
        
    def set_or_update_json_key_value(self, table_name, field_name, json_key, json_value, where_data: dict=None):
        # Help on JSON column: https://www.databasestar.com/postgresql-json/#How_to_Update_JSON_Data_in_PostgreSQL
        result = self.select(table_name, where_data, sql_return=field_name)
        is_set = isinstance(result, TableWithHeader) and result[0][0].content is not None
        if not is_set:
            self.update(table_name, {field_name: f'{{"{json_key}":"{json_value}"}}'}, where_data, do_commit=True)
        else:
            result = self.select(table_name, where_data, sql_return=f"{field_name} ->> '{json_key}'")
            is_key_set = isinstance(result, TableWithHeader) and result[0][0].content is not None
            if is_key_set:
                self.update(table_name, {field_name: f"JSONB_SET({field_name}, '{json_key}', '\"{json_value}\"')"}, where_data, do_commit=True)
            else:
                self.update(table_name, {field_name: f'{field_name} || {{"{json_key}":"{json_value}"}}'}, where_data, do_commit=True)
    
    
