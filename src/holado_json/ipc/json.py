
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.tables.table_with_header import TableWithHeader
import json
from holado_core.common.exceptions.technical_exception import TechnicalException
from json.decoder import JSONDecodeError
from holado_protobuf.ipc.protobuf.protobuf_messages import ProtobufMessages
from holado.common.context.session_context import SessionContext
import base64

logger = logging.getLogger(__name__)

def convert_to_json(value):
    if isinstance(value, str):
        try:
            return json.loads(value)
        except JSONDecodeError:
            return value
    elif isinstance(value, bytes):
        return base64.b64encode(value)
    elif isinstance(value, dict) or isinstance(value, list):
        try:
            tmp = json.dumps(value)
            return json.loads(tmp)
        except JSONDecodeError:
            return value
    elif ProtobufMessages.is_object_message(value):     # TODO EKL: move in sub-project holado_protobuf
        converter = SessionContext.instance().protobuf_converter
        return converter.convert_protobuf_object_to_json_object(value)
    else:
        return value

def create_name_value_table_from_json(json_value, recursive=False, uncollapse_list=False):
    res = TableWithHeader()
    res.header.add_cells_from_contents(["Name", "Value"])

    json_value = convert_to_json(json_value)
    
    name_values = __convert_json_value_2_name_values(json_value, recursive=recursive, uncollapse_list=uncollapse_list)
    for name, value in name_values:
        res.add_row(cells_content=(name, value))
    
    return res
    
def create_table_with_header_from_json(json_value, recursive=False, uncollapse_list=False):
    res = TableWithHeader()
    
    json_value = convert_to_json(json_value)
    if isinstance(json_value, list):
        json_list = json_value
    else:
        json_list = [json_value]
    
    for json_val in json_list:
        json_val = convert_to_json(json_val)
        
        name_values = __convert_json_value_2_name_values(json_val, recursive=recursive, uncollapse_list=uncollapse_list)
        
        # Add new columns if needed
        for name, value in name_values:
            if not res.has_column(name, raise_exception=False):
                res.add_column(name=name, cells_content = [None]*res.nb_rows)
        
        # Fill table
        values_by_name = {name: value for name, value in name_values}
        res.add_row(contents_by_colname = values_by_name)
        
    # Order columns
    if res.nb_columns > 0:
        res.order_columns(names = sorted(res.get_column_names()))
        
    return res
    
def __convert_json_value_2_name_values(json_value, recursive=False, uncollapse_list=False):
    res = []
    __fill_name_values_with_json_value(res, None, json_value, recursive=recursive, uncollapse_list=uncollapse_list)
    return res
    
def __fill_name_values_with_json_value(res, name_prefix, json_value, recursive=False, uncollapse_list=False):
    if isinstance(json_value, dict):
        __fill_name_values_with_json_dict(res, name_prefix, json_value, recursive=recursive, uncollapse_list=uncollapse_list)
    elif isinstance(json_value, list):
        __fill_name_values_with_json_list(res, name_prefix, json_value, recursive=recursive, uncollapse_list=uncollapse_list)
    else:
        res.append((name_prefix, json_value))
    
def __fill_name_values_with_json_dict(res, name_prefix, json_value, recursive=False, uncollapse_list=False):
    if not isinstance(json_value, dict):
        raise TechnicalException(f"json value is expected to be a dict (obtained: {type(json_value)}")
    
    sorted_dict = dict(sorted(json_value.items()))
    for name, value in sorted_dict.items():
        new_prefix = f"{name_prefix}.{name}" if name_prefix is not None and len(name_prefix) > 0 else name
        if recursive:
            __fill_name_values_with_json_value(res, new_prefix, value, recursive=recursive, uncollapse_list=uncollapse_list)
        else:
            res.append((new_prefix, value))
    
def __fill_name_values_with_json_list(res, name_prefix, json_value, recursive=False, uncollapse_list=False):
    if not isinstance(json_value, list):
        raise TechnicalException(f"json value is expected to be a list (obtained: {type(json_value)}")
    
    if not uncollapse_list:
        res.append((name_prefix, json_value))
        return
        
    if len(json_value) == 0:
        res.append((name_prefix+"[]", None))
        return
        
    for index, value in enumerate(json_value):
        new_prefix = f"{name_prefix}[{index}]" if name_prefix is not None else f"[{index}]"
        __fill_name_values_with_json_value(res, new_prefix, value, recursive=recursive, uncollapse_list=uncollapse_list)


