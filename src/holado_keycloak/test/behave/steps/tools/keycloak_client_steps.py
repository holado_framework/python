# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_value.common.tables.value_table_manager import ValueTableManager
from holado_keycloak.tools.keycloak.keycloak_client import KeycloakClient
import logging
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


def __get_session_context():
    return SessionContext.instance()

def __get_scenario_context():
    return __get_session_context().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()




@Given(u"(?P<var_name>\w+) = token from Keycloak server")
def step_impl(context, var_name):
    table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    params = ValueTableManager.convert_name_value_table_2_dict(table)
    
    client = KeycloakClient()
    if 'username' in params and 'password' in params:
        res = client.get_token_by_grant_type_password(**params)
    elif 'refresh_token' in params:
        res = client.get_token_by_grant_type_refresh_token(**params)
    else:
        raise FunctionalException(f"Unable to get token with given parameters.\nAllowed methods: by password (parameters 'username' and 'password'), by refresh (parameter 'refresh_token')\nReceived parameters: {params}")
    
    __get_variable_manager().register_variable(var_name, res)
    
    
