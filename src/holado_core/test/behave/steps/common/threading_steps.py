# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado.common.context.session_context import SessionContext
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_test.behave.behave import *
from holado_core.common.threading.functionthreaded import FunctionThreaded
from holado_core.common.threading.threadsmanager import ThreadsManager
from holado_core.common.block.function import Function
from holado_test.scenario.step_tools import StepTools
import logging
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)

SR_VAR_NAME = step_regex('variable name')
SR_VAR_EXPR = step_regex('variable expression')
SR_FLOAT = step_regex('float parameter')


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()


@Given(u'(?P<var_name>{re_var_name}) = call steps in a thread(?: \(delay before: (?P<delay_before_str>{re_float}) s\))?'.format(re_var_name=SR_VAR_NAME, re_float=SR_FLOAT))
def step_impl(context, var_name, delay_before_str):
    delay_before_sec = StepTools.evaluate_scenario_parameter(delay_before_str)
    steps = BehaveStepTools.get_step_multiline_text(context)
    
    func = FunctionThreaded(execute_steps, args=[steps], register_thread=False, delay_before_run_sec=delay_before_sec)
    func.start()
    
    thread_name = f"CALL_STEPS[{var_name};{func.ident}]"
    func.name = thread_name
    ThreadsManager.register_thread(thread_name, func)
    
    __get_variable_manager().register_variable(var_name, thread_name)

@Given(u'for thread (?P<var_name>\w+), call steps for interrupt')
def step_impl(context, var_name):
    steps = BehaveStepTools.get_step_multiline_text(context)
    
    interrupt_func = Function(execute_steps, steps)
    
    thread_id = __get_variable_manager().get_variable_value(var_name)
    func = ThreadsManager.get_thread(thread_id)
    if hasattr(func, "interrupt_function"):
        func.interrupt_function = interrupt_func
    else:
        raise FunctionalException("Thread '{}' is not interruptable by a function".format(var_name))


