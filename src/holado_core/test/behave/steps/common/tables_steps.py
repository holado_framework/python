# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.behave.behave import *  # @UnusedWildImport
import copy
from holado_value.common.tools.value_types import ValueTypes
from holado_core.common.tables.table_manager import TableManager
from holado_value.common.tables.comparators.table_2_value_table_comparator import Table2ValueTable_Comparator
from holado_core.common.tables.comparators.string_table_comparator import StringTableComparator
from holado_core.common.tools.tools import reversed_enumerate, reversed_range,\
    Tools
from holado_test.scenario.step_tools import StepTools
from holado_core.common.tables.comparators.table_comparator_manager import TableComparatorManager
from holado_core.common.tables.comparators.table_comparator import TableComparator
from holado_core.common.tables.comparators.table_row_comparator import TableRowComparator
from holado_value.common.tables.comparators.table_2_value_table_row_comparator import Table2ValueTable_RowComparator
from holado_value.common.tables.comparators.table_2_value_table_with_header_comparator import Table2ValueTable_WithHeaderComparator
from holado_value.common.tables.comparators.table_2_value_table_cell_comparator import Table2ValueTable_CellComparator
from holado_core.common.tables.table_with_header import TableWithHeader
from holado_core.common.tables.table import Table
from holado_core.common.tables.table_row import TableRow
from holado_core.common.tables.comparators.string_table_row_comparator import StringTableRowComparator
import logging
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.exceptions.verify_failure_exception import VerifyFailureException
from holado.common.context.session_context import SessionContext
from holado_value.common.tables.value_table import ValueTable
from holado_value.common.tables.value_table_with_header import ValueTableWithHeader
from holado_core.common.tools.converters.converter import Converter
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)

SR_VAR_NAME = step_regex('variable name')
SR_VAR_EXPR = step_regex('variable expression')


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()

@Given(u"(?P<var_name>\w+) = table")
def step_impl(context, var_name):
    table = BehaveStepTools.convert_step_table_2_value_table(context.table)
    __get_variable_manager().register_variable(var_name, table)

@Given(u"(?P<var_name>\w+) = table with header")
def step_impl(context, var_name):
    table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    __get_variable_manager().register_variable(var_name, table)

@Step(u"(?P<var_name>\w+) = convert dictionary (?P<obj_str>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) to name/value table")
def step_impl(context, var_name, obj_str):
    obj = StepTools.evaluate_scenario_parameter(obj_str)
    if isinstance(obj, str):
        obj = eval(obj)
    if not isinstance(obj, dict):
        raise TechnicalException(f"Expecting dict, received {type(obj)}")
    
    table = TableManager.convert_dict_2_name_value_table(obj)
    __get_variable_manager().register_variable(var_name, table)

@Step(u"(?P<var_name>\w+) = convert dictionary (?P<obj_str>'[^']*'%?|[^' ]*'[^']*'[^ ]*|[^ ]+) to table with keys as columns")
def step_impl(context, var_name, obj_str):
    obj = StepTools.evaluate_scenario_parameter(obj_str)
    if isinstance(obj, str):
        obj = eval(obj)
    if not isinstance(obj, dict):
        raise TechnicalException(f"Expecting dict, received {type(obj)}")
    
    table = TableManager.convert_dict_2_table_with_keys_as_column(obj)
    __get_variable_manager().register_variable(var_name, table)

@Step(u'(?P<var_name>\w+) = convert object (?P<obj_str>\w+) to name/value table')
def step_impl(context, var_name, obj_str):
    obj = __get_variable_manager().get_variable_value(obj_str)
    table = TableManager.convert_object_attributes_2_name_value_table(obj)
    __get_variable_manager().register_variable(var_name, table)
    
#TODO EKL: move step
@Step(u"(?P<var_name>\w+) = convert string (?P<list_obj_str>\w+) to list")
def step_impl(context, var_name, list_obj_str):
    list_str = __get_variable_manager().get_variable_value(list_obj_str)
    
    list_obj = eval(list_str)
    if not isinstance(list_obj, list):
        raise FunctionalException("Given string is not a string representation of a list")
    
    __get_variable_manager().register_variable(var_name, list_obj)

@Step(u'(?P<var_name>\w+) = convert list (?P<list_obj_str>\w+) to table with object attributes as columns')
def step_impl(context, var_name, list_obj_str):
    list_obj = __get_variable_manager().get_variable_value(list_obj_str)
    table = TableManager.convert_object_list_2_table_with_attributes_as_column(list_obj)
    __get_variable_manager().register_variable(var_name, table)

@Step(u'(?P<var_name>\w+) = convert list (?P<list_obj_str>\w+) to column table')
def step_impl(context, var_name, list_obj_str):
    list_obj = __get_variable_manager().get_variable_value(list_obj_str)
    table = TableManager.convert_object_list_2_column_table(list_obj)
    __get_variable_manager().register_variable(var_name, table)

@Then(u"compare table 1 (?P<table_1_str>\w+) to table 2 (?P<table_2_str>\w+) with")
def step_impl(context, table_1_str, table_2_str):
    from behave.model_describe import ModelDescriptor
    rendered_table = ModelDescriptor.describe_table(context.table, "    ")

    execute_steps(u"""
        Then compare table 1 {table_1_str} to table 2 {table_2_str} with (reorder columns table 1:true ; reorder columns table 2:true)
        {table}
        """.format(table_1_str=table_1_str, table_2_str=table_2_str, table=rendered_table))

@Then(u"compare table 1 (?P<table_1_str>\w+) to table 2 (?P<table_2_str>\w+) with \(reorder columns table 1:(?P<reorder_table_1_str>true|false) ; reorder columns table 2:(?P<reorder_table_2_str>true|false)\)")
def step_impl(context, table_1_str, table_2_str, reorder_table_1_str, reorder_table_2_str):
    table_1 = __get_variable_manager().get_variable_value(table_1_str)
    table_2 = __get_variable_manager().get_variable_value(table_2_str)
    table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    
    t1 = copy.copy(table_1)
    t2 = copy.copy(table_2)
    colnames_t1 = table.get_column(name="Column Names 1")
    colnames_t2 = table.get_column(name="Column Names 2")
    has_compare_method = table.has_column(name="Compare method", raise_exception=False)
    if has_compare_method:
        compare_methods = table.get_column(name="Compare method")
    
    # Extract compared data
    compare_colnames_t1 = copy.copy(colnames_t1)
    compare_colnames_t2 = copy.copy(colnames_t2)
    exp_colnames_t1 = copy.copy(colnames_t1)
    exp_colnames_t2 = copy.copy(colnames_t2)
    for ind in reversed_range(len(colnames_t1)):
        if colnames_t1[ind].value_type == ValueTypes.NotApplicable or colnames_t2[ind].value_type == ValueTypes.NotApplicable:
            del compare_colnames_t1[ind]
            del compare_colnames_t2[ind]
            if has_compare_method:
                del compare_methods[ind]
        if colnames_t1[ind].value_type == ValueTypes.NotApplicable:
            cn_t2 = colnames_t2[ind].string_content
            t2.remove_column(name=cn_t2)
            del exp_colnames_t1[ind]
        if colnames_t2[ind].value_type == ValueTypes.NotApplicable:
            cn_t1 = colnames_t1[ind].string_content
            t1.remove_column(name=cn_t1)
            del exp_colnames_t2[ind]
    
    # Reorder tables
    if Converter.to_boolean(reorder_table_1_str.capitalize()):
        table_1.order_columns(names=exp_colnames_t1.cells_value)
        t1.order_columns(names=compare_colnames_t1.cells_value)
    if Converter.to_boolean(reorder_table_2_str.capitalize()):
        table_2.order_columns(names=exp_colnames_t2.cells_value)
        t2.order_columns(names=compare_colnames_t2.cells_value)
    
    # Verify source table headers
    comp = Table2ValueTable_RowComparator()
    try:
        comp.equals(table_1.header, exp_colnames_t1)
    except VerifyFailureException as exc:
        msg_list = ["Column Names 1 is not matching table 1 columns (columns order is mandatory):",
                    "    Table 1 columns: " + table_1.header.represent(0),
                    "    Column Names 1:  " + exp_colnames_t1.represent(0) ]
        raise FunctionalException("\n".join(msg_list) + f"\n{'-'*20}\n{exc}") from exc
    try:
        sorted_table_2_header = copy.copy(table_2.header)
        sorted_table_2_header.sort()
        sorted_exp_colnames_t2 = copy.copy(exp_colnames_t2)
        sorted_exp_colnames_t2.sort()
        comp.equals(sorted_table_2_header, sorted_exp_colnames_t2)
    except VerifyFailureException as exc:
        msg_list = ["Column Names 2 is not matching table 2 columns (columns order is not mandatory):",
                    "    Table 2 columns: " + table_2.header.represent(0),
                    "    Column Names 2:  " + exp_colnames_t2.represent(0) ]
        raise FunctionalException("\n".join(msg_list) + f"\n{'-'*20}\n{exc}") from exc
    
    # Verify compared table headers
    if t1.nb_columns != len(compare_colnames_t1):
        msg_list = ["Compared columns are not matching table 1 columns:",
                    "    Table 1 columns:  " + t1.header.represent(0),
                    "    compared columns: " + compare_colnames_t1.represent(0) ]
        raise FunctionalException("\n".join(msg_list))
    if t2.nb_columns != len(compare_colnames_t2):
        msg_list = ["Compared columns are not matching table 2 columns:",
                    "    Table 2 columns:  " + t2.header.represent(0),
                    "    compared columns: " + compare_colnames_t2.represent(0) ]
        raise FunctionalException("\n".join(msg_list))
    for ind in range(len(compare_colnames_t1)):
        index_t1 = t1.get_column_index(name=compare_colnames_t1[ind].string_content)
        index_t2 = t2.get_column_index(name=compare_colnames_t2[ind].string_content)
        if index_t1 != index_t2:
            msg_list = ["Compared columns have not same indexes in respective tables:",
                        "    Table header 1: " + t1.header.represent(0),
                        "    Table header 2: " + t2.header.represent(0) ]
            raise FunctionalException("\n".join(msg_list))
            
    # Compare table contents
    if has_compare_method:
        cells_comparators = [TableComparatorManager.convert_compare_method_2_TableCellComparator(cm.value) if cm.value_type != ValueTypes.NotApplicable else None for cm in compare_methods]
        row_comparator = TableRowComparator(cells_comparators=cells_comparators)
        comparator = TableComparator(row_comparator=row_comparator)
    else:
        comparator = StringTableComparator()
    try:
        comparator.equals(t1, t2, is_obtained_vs_expected=True, raise_exception=True)
    except FunctionalException as exc:
        raise FunctionalException(f"Tables are different (obtained = table 1 ; expected = table 2):\n{Tools.indent_string(4, exc.message)}") from exc

@Step(u"table (?P<table_varname>\w+) is")
def step_impl(context, table_varname):
    table_obtained = __get_variable_manager().get_variable_value(table_varname)
    BehaveStepTools.then_table_is(table_obtained, context.table)

@Then(u"table (?P<table_varname>\w+) is empty")
def step_impl(context, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    
    if not table.is_empty:
        raise FunctionalException(f"Table {table_varname} is not empty")

@Step(u"table (?P<table_varname>\w+) contains")
def step_impl(context, table_varname):
    table_obtained = __get_variable_manager().get_variable_value(table_varname)
    BehaveStepTools.then_table_contains(table_obtained, context.table)
    
@Given(u'(?P<var_name>\w+) = table (?P<table_varname>\w+) with columns ordered')
def step_impl(context, var_name, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    table_columns = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)

    res_table = copy.copy(table)
    columns_ordered = table_columns.get_column_names()
    res_table.order_columns(names=columns_ordered)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u'(?P<var_name>\w+) = table (?P<table_varname>\w+) with only columns')
def step_impl(context, var_name, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    table_columns = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)

    res_table = copy.copy(table)
    expected_colnames = table_columns.get_column_names()
    
    table_colnames = table.get_column_names()
    for col_index, col_name in reversed_enumerate(table_colnames):
        if col_name not in expected_colnames:
            res_table.remove_column(index=col_index)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u'(?P<var_name>\w+) = table (?P<table_varname>\w+) with only columns ordered')
def step_impl(context, var_name, table_varname):
    from behave.model_describe import ModelDescriptor
    rendered_table = ModelDescriptor.describe_table(context.table, "    ")

    execute_steps(u"""
        Given __TABLE_WITH_ONLY_COLUMNS__ = table {table_varname} with only columns
        {table}
        """.format(table_varname=table_varname, table=rendered_table))
    
    execute_steps(u"""
        Given {var_name} = table __TABLE_WITH_ONLY_COLUMNS__ with columns ordered
        {table}
        """.format(var_name=var_name, table=rendered_table))
    
@Given(u'(?P<var_name>\w+) = table (?P<table_varname>\w+) with new columns')
def step_impl(context, var_name, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    table_new_columns = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)

    res_table = copy.copy(table)
    is_res_value_table = isinstance(res_table, ValueTable) or isinstance(res_table, ValueTableWithHeader)
    
    res_col_indexes = res_table.get_column_indexes_by_string_content()
    
    tnc_indexes = table_new_columns.get_column_indexes_by_string_content()
    for row_new_column in table_new_columns.rows:
        new_colname_cell = row_new_column[tnc_indexes["Column Name"]]
        new_value_expr_cell = row_new_column[tnc_indexes["Value Expression"]]
        
        # Build new column
        new_col_cells_content = []
        for row in res_table.rows:
            # Add each existing column cells as variable
            for cn in res_col_indexes:
                if is_res_value_table:
                    __get_variable_manager().register_variable(f"Column({cn})", row.get_cell(res_col_indexes[cn]).value, accept_expression=False)
                else:
                    __get_variable_manager().register_variable(f"Column({cn})", row.get_cell(res_col_indexes[cn]).content, accept_expression=False)
                
            # Add new cell
            new_content = new_value_expr_cell.value
            if is_res_value_table and isinstance(new_content, str):
                new_content = f"'{new_content}'"
            new_col_cells_content.append(new_content)
                
        res_table.add_column(name=new_colname_cell.value, cells_content=new_col_cells_content)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u'(?P<var_name>\w+) = table (?P<table_varname>\w+) with new rows')
def step_impl(context, var_name, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    table_new_rows = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)

    res_table = copy.copy(table)
    is_res_value_table = isinstance(res_table, ValueTable) or isinstance(res_table, ValueTableWithHeader)
    
    # Verify tables headers
    header_comp = StringTableRowComparator()
    header_comp.equals(table_new_rows.header, res_table.header)
    
    # Add rows
    for new_row in table_new_rows.rows:
        if is_res_value_table:
            res_table.add_row(cells_content=new_row.cells_value)
        else:
            res_table.add_row(cells_content=new_row.cells_content)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u"(?P<var_name>\w+) = table (?P<table_varname>\w+) with column (?P<col_name>'[^']*'%?|[^' ]+) content replaced by")
def step_impl(context, var_name, table_varname, col_name):
    table = __get_variable_manager().get_variable_value(table_varname)
    table_replace = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    col_name = StepTools.evaluate_scenario_parameter(col_name)

    res_table = copy.copy(table)
    col = res_table.get_column(name=col_name)
    
    tr_indexes = table_replace.get_column_indexes_by_string_content()
    tcell_comparator = Table2ValueTable_CellComparator() if "Condition Expression" in tr_indexes else None
    for cell in col:
        for row_replace in table_replace.rows:
            cell_cond_value = row_replace[tr_indexes["Condition Value"]] if "Condition Value" in tr_indexes else None
            cell_cond_expr = row_replace[tr_indexes["Condition Expression"]] if "Condition Expression" in tr_indexes else None
            cell_rep_value = row_replace[tr_indexes["Replace Value"]] if "Replace Value" in tr_indexes else None
            
            if cell_cond_value is not None:
                cond = (cell_cond_value.value_type == ValueTypes.Symbol and cell_cond_value.content == 'DEFAULT'
                        or cell_cond_value.value_type != ValueTypes.NotApplicable and cell.content == cell_cond_value.value)
            elif cell_cond_expr is not None:
                cond = tcell_comparator.equals(cell, cell_cond_expr, raise_exception = False)
            else:
                continue
                
            if cond:
                if cell_rep_value.value_type != ValueTypes.NotApplicable:
                    cell.content = cell_rep_value.value
                    break
                else:
                    continue
                
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u"(?P<var_name>\w+) = table (?P<table_varname>\w+) with columns (?P<col_names>'[^']*'%?|[^' ]+|\[.*\]) content replaced by")
def step_impl(context, var_name, table_varname, col_names):
    table = __get_variable_manager().get_variable_value(table_varname)
    col_names = StepTools.evaluate_list_scenario_parameter(col_names, "col_names")
    
    from behave.model_describe import ModelDescriptor
    rendered_table = ModelDescriptor.describe_table(context.table, "    ")

    __get_variable_manager().register_variable(var_name, copy.copy(table))
    
    for col_name in col_names:
        execute_steps(u"""
            Given {var_name} = table {var_name} with column '{col_name}' content replaced by
            {table}
            """.format(var_name=var_name, col_name=col_name, table=rendered_table))
    
@Given(u"(?P<var_name>\w+) = table (?P<table_varname>\w+) with rows ordered by (?P<col_names>'[^']*'%?|[^' ]+|\[.*\])")
def step_impl(context, var_name, table_varname, col_names):
    table = __get_variable_manager().get_variable_value(table_varname)
    col_names = StepTools.evaluate_list_scenario_parameter(col_names, "col_names")

    res_table = copy.copy(table)
    res_table.sort(names=col_names)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u"(?P<var_name>\w+) = table (?P<table_varname>\w+) with rows verifying")
def step_impl(context, var_name, table_varname):
    """
    Keep only rows verifying at least one line of step table.
    
    If table table_varname is without header, step table can't have a header and must have same column number as 'table_varname'. 
    If table table_varname is with header, step table must be a table with header, but it can contain only some columns of 'table_varname'. 
    """
    table = __get_variable_manager().get_variable_value(table_varname)
    is_with_header = TableManager.is_table_with_header(table)
    if is_with_header:
        table_remove = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    else:
        table_remove = BehaveStepTools.convert_step_table_2_value_table(context.table)
    
    if is_with_header:
        comparator = Table2ValueTable_WithHeaderComparator()
    else:
        comparator = Table2ValueTable_Comparator()

    res_table = copy.copy(table)
    if is_with_header and table_remove.nb_columns != table.nb_columns:
        res_table.remove_rows_only_verifying(table_remove, comparator, keep_rows=True)
    else:
        res_table.remove_rows_verifying(table_remove, comparator, keep_rows=True)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u"(?P<var_name>\w+) = table (?P<table_varname>\w+) without rows verifying")
def step_impl(context, var_name, table_varname):
    """
    Remove rows verifying at least one line of step table.
    
    If table table_varname is without header, step table can't have a header and must have same column number as 'table_varname'. 
    If table table_varname is with header, step table must be a table with header, but it can contain only some columns of 'table_varname'. 
    """
    table = __get_variable_manager().get_variable_value(table_varname)
    is_with_header = TableManager.is_table_with_header(table)
    if is_with_header:
        table_remove = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    else:
        table_remove = BehaveStepTools.convert_step_table_2_value_table(context.table)
    
    if is_with_header:
        comparator = Table2ValueTable_WithHeaderComparator()
    else:
        comparator = Table2ValueTable_Comparator()

    res_table = copy.copy(table)
    if is_with_header and table_remove.nb_columns != table.nb_columns:
        res_table.remove_rows_only_verifying(table_remove, comparator)
    else:
        res_table.remove_rows_verifying(table_remove, comparator)
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(f"(?P<var_name>{SR_VAR_EXPR}) = table (?P<table_varname>{SR_VAR_EXPR}) without duplicated rows")
def step_impl(context, var_name, table_varname):
    """
    Remove duplicated rows considering only column names in step table.
    """
    table = __get_variable_manager().get_variable_value(table_varname)
    table_columns = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)

    res_table = copy.copy(table)
    res_table.remove_rows_duplicated(column_names=table_columns.get_column_names())
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u"(?P<var_name>\w+) = extract column '(?P<col_name>[^']+)' from table (?P<table_varname>\w+)")
#TODO: duplicate of @Given(u'(?P<var_name>\w+) = table (?P<table_varname>\w+) with only columns')
def step_impl(context, var_name, table_varname, col_name):
    table = __get_variable_manager().get_variable_value(table_varname)

    if isinstance(table, TableWithHeader):
        res_table = TableWithHeader()
        res = table.get_column(name=col_name)

        res_table.header = TableRow(cells_content={col_name})
        for c in res.cells:
            res_table.add_row(cells_content={c.content})
       
        __get_variable_manager().register_variable(var_name, res_table)
    else:
        raise TechnicalException(f"Table {table_varname} as no column {col_name}")
    
@Given(u"(?P<var_name>\w+) = extract column '(?P<col_name>[^']+)' cells from table (?P<table_varname>\w+)")
#TODO: return a list rather than a table
def step_impl(context, var_name, table_varname, col_name):
    table = __get_variable_manager().get_variable_value(table_varname)

    if isinstance(table, TableWithHeader):
        res_table = Table()
        res = table.get_column(name=col_name)

        for c in res.cells:
            res_table.add_row(cells_content={c.content})
       
        __get_variable_manager().register_variable(var_name, res_table)
    else:
        raise TechnicalException(f"Table {table_varname} as no column {col_name}")
    
@Given(u"(?P<var_name>\w+) = extract column '(?P<col_name>[^']+)' cells from table (?P<table_varname>\w+) as row")
#TODO: remove
def step_impl(context, var_name, table_varname, col_name):
    table = __get_variable_manager().get_variable_value(table_varname)

    if isinstance(table, TableWithHeader):
        res_table = Table()
        res = table.get_column(name=col_name)

        res_table.add_row()
        for c in res.cells:
            res_table.add_column(cells_content={c.content})
       
        __get_variable_manager().register_variable(var_name, res_table)
    else:
        raise TechnicalException(f"Table {table_varname} as no column {col_name}")
        

@Given(u"(?P<var_name>\w+) = table (?P<table_varname>\w+) with columns renamed")
def step_impl(context, var_name, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    table_columns = BehaveStepTools.convert_step_table_2_value_table(context.table)
    
    col_index=table.get_column_indexes_by_string_content()
    
    res_table=copy.copy(table)
    header=res_table.header

    for row in table_columns.rows:
        old_name=row.get_cell(0).string_content
        new_name=row.get_cell(1).string_content
        
        if old_name in col_index:
            header.get_cell(col_index[old_name]).content=new_name
    res_table.header=header
    
    __get_variable_manager().register_variable(var_name, res_table)
    
@Given(u"(?P<var_name>\w+) = number of rows in table (?P<table_varname>\w+)")
def step_impl(context, var_name, table_varname):
    table = __get_variable_manager().get_variable_value(table_varname)
    
    # print(table.represent())
    count_table = table.nb_rows
    
    # print(count_table)    
    __get_variable_manager().register_variable(var_name, count_table)