# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
import tempfile
import base64
import csv
from holado_core.common.tables.table_with_header import TableWithHeader
from holado_core.common.tables.table_row import TableRow
import os.path
from datetime import datetime
from holado_core.common.exceptions.technical_exception import TechnicalException
from behave.model_describe import ModelDescriptor
import logging
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()

def __get_report_manager():
    return SessionContext.instance().report_manager

def __get_path_manager():
    return SessionContext.instance().path_manager


@Given(u"(?P<var_name>\w+) = new temporary directory with prefix (?P<prefix>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, prefix):
    pref = StepTools.evaluate_scenario_parameter(prefix)
    tmp_dir = tempfile.mkdtemp(prefix=pref)
    __get_variable_manager().register_variable(var_name, tmp_dir)
    
@Given(u"(?P<var_name>\w+) = scenario report directory (?P<path>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path):
    path = StepTools.evaluate_scenario_parameter(path)
    rd_path = __get_report_manager().current_scenario_report.get_path(path)
    __get_variable_manager().register_variable(var_name, rd_path)
    

@Given(u"(?P<var_name>\w+) = file name of path (?P<path>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path):  # @DuplicatedSignature
    path = StepTools.evaluate_scenario_parameter(path)
    __get_variable_manager().register_variable(var_name, os.path.basename(path))


@Given(u"(?P<var_name>\w+) = content of file (?P<path>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path):  # @DuplicatedSignature
    path = StepTools.evaluate_scenario_parameter(path)
    content = open(path, "rb").read()
    __get_variable_manager().register_variable(var_name, content)

@Given(u"(?P<var_name>\w+) = content of file (?P<path>'[^']*'%?|[^' ]+) in base 64")
def step_impl(context, var_name, path):  # @DuplicatedSignature
    path = StepTools.evaluate_scenario_parameter(path)
    content = open(path, "rb").read()
    content64 = base64.b64encode(content)
    __get_variable_manager().register_variable(var_name, content64)


@Given(u"(?P<var_name>\w+) = table with CSV content (?P<content>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, content):
    execute_steps(u"""
        Given __FILE_CSV__ = create CSV file with prefix 'csv_content' and content {content}
        Given {var_name} = table with content of CSV file __FILE_CSV__
        """.format(var_name=var_name, content=content) )

@Given(u"(?P<var_name>\w+) = table with content of CSV file (?P<path>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path):
    file_path = StepTools.evaluate_scenario_parameter(path)
    
    delimiter=';'
    quoting=csv.QUOTE_NONE
    
    with open(file_path, 'r') as fin:
        dr = csv.DictReader(fin, delimiter=delimiter, quoting=quoting)
        
        res = TableWithHeader()
        res.header = TableRow(cells_content=dr.fieldnames)
            
        for row in dr:
            nrow=dict(row)
            res.add_row(contents_by_colname=nrow)    
                
    __get_variable_manager().register_variable(var_name, res)

@Given(u"(?P<var_name>\w+) = create CSV file with prefix (?P<path_prefix>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path_prefix):
    prefix = StepTools.evaluate_scenario_parameter(path_prefix)
    rendered_table = ModelDescriptor.describe_table(context.table, "    ")

    now_str = datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S")
    filename = "{}_{}.csv".format(prefix, now_str)

    execute_steps(u"""
        Given {var_name} = create CSV file with name '{filename}'
        {table}
        """.format(var_name=var_name, filename=filename, table=rendered_table))

@Given(u"(?P<var_name>\w+) = create CSV file with name (?P<filename>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, filename):
    filename = StepTools.evaluate_scenario_parameter(filename)
    table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
    
    dest_path = __get_report_manager().current_scenario_report.get_path("files")
    file_path = os.path.join(dest_path, filename)
    __get_path_manager().makedirs(file_path)
    
    with open(file_path, 'w') as fout:
        fieldnames = table.header.cells_content
        dw = csv.DictWriter(fout, fieldnames=fieldnames, delimiter=';', quoting=csv.QUOTE_NONE)
        
        dw.writeheader()
        for row in table.rows:
            rowdict = {fieldnames[i]:row[i].string_content for i in range(len(fieldnames))}
            dw.writerow(rowdict)
                
    __get_variable_manager().register_variable(var_name, file_path)

@Given(u"(?P<var_name>\w+) = create CSV file with prefix (?P<path_prefix>'[^']*'%?|[^' ]+) and content (?P<content>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path_prefix, content):
    execute_steps(u"""
        Given {var_name} = create file with prefix {path_prefix} and extension 'csv' and content {content}
        """.format(var_name=var_name, path_prefix=path_prefix, content=content) )

@Given(u"(?P<var_name>\w+) = create file with prefix (?P<path_prefix>'[^']*'%?|[^' ]+) and extension (?P<path_ext>'[^']*'%?|[^' ]+) and content (?P<content>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path_prefix, path_ext, content):
    prefix = StepTools.evaluate_scenario_parameter(path_prefix)
    ext = StepTools.evaluate_scenario_parameter(path_ext)

    now_str = datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S")
    dest_fn = "{}_{}.{}".format(prefix, now_str, ext)
    
    execute_steps(u"""
        Given {var_name} = create file with name {dest_fn} and content {content}
        """.format(var_name=var_name, dest_fn=dest_fn, content=content) )

@Given(u"(?P<var_name>\w+) = create file with name (?P<path_name>'[^']*'%?|[^' ]+) and content (?P<content>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path_name, content):
    filename = StepTools.evaluate_scenario_parameter(path_name)
    content = StepTools.evaluate_scenario_parameter(content)
    
    dest_path = __get_report_manager().current_scenario_report.get_path("files")
    file_path = os.path.join(dest_path, filename)
    __get_path_manager().makedirs(file_path)
    
    if isinstance(content, str):
        with open(file_path, 'wt') as fout:
            fout.write(content)
    elif isinstance(content, bytes):
        with open(file_path, 'wb') as fout:
            fout.write(content)
    else:
        raise TechnicalException(f"Unexpected content type {type(content)} (allowed types: string, bytes)")

    __get_variable_manager().register_variable(var_name, file_path)


@Given(u"(?P<var_name>\w+) = create file with name (?P<path_name>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, path_name):
    content = BehaveStepTools.get_step_multiline_text(context, raise_exception_if_none=False)
    if content is None:
        content = ""
    __get_variable_manager().register_variable("__FILE_CONTENT__", content)
    
    execute_steps(u"""
        Given {var_name} = create file with name {path_name} and content __FILE_CONTENT__
        """.format(var_name=var_name, path_name=path_name) )

@Given(u"(?P<var_name>\w+) = value of environment variable (?P<env_var_name>'[^']*'%?|[^' ]+)")
def step_impl(context, var_name, env_var_name):
    env_var_name = StepTools.evaluate_scenario_parameter(env_var_name)
    res = os.getenv(env_var_name)
    if res is None:
        raise EnvironmentError(f"Environment variable {env_var_name} is not defined")
    __get_variable_manager().register_variable(var_name, res)

