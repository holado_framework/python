# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.exceptions.timeout_exception import TimeoutException
from holado_test.behave.behave import *
import time
import datetime
import logging
import re
from holado_core.common.exceptions.verify_failure_exception import VerifyFailureException
from holado_core.common.tools.tools import Tools

logger = logging.getLogger(__name__)


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()


@Step(u'wait (?P<nb_sec>\d+(?:\.\d*)?) seconds')
def step_impl(context, nb_sec):
    time.sleep(float(nb_sec))

@Step(u"wait until (?P<step_str>.+) \(accepted time: (?P<accepted_time>\d+(?:\.\d*)?) s\)")
def step_impl(context, step_str, accepted_time):
    timeout = 10 * float(accepted_time)
    execute_steps(format_step_with_context(context, 
            u"wait until {step_str} (accepted time: {accepted_time} s ; timeout: {timeout} s)"
                .format(step_str=step_str, accepted_time=accepted_time, timeout=timeout) ))

@Step(u"wait until (?P<step_str>.+) \(accepted time: (?P<accepted_time>\d+(?:\.\d*)?) s ; timeout: (?P<timeout>\d+(?:\.\d*)?) s\)")
def step_impl(context, step_str, accepted_time, timeout):
    polling = float(accepted_time) / 100
    if polling > 1:
        polling = 1
        
    execute_steps(format_step_with_context(context, 
            u"wait until {step_str} (accepted time: {accepted_time} s ; timeout: {timeout} s ; polling: {polling} s)"
                .format(step_str=step_str, accepted_time=accepted_time, timeout=timeout, polling=polling) ))

@Step(u"wait until (?P<step_str>.+) \(accepted time: (?P<accepted_time>\d+(?:\.\d*)?) s ; polling: (?P<polling>\d+(?:.\d+)?) s\)")
def step_impl(context, step_str, accepted_time, polling):
    timeout = 10 * float(accepted_time)
    execute_steps(format_step_with_context(context, 
            u"wait until {step_str} (accepted time: {accepted_time} s ; timeout: {timeout} s ; polling: {polling} s)"
                .format(step_str=step_str, accepted_time=accepted_time, timeout=timeout, polling=polling) ))

@Step(u'wait until (?P<then_step>.*) \(accepted time: (?P<accepted_time>\d+(?:\.\d*)?) s ; timeout: (?P<timeout>\d+(?:\.\d*)?) s ; polling: (?P<polling>\d+(?:.\d+)?) s\)')
def step_impl(context, then_step, accepted_time, timeout, polling):
    accepted_time = float(accepted_time)
    timeout = float(timeout)
    polling_time = float(polling)

    start = datetime.datetime.now()
    now = datetime.datetime.now()
    end_accepted = start + datetime.timedelta(milliseconds=accepted_time*1000)
    end_timeout = start + datetime.timedelta(milliseconds=timeout*1000)
    
    good = False
    last_exception = None
    steps_text = format_step_with_context(context, u"Then {}".format(then_step), with_keyword=False)
    while not good and now <= end_timeout:
        try:
            execute_steps(steps_text)
        except Exception as exc:
            if 'UNDEFINED SUB-STEP' in str(exc):
                raise FunctionalException(f"A step doesn't exist in:\n{steps_text}") from exc
                    
            good = False
            last_exception = exc
            logger.debug("Following steps failed:\n{}\n  Error: {}({})".format(Tools.indent_string(4, steps_text), Tools.get_object_class_fullname(exc), str(exc)))
            time.sleep(polling_time)
        else:
            good = True
        now = datetime.datetime.now()
        
    if not good and now > end_timeout:
        raise TimeoutException(u"Timeout ({} s) when waiting {}\nLast exception: {}".format(timeout, then_step, str(last_exception))) from last_exception
    elif now > end_accepted:
        spent_time = now - start
        raise FunctionalException(u"Too long ({} s) to wait {} (accepted time: {} s)".format(spent_time.total_seconds(), then_step, accepted_time))

@Then(u"(?P<value>'[^']*'%?|[^' ]+) matches pattern '(?P<pattern_str>.*)'")
def step_impl(context, value, pattern_str):
    val = StepTools.evaluate_scenario_parameter(value)
    
    pattern_str = __get_text_interpreter().interpret(pattern_str)
    pattern_str_unescaped = StepTools.unescape_string(pattern_str)
    pattern = re.compile(pattern_str_unescaped)

    if not pattern.search(val):
        raise VerifyFailureException("Match failure, value doesn't match pattern:\n    value : {}\n  pattern : {}".format(val, pattern_str_unescaped))

