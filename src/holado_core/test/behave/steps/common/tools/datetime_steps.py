# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from datetime import datetime, timedelta
from holado_core.common.tools.datetime import DateTime
import logging
from holado_core.common.exceptions.technical_exception import TechnicalException
from dateutil.relativedelta import relativedelta

logger = logging.getLogger(__name__)


def __get_scenario_context():
    return SessionContext.instance().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()


@Given(u"(?P<varname>\w+) = generate date with format (?P<format_value>'[^']+'|\w+) (?P<duration>[0-9]+) (?P<duration_unit>seconds|minutes|hours|days|weeks) (?P<value>ago|ahead)")
def step_impl(context,varname,format_value, duration, duration_unit, value):
    format_value = StepTools.evaluate_scenario_parameter(format_value)
    args={duration_unit:int(duration)}
    deltaT = timedelta(**args)
     
    if value == "ago": 
        date = datetime.now() - deltaT
    elif value == "ahead":
        date = datetime.now() + deltaT
        
    
    v = date.strftime(format_value)
    
    logger.debug("New date : ") 
    logger.debug(v)    
    __get_variable_manager().register_variable(varname, v)

@Given(u"(?P<varname>\w+) = datetime now")
def step_impl(context,varname):
    res = datetime.now()
    __get_variable_manager().register_variable(varname, res)

@Given(u"(?P<varname>\w+) = datetime now in UTC")
def step_impl(context,varname):
    res = datetime.utcnow()
    __get_variable_manager().register_variable(varname, res)

@Given(u"(?P<varname>\w+) = datetime now in TAI")
def step_impl(context,varname):
    res = datetime.utcnow() + timedelta(seconds=37)
    __get_variable_manager().register_variable(varname, res)

@Given(u"(?P<varname>\w+) = datetime (?P<date_str>'[^']+'|\w+)")
def step_impl(context, varname, date_str):
    date_str = StepTools.evaluate_scenario_parameter(date_str)
    res = DateTime.str_2_datetime(date_str)
    __get_variable_manager().register_variable(varname, res)

# @Given(u"(?P<varname>\w+) = datetime (?P<date_str>'[^']+') in UTC")
# def step_impl(context, varname, date_str):
#     res = ...
#     __get_variable_manager().register_variable(varname, res)
#
# @Given(u"(?P<varname>\w+) = datetime (?P<date_str>'[^']+') in TAI")
# def step_impl(context, varname, date_str):
#     res = ...
#     __get_variable_manager().register_variable(varname, res)
    
@Given(u"(?P<varname>\w+) = (?P<date_value>'[^' ]+'|\w+) (?P<operation>[+-]) (?P<duration>[0-9]+|\w+) (?P<duration_unit>microseconds?|milliseconds?|seconds?|minutes?|hours?|days?|weeks?|months?|years?)")
def step_impl(context, varname, date_value, operation, duration, duration_unit):
    date_value = StepTools.evaluate_scenario_parameter(date_value)
    duration = StepTools.evaluate_scenario_parameter(duration)
    if not duration_unit.endswith('s'):
        duration_unit += 's'
    
    args={duration_unit:duration}
    if duration_unit in ['milliseconds']:
        delta = timedelta(**args)
    else:
        delta = relativedelta(**args)
    
    if operation == "-": 
        res = date_value - delta
    elif operation == "+":
        res = date_value + delta
    else:
        raise TechnicalException(f"Unmanaged operation '{operation}'")
  
    __get_variable_manager().register_variable(varname, res)

@Given(u"(?P<varname>\w+) = convert datetime (?P<date_value>'[^' ]+'|\w+) to string with format (?P<format_str>'[^' ]+'|\w+)")
def step_impl(context, varname, date_value, format_str):
    format_value = StepTools.evaluate_scenario_parameter(format_str)
    datev = StepTools.evaluate_scenario_parameter(date_value)
    res = DateTime.datetime_2_str(dt_value=datev, dt_format=format_value)
    __get_variable_manager().register_variable(varname, res)
    
@Given(u"(?P<varname>\w+) = convert datetime (?P<date_value>'[^ ]+'|\w+) to string")
def step_impl(context, varname, date_value):
    datev = StepTools.evaluate_scenario_parameter(date_value)
    res = DateTime.datetime_2_str(dt_value=datev, dt_format=None)
    __get_variable_manager().register_variable(varname, res)