
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import threading
import time
import datetime
from holado.holado_config import Config
import logging
import holado_core.common.threading.thread as ctt


class LoopThread(ctt.InterruptableThread):
    """ Helper class managing a thread."""

    def __init__(self, name, default_wait_timeout = None):
        super().__init__(name, default_wait_timeout=default_wait_timeout)

        # Manage loops
        self.__is_under_loop = threading.Event()
        self.__next_loop_datetime = datetime.datetime.now()
        self.__loop_counter = 0
        
    @property
    def next_loop_datetime(self):
        return self.__next_loop_datetime

    @next_loop_datetime.setter
    def next_loop_datetime(self, dt):
        self.__next_loop_datetime = dt

    @property
    def loop_counter(self):
        return self.__loop_counter
        
    def run(self):
        logging.info("Loop thread '{}' is launched.".format(self.name))

        while not self.is_interrupted:
#             logging.debug("[Thread '{}'] New run".format(self.name))
            try:
                if self.does_need_run_loop():
                    with self._is_lock:
                        self._is_idle.clear()
                        self.__is_under_loop.set()
                        self.__loop_counter += 1
                    
                    self.run_loop()
                    
                    with self._is_lock:
                        self.__is_under_loop.clear()
                        self._is_idle.set()
                else:
                    time.sleep(1)
                    
            except:
                logging.exception("[Thread '{}'] Exception catched during loop".format(self.name))

        logging.info("Loop thread '{}' has finished.".format(self.name))
        
    def does_need_run_loop(self):
        return datetime.datetime.now() >= self.__next_loop_datetime
    
    def run_loop(self):
        raise NotImplementedError()

    def wait_is_under_loop(self, timeout=None):
        self._wait(self.__is_under_loop, "is under loop", timeout=timeout)

