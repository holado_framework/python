
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import super
import logging
import threading
import time
import datetime
from holado_core.common.threading.thread import InterruptableThread
from holado_core.common.threading.functionthreaded import FunctionThreaded


class PeriodicFunctionThreaded(InterruptableThread):
    '''
    Execute periodically in a thread given function with given arguments
    '''
    
    def __init__(self, period_seconds, target, args=None, kwargs=None):
        super().__init__("Periodic function '{}'".format(target))

        self.__period_seconds = period_seconds
        self.__function_threaded = FunctionThreaded(target, args, kwargs)

    @property
    def function_threaded(self):
        return self.__function_threaded


    def run(self):
        logging.info("Thread managing periodic function.")

        while not self.is_interrupted:
            start = datetime.datetime.now()
            
            # Process function
            logging.info("Processing periodic function...")
            try:
                self.__function_threaded.run()
            except Exception as exc:
                msg = "Exception catched while processing function:\n    [{}]".format(repr(exc))
                logging.exception(msg)

            # Wait periodicity
            if self.__period_seconds > 0:
                end = start + datetime.timedelta(milliseconds = self.__period_seconds * 1000)
                logging.info("Waiting {} to process next periodic function...".format(end.strftime("%Y-%m-%d %H-%M-%S")))
                while not self.is_interrupted and datetime.datetime.now() < end and threading.active_count() > 0:
                    time.sleep(1)
            
