
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import super
import logging
from holado_core.common.block.function import Function
from holado_core.common.exceptions.functional_exception import FunctionalException
import holado_core.common.threading.thread as ctt
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.tools.tools import Tools
import time


class FunctionThreaded(ctt.InterruptableThread):
    '''
    Execute in a thread given function with given arguments
    '''
    
    def __init__(self, target, args=None, kwargs=None, name = None, default_wait_timeout = None, register_thread = True, delay_before_run_sec=None):
        super().__init__(name if name is not None else "Function '{}'".format(target), default_wait_timeout = default_wait_timeout, register_thread = register_thread, delay_before_run_sec=delay_before_run_sec)
        
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        
        self._function = Function(target, *args, **kwargs)
        self.__error = None
        self.__result = None
        self.__interrupt_function = None
        self.__callback_function = None

    @property
    def error(self):
        return self.__error

    @property
    def result(self):
        return self.__result

    @property
    def interrupt_function(self):
        return self.__interrupt_function

    @interrupt_function.setter
    def interrupt_function(self, func):
        """Set the Function to call on interrupt"""
        self.__interrupt_function = func

    @property
    def callback_function(self):
        return self.__callback_function

    @callback_function.setter
    def callback_function(self, func):
        """Set the callback function"""
        self.__callback_function = func
 
    def run(self):
        if self.delay_before_run_seconds is not None:
            try:
                time.sleep(self.delay_before_run_seconds)
            except Exception as exc:
                logging.exception(f"Exception while waiting delay before run (delay: {self.delay_before_run_sec} ; type: {type(self.delay_before_run_sec)})")
            
        try:
#             logging.debug("+++++++++ Launching function [{}({})]".format(repr(self._target), repr(self._args)))
            self.__result = self._function.run()
        except Exception as exc:
            logging.exception("Exception catched during threaded function [{}]".format(self._function.represent()))
            self.__error = exc
        
        try:
            if self.__callback_function:
                self.__callback_function.run(self.result, self.error)
        except Exception as exc:
            msg = "Exception catched while calling callback function:\n    callback_function: {}\n    result: [{}]\n    error: [{}]".format(repr(self.__callback_function), Tools.truncate_text(repr(self.result)), repr(self.error))
            logging.exception(msg)
            raise FunctionalException(msg) from exc

    def throw_if_error(self):
        if self.error:
            raise FunctionalException("Error during call of function [{}({})]".format(repr(self._target), repr(self._args))) from self.error
    
    @property    
    def is_interruptable(self):
        return self.interrupt_function is not None
    
    def interrupt(self):
        if not self.is_interrupted and self.is_alive():
            super().interrupt()
            if self.interrupt_function is None:
                raise TechnicalException("Interrupt function is not set in thread '{}'. Use property 'interrupt_function' to set it.".format(self.name))
            self.interrupt_function.run()
