
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import threading
import logging
from holado_core.common.exceptions.technical_exception import TechnicalException
import holado_core.common.threading as ct

class ThreadsManager(object):
    """
    Helper class to manage running threads.
    """
    
    __managed_threads_lock = threading.Lock()
    __managed_threads = {}
    

    @staticmethod
    def register_thread(name, thread):
        with ThreadsManager.__managed_threads_lock:
            if name in ThreadsManager.__managed_threads:
                logging.error(f"A thread with name '{name}' is already registered, it will be overwritten: {ThreadsManager.__managed_threads[name]}")
            else:
                logging.debug(f"Registered thread with name '{name}': {thread}")
            ThreadsManager.__managed_threads[name] = thread

    @staticmethod
    def unregister_thread(name):
        with ThreadsManager.__managed_threads_lock:
            if name in ThreadsManager.__managed_threads:
                if ThreadsManager.__managed_threads[name].is_alive():
                    logging.warn(f"Unregistering thread with name '{name}' whereas it is still alive")
                else:
                    logging.debug(f"Unregistered thread with name '{name}'")
                del ThreadsManager.__managed_threads[name]
            else:
                raise Exception("Not registered thread of name '{}'".format(name))

    @staticmethod
    def get_thread(name):
        with ThreadsManager.__managed_threads_lock:
            if name in ThreadsManager.__managed_threads:
                return ThreadsManager.__managed_threads[name]
            else:
                raise Exception("Unregistered thread of name '{}'".format(name))

    @staticmethod
    def interrupt_thread(name, with_join=True, join_timeout=None, raise_if_not_interruptable=True):
        thread = ThreadsManager.get_thread(name)
        if thread.is_alive():
            if ThreadsManager.is_interruptable(thread=thread):
                try:
                    thread.interrupt()
                except Exception as exc:
                    # logging.exception("Failed to interrupt thread of name '{}'".format(name))
                    raise exc
                
                if with_join:
                    thread.join(join_timeout)
            elif raise_if_not_interruptable:
                raise TechnicalException("Thread '{}' is not interruptable".format(name))
                

    @staticmethod
    def join_thread(name, timeout=None):
        thread = ThreadsManager.get_thread(name)
        if thread.is_alive():
            thread.join(timeout)

    @staticmethod
    def interrupt_all_threads(with_join=True, join_timeout=None):
        """Interrupt all threads that are interruptable."""
        names = ThreadsManager.get_thread_names()
        for name in names:
            try:
                if ThreadsManager.is_interruptable(name=name):
                    ThreadsManager.interrupt_thread(name, with_join=False)
            except Exception:
                logging.exception("Failed to interrupt thread of name '{}'".format(name))

        if with_join:
            ThreadsManager.join_all_threads(join_timeout, skip_not_interruptable=True)

    @staticmethod
    def join_all_threads(timeout=None, skip_not_interruptable=False):
        names = ThreadsManager.get_thread_names()
        for name in names:
            if skip_not_interruptable and ThreadsManager.is_interruptable(name=name):
                continue
            ThreadsManager.join_thread(name, timeout)
    
    @staticmethod
    def unregister_all_threads(keep_alive=True):
        names = ThreadsManager.get_thread_names()
        for name in names:
            thread = ThreadsManager.get_thread(name)
            if not keep_alive or keep_alive and not thread.is_alive():
                ThreadsManager.unregister_thread(name)
    
    @staticmethod
    def get_thread_names():
        with ThreadsManager.__managed_threads_lock:
            return sorted(list( ThreadsManager.__managed_threads.keys() ))
    
    @staticmethod
    def get_alive_thread_names():
        with ThreadsManager.__managed_threads_lock:
            res = sorted(list( ThreadsManager.__managed_threads.keys() ))
            res = [name for name in res if ThreadsManager.__managed_threads[name].is_alive()]
            
    @staticmethod
    def is_interruptable(thread=None, name=None):
        if name is not None:
            thread = ThreadsManager.get_thread(name)
        if thread is None:
            raise TechnicalException("At least one argument must be not None")    
        
        return isinstance(thread, ct.thread.InterruptableThread) and thread.is_interruptable
    