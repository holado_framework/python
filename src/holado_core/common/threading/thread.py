
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import threading
from holado_core.common.threading.threadsmanager import ThreadsManager
from holado.holado_config import Config
import logging


class Thread(threading.Thread):
    """ Helper class managing an interruptable thread."""
    
    def __init__(self, name, default_wait_timeout = None, register_thread = True, delay_before_run_sec=None):
        super().__init__()
        
        if default_wait_timeout is None:
            default_wait_timeout = Config.TimeoutSeconds * 1000
            
        self.__name = name
        self.__default_wait_timeout = default_wait_timeout
        self.__delay_before_run_sec = delay_before_run_sec

        # Put thread as daemon
        self.daemon = True
        
        # Manage thread status
        self._is_lock = threading.Lock()
        self._is_idle = threading.Event()
        self._is_idle.set()
        
        # Register thread
        if register_thread:
            ThreadsManager.register_thread(name, self)
        
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name
        
    @property
    def default_wait_timeout(self):
        return self.__default_wait_timeout
        
    @default_wait_timeout.setter
    def default_wait_timeout(self, timeout):
        self.__default_wait_timeout = timeout
        
    @property
    def delay_before_run_seconds(self):
        return self.__delay_before_run_sec
        
    def join(self, timeout=None):
        logging.debug("Joining thread '{}'{} ...".format(self.name, (" ; timeout={}s".format(timeout) if timeout else "")))
        super().join(timeout)
        logging.debug("Joining thread '{}': end{} ; alive:{}".format(self.name, (" ; timeout={}s".format(timeout) if timeout else ""), self.is_alive()))

    def wait_is_idle(self, timeout=None):
        """ Wait event self._is_idle"""
        self._wait(self.__is_idle, "is idle", timeout=timeout)

    def _wait(self, event, event_details, timeout=None):
        """ Wait given threading.Event"""
        is_set = False
        wait_round_counter = 0
        round_wait_seconds = 1
        if timeout is None:
            timeout = self.__default_wait_timeout
        
        logging.debug("[{}] Waiting {}...".format(self.name, event_details))
        while not is_set:
            wait_round_counter += 1
            is_set = event.wait(round_wait_seconds)
            
            if not is_set:
                if wait_round_counter * round_wait_seconds * 1000 > timeout:
                    raise TimeoutError("[{}] Timeout when waiting {}".format(self.name, event_details))
                elif wait_round_counter % 10 == 0:
                    logging.debug("[{}] Still waiting {} (round {})".format(self.name, event_details, wait_round_counter))
        logging.debug("[{}] Finished waiting {}...".format(self.name, event_details))


class InterruptableThread(Thread):
    """ Helper class managing a thread interruptable."""
    
    def __init__(self, name, default_wait_timeout = None, register_thread = True, delay_before_run_sec=None):
        super().__init__(name, default_wait_timeout = default_wait_timeout, register_thread = register_thread, delay_before_run_sec=delay_before_run_sec)

        # Manage interrupt
        self.__interrupt_lock = threading.Lock()
        self.__interrupt_event = threading.Event()
    
    @property    
    def is_interruptable(self):
        return True
    
    @property
    def is_interrupted(self):
        with self.__interrupt_lock:
            return self.__interrupt_event.is_set()

    def interrupt(self):
        if not self.is_interrupted:
            logging.debug("Interrupt thread '{}'".format(self.name))
            with self.__interrupt_lock:
                self.__interrupt_event.set()
        

