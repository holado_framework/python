
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.tables.table_with_header import TableWithHeader
import inspect
from builtins import object
from holado_core.common.tables.table import Table
from holado_value.common.tables.value_table_with_header import ValueTableWithHeader
from holado_value.common.tables.value_table import ValueTable
from holado_core.common.tools.tools import Tools

logger = logging.getLogger(__name__)


class TableManager(object):
    
    @classmethod
    def is_table_with_header(cls, table):
        return isinstance(table, TableWithHeader)
    
    @classmethod
    def convert_object_attributes_2_name_value_table(cls, obj):
        logger.debug("Converting object to name/value table")

        res = TableWithHeader()
        
        # Set header
        res.header.add_cells_from_contents(["Name", "Value"])
        
        # Get attributes
        attributes = Tools.get_object_attributes(obj)
        
        # Set body
        for attr in attributes:
            res.add_row(cells_content=attr)
            
        return res
    
    @classmethod
    def convert_list_2_column_table(cls, el_list):
        logger.debug("Converting list to column table")

        res = Table()
        
        # Set body
        for el in el_list:
            res.add_row(cells_content=[el])
            
        return res
 
    @classmethod
    def convert_object_list_2_table_with_attributes_as_column(cls, list_obj):
        logger.debug("Converting object list to table with object attributes as column...")
        res = TableWithHeader()

        for obj in list_obj:
            if not res.header:
                if isinstance(obj, dict):
                    header_names = list(obj.keys())
                else:
                    header_names = Tools.get_object_attribute_names(obj)
                res.header.add_cells_from_contents(cells_content=header_names)
                logger.debug(f"Result table - set header {res.header.represent(0)}")
            
            if isinstance(obj, dict):
                values_by_name = obj
            else:
                values_by_name = Tools.get_object_attribute_names(obj)
            logger.debug(f"Result table - add row with {values_by_name}")
            res.add_row(contents_by_colname=values_by_name)

        return res
 
    @classmethod
    def convert_object_list_2_column_table(cls, list_obj):
        logger.debug("Converting object list to column table...")
        res = Table()

        for obj in list_obj:
#            logger.debug(f"+++++ add row with object: {obj}")
            res.add_row(cells_content=[obj])

        return res
        
    
    @classmethod
    def convert_dict_2_name_value_table(cls, obj):
        logger.debug("Converting dictionary to name/value table...")
        res = TableWithHeader()
        
        res.header.add_cells_from_contents(["Name", "Value"])
        
        sorted_dict = dict(sorted(obj.items()))
        for entry in sorted_dict.items():
            res.add_row(cells_content=entry)
            
        return res
    
    @classmethod
    def convert_dict_2_table_with_keys_as_column(cls, obj):
        logger.debug("Converting dictionary to table with keyx as column name...")
        res = TableWithHeader()
        
        res.add_row()
        sorted_dict = dict(sorted(obj.items()))
        for key, value in sorted_dict.items():
            res.add_column(name=key, cells_content=[value])
            
        return res
    
    @classmethod
    def convert_table_2_list_of_tuples(cls, table):
        logger.debug("Converting table to a list of tuples...")
        if isinstance(table, ValueTable) or isinstance(table, ValueTableWithHeader):
            res = [tuple((cell.value for cell in row.cells)) for row in table.rows]
        else:
            res = [tuple((cell.content for cell in row.cells)) for row in table.rows]
        return res
