#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from holado_core.common.tables.table_with_header import TableWithHeader
from holado_core.common.tables.table_row import TableRow
from holado_core.common.exceptions.technical_exception import TechnicalException




class TableConverter(object):
    
    @classmethod
    def convert_table_with_header_to_dict(cls, table):
        res_list = cls.convert_table_with_header_to_dict_list(table)
        if len(res_list) == 0:
            return None
        elif len(res_list) == 1:
            return res_list[0]
        else:
            raise TechnicalException(f"Failed to convert table to dict, since table has more than one row.")
    
    @classmethod
    def convert_table_with_header_to_dict_list(cls, table):
        index_by_name = table.get_column_indexes_by_string_content()

        res = []
        for row in table.rows:
            new_dict = {name: row.get_cell(index).content for name, index in index_by_name.items()}
            res.append(new_dict)
            
        return res
    
    @classmethod
    def convert_dict_list_to_table_with_header(cls, dict_list):
        """
        Builds a table with header from a list of dictionaries.
        """
        header = set()
        for new_dict in dict_list:
            header = header.union(new_dict.keys())
        header = sorted(header)

        res = TableWithHeader()
        res.header = TableRow(cells_content=header)
        
        for new_dict in dict_list:
            res.add_row(contents_by_colname=new_dict)
        
        return res
    