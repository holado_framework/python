
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################



from builtins import super
from holado_core.common.tables.table_cell import TableCell
from holado_core.common.tools.tools import Tools
import copy
from holado_core.common.exceptions.technical_exception import TechnicalException
import logging

logger = logging.getLogger(__name__)


class TableRow(list):
    def __init__(self, cells=None, cells_content=None):
        super().__init__()
        if cells is not None:
            self.cells.extend(cells)
        elif cells_content is not None:
            self.add_cells_from_contents(cells_content)
        
    @property
    def cells(self): 
        return self
    
    @property
    def cells_content(self): 
        return tuple(c.content for c in self.cells)
    
    @property
    def nb_cells(self): 
        return len(self.cells)
    
    def __copy__(self):
        cls = self.__class__
        res = cls()
        
        # Copy cells
        for cell in self.cells:
            res.add_cell(cell=copy.copy(cell))
            
        return res

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, copy.deepcopy(v, memo))
        return result
    
    def add_cell(self, cell=None, content=None):
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"add_cell({cell=} [{type(cell)}], {content=} [{type(content)}])")
        if cell is not None:
            self.cells.append(cell)
        else:
            self.add_cell(cell=self._new_cell(content))
    
    def add_cells_from_contents(self, cells_content):
        for cell_content in cells_content:
            self.add_cell(content=cell_content)
    
    def get_cell(self, index) -> TableCell :
        return self.cells[index]
    
    def _new_cell(self, cell_content):
        return TableCell(cell_content)
    
    def remove_cell(self, index):
        return self.cells.pop(index)
    
    def keep_cells(self, indexes):
        for i in reversed(range(self.nb_cells)):
            if i not in indexes:
                self.remove_cell(i)
    
    def represent(self, indent = 0, value_prefix = None, value_postfix = None, marge_left = " ", marge_right = " "):
        res_list = []
        
        res_list.append(Tools.get_indent_string(indent))
        res_list.append("|")
        for cell in self.cells:
            if marge_left is not None:
                res_list.append(marge_left)
            
            if cell is not None:
                if value_prefix is not None or value_postfix is not None:
                    res_list.append(cell.represent(0, value_prefix, value_postfix))
                else:
                    res_list.append(cell.represent(0))
            else:
                res_list.append("{CELL_IS_NONE}")
            
            if marge_right is not None:
                res_list.append(marge_right)
            
            res_list.append("|")
        
        return "".join(res_list)

    def order_cells(self, indexes=None):
        orig_indexes = list(range(self.nb_cells))
        for cell_index, orig_cell_ind in enumerate(indexes):
            cur_cell_ind = orig_indexes.index(orig_cell_ind)
            if cur_cell_ind != cell_index:
                self.switch_cells(index_1=cell_index, index_2=cur_cell_ind)
                orig_indexes[cell_index], orig_indexes[cur_cell_ind] = orig_indexes[cur_cell_ind], orig_indexes[cell_index]
        
    def switch_cells(self, index_1, index_2):
        if index_1 == index_2:
            raise TechnicalException(f"The two indexes are equal (index = {index_1})")
    
        self.cells[index_1], self.cells[index_2] = self.cells[index_2], self.cells[index_1]

