
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import hashlib
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado.holado_config import Config
import traceback
import sys
import inspect
from holado_core.common.handlers.enums import AccessType
from holado_core.common.tools.converters.converter import Converter
import logging

logger = logging.getLogger(__name__)


def reversed_enumerate(l):
    return zip(range(len(l)-1, -1, -1), reversed(l))

def reversed_range(min_, max_=None, step=-1):
    if max_ is None:
        min_, max_ = 0, min_
    return range(max_-1, min_-1, step)


class Tools(object):
    
    @classmethod
    def get_indent_string(cls, indent):
        return " " * indent
    
    @classmethod
    def indent_string(cls, indent, txt):
        ind_str = Tools.get_indent_string(indent)
        lines = txt.split("\n") if txt else []
        return ind_str + ("\n" + ind_str).join(lines)

    @classmethod
    def truncate_text(cls, text, length = Config.MessageTruncateLength, truncated_suffix = "[...]", is_length_with_suffix=False):
        if len(text) > length:
            if truncated_suffix:
                if is_length_with_suffix:
                    return text[0 : length - len(truncated_suffix)] + truncated_suffix
                else:
                    return text[0 : length] + truncated_suffix
            else:
                return text[0 : length]
        else:
            return text
    
    @classmethod
    def get_object_class(cls, obj):
        if hasattr(obj, "__class__"):
            return obj.__class__
        else:
            return type(obj)
    
    @classmethod
    def get_object_class_fullname(cls, obj):
        obj_class_name = Tools.get_object_class(obj).__name__
        if hasattr(obj, "__module__"):
            return obj.__module__ + "." + obj_class_name
        else:
            return obj_class_name
    
    @classmethod
    def get_object_attributes(cls, obj, access_type=AccessType.Public):
        if hasattr(obj, "__module__") and getattr(obj, "__module__") == 'zeep.objects' and hasattr(obj, "__values__"):
            # Manage zeep objects separately
            d = getattr(obj, "__values__")
            return [(key, d[key]) for key in sorted(d)]
        else:
            try:
                try:
                    if not hasattr(obj, '__bases__'):
                        obj.__bases__ = ()
                except KeyError:
                    obj.__bases__ = ()
                except AttributeError:
                    pass
            
                if access_type == AccessType.Public:
                    return [(name, member) for name, member in inspect.getmembers(obj, lambda a: not inspect.isroutine(a)) if not name.startswith('_')]
                elif access_type == AccessType.Protected:
                    return [(name, member) for name, member in inspect.getmembers(obj, lambda a: not inspect.isroutine(a)) if not name.startswith('__')]
                elif access_type == AccessType.Private:
                    return [(name, member) for name, member in inspect.getmembers(obj, lambda a: not inspect.isroutine(a))]
                else:
                    raise TechnicalException(f"Unmanaged access type '{access_type}'")
            except ModuleNotFoundError:
                return []
    
    @classmethod
    def get_object_attribute_names(cls, obj):
        if hasattr(obj, "__module__") and getattr(obj, "__module__") == 'zeep.objects' and hasattr(obj, "__values__"):
            # Manage zeep objects separately
            d = getattr(obj, "__values__")
            return [key for key in sorted(d)]
        else:
            return [name for name, _ in inspect.getmembers(obj, lambda a: not inspect.isroutine(a)) if not name.startswith('_')]
    
    @classmethod
    def get_object_attribute_values_by_name(cls, obj):
        if hasattr(obj, "__module__") and getattr(obj, "__module__") == 'zeep.objects' and hasattr(obj, "__values__"):
            # Manage zeep objects separately
            return getattr(obj, "__values__")
        else:
            return {name: member for name, member in inspect.getmembers(obj, lambda a: not inspect.isroutine(a)) if not name.startswith('_')}
    
    @classmethod
    def get_class_for_type_name(cls, type_name):
        type_name_splitted = type_name.split('.')
        if len(type_name_splitted) > 1:
            module = __import__(type_name_splitted[0])
            for name in type_name_splitted[1:-1]:
                module = getattr(module, name)
            res = getattr(module, type_name_splitted[-1])
        else:
            res = globals()[type_name]
            
        return res

    @classmethod
    def hexmd5(cls, content):
        m = hashlib.md5()
        
        # Manage list separately for memory purpose
        done = False
        if not isinstance(content, str): 
            try:
                for cont in content:
                    if type(cont) != bytes:
                        cont = cont.encode('utf-8')
                    m.update(cont)
                done = True
            except TypeError:
                done = False
    
        if not done:
            if type(content) != bytes:
                content = content.encode('utf-8')
            m.update(content)
        
        return m.hexdigest()

    @classmethod
    def represent_exception(cls, exc, tb=None):
        if sys.version_info >= (3,10):
            if tb is not None:
                list_format = traceback.format_exception(exc, value=exc, tb=tb)
            else:
                list_format = traceback.format_exception(exc)
        else:
            list_format = traceback.format_exception(type(exc), exc, exc.__traceback__)
        return "".join(list_format)
    
    @classmethod
    def represent_object(cls, obj, indent=0, access_type=AccessType.Public):
        return cls.__represent_object(obj, indent, access_type, [])
    
    @classmethod
    def __represent_object(cls, obj, indent, access_type, _internal):
        if Converter.is_primitive(obj):
            return f"{obj} [{str(type(obj))}]"
        else:
            id_obj = id(obj)
            if id_obj in _internal:
                return f"[RECURSION] (type: {str(type(obj))} ; id: {id_obj})"
            else:
                _internal.append(id_obj)
                
            if Converter.is_dict(obj):
                res_list = [f"[{str(type(obj))}({id_obj})]"]
                # keys = sorted(value.keys())
                keys = list(obj.keys())
                for key in keys:
                    res_list.append(f"    {key}: {cls.__represent_object(obj[key], indent+4, access_type, _internal)}")
                return Tools.indent_string(indent, "\n".join(res_list))
            else:
                logger.trace(f"Representing object of type {type(obj)}")
                attributes = Tools.get_object_attributes(obj, access_type=access_type)
                if attributes:
                    res_list = [f"[{str(type(obj))}({id_obj})]"]
                    for name, value in attributes:
                        res_list.append(f"    {name}: {cls.__represent_object(value, indent+4, access_type, _internal)}")
                    return Tools.indent_string(indent, "\n".join(res_list))
                else:
                    return f"{obj} [{str(type(obj))}({id_obj})]"
                    # raise TechnicalException(f"Unmanaged representation of object of type '{type(obj)}'")
    
    @classmethod
    def has_sub_kwargs(cls, kwargs: dict, prefix: str):
        for k in kwargs.keys():
            if k.startswith(prefix):
                return True
        return False
    
    @classmethod    
    def pop_sub_kwargs(cls, kwargs: dict, prefix: str):
        res = {}
        for k in list(kwargs.keys()):
            if k.startswith(prefix):
                res[k[len(prefix):]] = kwargs.pop(k)
        return res
    
    @classmethod
    def do_log_if_objects_are_different(cls, obj1, obj2):
        try:
            return (obj1 != obj2)
        except Exception as exc:
            logger.trace(f"Assume a log is needed since usually a comparison fails on mismatching types and thus objects are different. Obtained error: {exc}")
            return True
    
    @classmethod
    def raise_same_exception_type(cls, exception, new_message, add_from=True):
        exc = type(exception)(new_message)
        if add_from:
            raise exc from exception
        else:
            raise exc
    
    
