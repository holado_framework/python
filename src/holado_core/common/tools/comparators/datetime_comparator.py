
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.tools.comparators.comparator import Comparator
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.tools.datetime import DateTime
from datetime import datetime, timezone

logger = logging.getLogger(__name__)

try:
    from google.protobuf import timestamp_pb2
    with_google_protobuf = True
except Exception as exc:
    logger.warning(f"In DatetimeComparator, google protobuf Timestamp type is not managed. Initialization failed on error: {exc}")
    with_google_protobuf = False



class DatetimeComparator(Comparator):
    
    def __init__(self):
        super().__init__("datetime")
        
    def _convert_input(self, obj, name):
        if isinstance(obj, str) and DateTime.is_str_datetime(obj):
            res = DateTime.str_2_datetime(obj)
        elif with_google_protobuf and isinstance(obj, timestamp_pb2.Timestamp):  # @UndefinedVariable       # TODO EKL: move in sub-project holado_protobuf
            # res = obj.ToDatetime()
            res = datetime.utcfromtimestamp(obj.seconds + obj.nanos/1e9)
        elif isinstance(obj, int) or isinstance(obj, float):
            res = datetime.utcfromtimestamp(obj)
        else:
            res = obj
            
        if not isinstance(res, datetime):
            raise FunctionalException(f"{name.capitalize()} value is not a datetime: [{res}] (type: {type(res)})")
        if res.tzinfo is None:
            res = res.replace(tzinfo=timezone.utc)
        return res
        # return res.isoformat()




