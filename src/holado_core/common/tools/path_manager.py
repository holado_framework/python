
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
import os
from glob import glob
import shutil
import re
from holado_core.common.exceptions.technical_exception import TechnicalException

logger = logging.getLogger(__name__)


class PathManager(object):

    def __init__(self):
        pass

    def makedirs(self, path, mode = None, is_directory = False, uid = None, gid = None):
        if not is_directory:
            path = os.path.dirname(path)
        if not os.path.exists(path):
            if mode:
                os.makedirs(path, mode)
            else:
                os.makedirs(path)
            self.chown(path, uid, gid)
            
    def remove_paths(self, glob_pattern, ignore_errors=False):
        paths = glob(glob_pattern)
        for path in paths:
            if os.path.isfile(path):
                try:
                    os.remove(path)
                except Exception as exc:
                    if not ignore_errors:
                        raise exc
            elif os.path.isdir(path):
                shutil.rmtree(path, ignore_errors=ignore_errors)
                
    def copy_path_recursively(self, src_path, dst_path, filter_patterns = None, ignore_patterns = None, do_log = False, log_prefix = "", uid = None, gid = None):
    #         logging.debug("Copying path '{}' -> '{}' (ignoring {})".format(src_path, dst_path, ignore_patterns))
        # Copy path
        if os.path.isfile(src_path):
            if self.__filter_path(src_path, filter_patterns=filter_patterns, ignore_patterns=ignore_patterns, do_log=do_log, log_prefix=log_prefix):
                if do_log:
                    logging.debug("{}Copy file '{}' -> '{}".format(log_prefix, src_path, dst_path))
                self.makedirs(dst_path, uid=uid, gid=gid)
                shutil.copy2(src_path, dst_path)
                self.chown(dst_path, uid, gid)
        elif os.path.isdir(src_path):
            lp = os.listdir(src_path)
            for cp in lp:
                cur_src_path = os.path.join(src_path, cp)
                cur_dst_path = os.path.join(dst_path, cp)
                self.copy_path_recursively(cur_src_path, cur_dst_path, filter_patterns=filter_patterns, ignore_patterns=ignore_patterns, do_log=do_log, log_prefix=log_prefix, uid=uid, gid=gid)
    
    def __filter_paths(self, paths, filter_patterns = None, ignore_patterns = None, do_log = False, log_prefix = ""):
        if filter_patterns is not None and not isinstance(filter_patterns, list):
            raise TechnicalException("Parameter 'filter_patterns' must be None or a list of patterns")
        if ignore_patterns is not None and not isinstance(ignore_patterns, list):
            raise TechnicalException("Parameter 'ignore_patterns' must be None or a list of patterns")
        
        res = []
        for path in paths:
            if self.__filter_path(path, filter_patterns, ignore_patterns, do_log, log_prefix):
                res.append(path)
                
        return res
    
    def __filter_path(self, path, filter_patterns = None, ignore_patterns = None, do_log = False, log_prefix = ""):
        if filter_patterns is not None and not isinstance(filter_patterns, list):
            raise TechnicalException("Parameter 'filter_patterns' must be None or a list of patterns")
        if ignore_patterns is not None and not isinstance(ignore_patterns, list):
            raise TechnicalException("Parameter 'ignore_patterns' must be None or a list of patterns")
        
        if filter_patterns is not None and len(filter_patterns) > 0:
            is_matching_a_filter = False
            for pattern in filter_patterns:
                if re.match(pattern, path):
                    if do_log:
                        logger.debug(f"{log_prefix}File '{path}' is matching filter '{pattern}'")
                    is_matching_a_filter = True
                    break
            if not is_matching_a_filter:
                if do_log:
                    logger.debug(f"{log_prefix}File '{path}' is excluded by filters {filter_patterns}")
                return False

        if ignore_patterns is not None and len(ignore_patterns) > 0:
            is_ignored = False
            for pattern in ignore_patterns:
                if re.match(pattern, path):
                    if do_log:
                        logger.debug(f"{log_prefix}File '{path}' is ignored since it matches pattern '{pattern}'")
                    is_ignored = True
                    break
            if is_ignored:
                return False

        return True
    
    def rename(self, src, dst, raise_if_exists=True):
        if raise_if_exists:
            os.rename(src, dst)
        else:
            os.replace(src, dst)
    
    def get_local_resources_path(self, name=None):
        if name is not None:
            return os.path.join(os.environ['HOME'], 'Projects', 'Tests', 'Resources', name)
        else:
            return os.path.join(os.environ['HOME'], 'Projects', 'Tests', 'Resources')
    
    def get_reports_path(self, name=None):
        if name is not None:
            return os.path.join(os.environ['HOME'], 'Projects', 'Tests', 'Reports', name)
        else:
            return os.path.join(os.environ['HOME'], 'Projects', 'Tests', 'Reports')
    
    def chown(self, path, uid = None, gid = None):
        if uid is not None or gid is not None:
            if uid is None or gid is None:
                raise TechnicalException(f"User and group ID cannot be None (uid={uid} ; gid={gid})")
            os.chown(path, uid, gid)

