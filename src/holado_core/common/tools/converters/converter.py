#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from holado_core.common.exceptions.technical_exception import TechnicalException



class Converter(object):
    
    @classmethod
    def is_boolean(cls, value):
        return isinstance(value, bool)\
            or isinstance(value, str) and (value == "False" or value == "True")

    @classmethod
    def to_boolean(cls, value):
        if isinstance(value, bool):
            return value
        elif isinstance(value, str):
            if value == "True":
                return True
            elif value == "False":
                return False
            else:
                raise TechnicalException(f"String value '{value}' is not a boolean")
        else:
            raise TechnicalException(f"Value [{value}] (type: {type(value)}) is not a boolean")

    @classmethod
    def is_dict(cls, obj):
        return isinstance(obj, dict)

    @classmethod
    def to_dict(cls, value):
        if isinstance(value, dict):
            return value
        else:
            return dict(value)

    @classmethod
    def is_integer(cls, value):
        try:
            int(value)
        except ValueError:
            return False
        else:
            return True

    @classmethod
    def to_integer(cls, value):
        if isinstance(value, int):
            return value
        else:
            return int(value)

    @classmethod
    def is_float(cls, value):
        try:
            float(value)
        except ValueError:
            return False
        else:
            return True

    @classmethod
    def to_float(cls, value):
        if isinstance(value, float):
            return value
        else:
            return float(value)
        
    @classmethod
    def is_primitive(cls, obj):
        return not hasattr(obj, '__dict__') or type(obj) == type

    