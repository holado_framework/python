
import logging
import datetime
import dateutil.parser
import re
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.exceptions.technical_exception import TechnicalException

logger = logging.getLogger(__name__)


EPOCH_1970 = datetime.datetime(year=1970, month=1, day=1, tzinfo=datetime.timezone.utc)


class DateTime(object):
    
    def __init__(self):
        pass

    @classmethod
    def is_str_datetime(cls, dt_str, dt_format=None):
        """Return if a string is a datetime of given format"""
        res = True
        try:
            DateTime.str_2_datetime(dt_str, dt_format)
        except (dateutil.parser._parser.ParserError, ValueError):
            res = False
        return res

    @classmethod
    def str_2_datetime(cls, dt_str, dt_format=None):
        """Convert string to datetime instance"""
        res = None
        if dt_format is None:
            res = dateutil.parser.parse(dt_str)
        else:
            res = datetime.datetime.strptime(dt_str, dt_format)
            # if res.tzinfo is None:
            #     res = res.replace(tzinfo=timezone.utc)
        return res

    @classmethod
    def str_2_timedelta(cls, dur_str):
        """Convert string to timedelta instance"""
        res = None
        dur_regex = re.compile("(\d+(?:\.\d*)?) (?:s|sec|seconds)")
        r = dur_regex.match(dur_str)
        if r:
            res = datetime.timedelta(seconds=float(r.group(1)))
        else:
            raise FunctionalException("Duration string must match pattern '\d+(\.\d*)? (s|sec|seconds)'")
        return res
    
    @classmethod
    def datetime_2_str(cls, dt_value, dt_format=None):
        res = None
        if dt_format is None:
            res = datetime.datetime.isoformat(dt_value)
        else:
            res = dt_value.strftime(dt_format)
        return res
    
    @classmethod
    def datetime_to_timestamp(cls, dt, epoch=None):
        if epoch is None:
            return dt.timestamp()
        else:
            return (dt - epoch).total_seconds()
    
    @classmethod
    def timestamp_to_datetime(cls, ts, epoch=None):
        """
        Convert timestamp to UTC datetime
        """
        if not isinstance(epoch, datetime):
            raise TechnicalException(f"Parameter 'epoch' must be a datetime")
        res = datetime.datetime.utcfromtimestamp(ts)
        if epoch is not None:
            res = epoch + (res - EPOCH_1970)
        return res
    
    
    
