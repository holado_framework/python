
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import super
import logging
from holado_core.common.block.base import BaseScope
from holado_core.common.block.block_steps import BlockSteps
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_test.behave.behave import execute_steps
from holado_test.scenario.step_tools import StepTools
from holado_core.common.tools.converters.converter import Converter

logger = logging.getLogger(__name__)


class ScopeSteps(BaseScope):
    '''
    Scope block that should only contains steps.
    This scope block is used when a new scope (function, for,...) is defined in a scenario.

    When processing this scope, a 'context' is supposed to be passed as parameter to 'process' method
    Warning: it remains possible to add any block, not only steps. Be carefull that added blocks are compatible with parameter 'context' passed to 'process' method.
    '''
    
    def __init__(self, name, ID=None):
        super().__init__(f"steps scope {name}", ID=ID)
        
    def add_steps(self, steps_str):
        logger.debug(f"In scope '{self.name}', add steps:\n{steps_str}")
        self.add_block( BlockSteps(steps_str) )
    
class ScopeForSteps(ScopeSteps):
    def __init__(self, name, iter_var_name, iterable, ID=None, variable_manager=None, scope_manager=None):
        super().__init__(name, ID=ID)
        
        self.__iter_var_name = iter_var_name
        self.__iterable = iterable
        
        self.__variable_manager = variable_manager
        self.__scope_manager = scope_manager
    
    def process(self, *args, **kwargs):
        """
        Process the scope.
        Returns last scope step result
        """
        res = None
        logger.debug(f"Processing scope [{self.name}]: begin")

        self._process_start()
        try:
            for index, value in enumerate(self.__iterable):
                execute_steps(f"Given FOR_INDEX_{self.__iter_var_name} = {index}")
                self.__variable_manager.register_variable(self.__iter_var_name, value)
                
                if self.__scope_manager:
                    level_already_exists = self.__scope_manager.has_scope_level("steps")
                    origin_level = self.__scope_manager.scope_level("steps")
                    if level_already_exists:
                        self.__scope_manager.increase_scope_level("steps")
                
                try:
                    self._process_blocks(*args, **kwargs)
                finally:
                    if self.__scope_manager:
                        self.__scope_manager.reset_scope_level("steps", origin_level)
        finally:
            self._process_end()
            logger.debug(f"Processing scope [{self.name}]: end")

        return res
    
class ScopeWhileSteps(ScopeSteps):
    def __init__(self, name, condition_expression, ID=None, variable_manager=None, scope_manager=None):
        super().__init__(name, ID=ID)
        
        self.__condition_expression = condition_expression
        
        self.__variable_manager = variable_manager
        self.__scope_manager = scope_manager
    
    def process(self, *args, **kwargs):
        """
        Process the scope.
        Returns last scope step result
        """
        res = None
        logger.debug(f"Processing scope [{self.name}]: begin")

        self._process_start()
        try:
            n = 0
            cond = StepTools.evaluate_scenario_parameter(self.__condition_expression, log_level=logging.TRACE)  # @UndefinedVariable
            while isinstance(cond, bool) and cond or Converter.is_boolean(cond) and Converter.to_boolean(cond):
                n += 1
                execute_steps(f"Given WHILE_ITERATION = {n}")
                
                if self.__scope_manager:
                    level_already_exists = self.__scope_manager.has_scope_level("steps")
                    origin_level = self.__scope_manager.scope_level("steps")
                    if level_already_exists:
                        self.__scope_manager.increase_scope_level("steps")
                
                try:
                    self._process_blocks(*args, **kwargs)
                finally:
                    if self.__scope_manager:
                        self.__scope_manager.reset_scope_level("steps", origin_level)
                
                cond = StepTools.evaluate_scenario_parameter(self.__condition_expression, log_level=logging.TRACE)  # @UndefinedVariable
                    
        finally:
            self._process_end()
            logger.debug(f"Processing scope [{self.name}]: end")

        return res
    
