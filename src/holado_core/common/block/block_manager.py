
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.block.scope_function import ScopeFunction

logger = logging.getLogger(__name__)


class BlockManager(object):
    '''
    Manage blocks.
    
    Currently it manages definition of functions.
    '''
    
    def __init__(self):
        self.__scope_by_id = {}
        self.__scope_in_define_id = None
        self.__scope_define_level = 0
        
    @property
    def is_in_define(self):
        return self.__scope_in_define_id is not None
    
    @property
    def is_in_sub_define(self):
        return self.__scope_define_level > 1
    
    @property
    def scope_in_define(self):
        if not self.is_in_define:
            raise FunctionalException("No block is under define")
        return self.__scope_by_id[self.__scope_in_define_id]
    
    def begin_define_scope(self, name, block_scope_type=None):
        """
        Begin define of a function of given name.
        Parameter block_function_type specifies the scope class to use (any class inheriting from BaseScope ; default: BlockFunction).
        """
        if block_scope_type is None:
            block_scope_type = ScopeFunction
            
        if self.is_in_define:
            raise FunctionalException("A block is already under define")

        func_id = self.__get_scope_ID(name)
        if func_id in self.__scope_by_id:
            raise FunctionalException(f"Function '{name}' was already defined")
        
        block_func = block_scope_type(ID=func_id, name=name)
        
        self.__scope_define_level = 1
        self.__scope_in_define_id = func_id
        self.__scope_by_id[func_id] = block_func
        
        logger.debug(f"[BlockManager] Define of '{self.scope_in_define.name}' => begin")
        
    def end_define_scope(self, name):
        if not self.is_in_define:
            raise FunctionalException("No block is under define")
        
        logger.debug(f"[BlockManager] Define of '{self.scope_in_define.name}' => end")
        self.__scope_in_define_id = None
        self.__scope_define_level = 0

    def increase_define_level(self):
        self.__scope_define_level += 1

    def decrease_define_level(self):
        self.__scope_define_level -= 1
        
    def has_scope(self, name):
        func_id = self.__get_scope_ID(name)
        return func_id in self.__scope_by_id

    def delete_scope(self, name):
        func_id = self.__get_scope_ID(name)
        
        # Check function
        self.__check_block_is_not_under_define(func_id)
        self.__check_block_is_defined(func_id)
        
        # Delete function
        del self.__scope_by_id[func_id]

    def __check_block_is_not_under_define(self, block_id):
        if self.__scope_in_define_id == block_id:
            raise FunctionalException(f"Block '{self.scope_in_define.name}' is under define")

    def __check_block_is_defined(self, block_id):
        if block_id not in self.__scope_by_id:
            raise FunctionalException(f"Block of ID '{block_id}' is not define")

    def __get_scope_ID(self, name):
        return f"SCOPE[{name}]"

    def process_scope(self, scope_name, *args, **kwargs):
        res = None
        func_id = self.__get_scope_ID(scope_name)
        
        # Check function
        self.__check_block_is_not_under_define(func_id)
        self.__check_block_is_defined(func_id)
        
        # Start function
        func = self.__scope_by_id[func_id]
        try:
            res = func.process(*args, **kwargs)
        except (FunctionalException, TechnicalException) as exc:
            raise exc
        except Exception as exc:
            raise TechnicalException(exc)
        
        return res

