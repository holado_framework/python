
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import super
import logging
from holado_core.common.block.base import BaseBlock
from holado_test.behave.behave import execute_steps

logger = logging.getLogger(__name__)



class BlockSteps(BaseBlock):
    '''
    Step block
    '''
    
    def __init__(self, steps_str):
        super().__init__(f"step")
        self.__steps_str = steps_str
    
    def process(self, *args, **kwargs):
        logger.info(f"Executing block steps:\n{self.__steps_str}")
        execute_steps(self.__steps_str)
    
    

