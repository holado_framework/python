
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_core.common.exceptions.technical_exception import TechnicalException
import logging

logger = logging.getLogger(__name__)

try:
    import requests
    from requests.auth import HTTPBasicAuth
    with_requests = True
except Exception as exc:
    logger.debug(f"RestClient is not available. Initialization failed on error: {exc}")
    with_requests = False


class RestClient(object):
    """
    REST client
    """
    
    @classmethod
    def is_available(cls):
        return with_requests
    
    def __init__(self, url, headers=None):
        self.__url = url.rstrip('/')
        self.__headers = headers if headers is not None else {}
        self.__kwargs = {}
    
    def authenticate_by_user(self, user, pwd):
        logger.debug(f"Authenticate with user ({user},{pwd})")
        self.__kwargs['auth'] = HTTPBasicAuth(user, pwd)
    
    def authenticate_by_token(self, token_type, access_token):
        logger.debug(f"Authenticate with token type '{token_type}' and access token '{access_token}'")
        self.__headers['Authorization'] = f'{token_type} {access_token}'
        
    def response_result(self, response, status_ok=200):
        if response.status_code != status_ok:
            raise FunctionalException(f"Request failed with status {response.status_code} (expected success status: {status_ok}")
        
        if "application/json" in response.headers['Content-Type']:
            return response.json()
        else:
            return response.text
        
    def delete(self, path):
        url = self.__build_url(path=path)
        kwargs = self.__build_kwargs()
        
        logger.debug(f"Calling DELETE on url '{url}'\n    kwargs: {kwargs}")
        try:
            res = requests.delete(url, **kwargs)
        except Exception as exc:
            raise TechnicalException(f"Failed to process DELETE '{url}' with arguments {kwargs}") from exc
        
        logger.debug(f"DELETE on url '{url}' => {res}")
        return res
        
    def get(self, path, params=None):
        url = self.__build_url(path=path)
        kwargs = self.__build_kwargs()
        
        logger.debug(f"Calling GET on url '{url}'\n    kwargs: {kwargs}\n    params: {params}")
        try:
            res = requests.get(url, params, **kwargs)
        except Exception as exc:
            raise TechnicalException(f"Failed to process GET '{url}' with arguments {kwargs}") from exc
        
        logger.debug(f"GET on url '{url}' => {res}")
        return res
        
    def patch(self, path, data=None):
        url = self.__build_url(path=path)
        kwargs = self.__build_kwargs()
        
        logger.debug(f"Calling PATCH on url '{url}'\n    kwargs: {kwargs}\n    data: {data}")
        try:
            res = requests.patch(url, data, **kwargs)
        except Exception as exc:
            raise TechnicalException(f"Failed to process PATCH '{url}' with arguments {kwargs}") from exc
        
        logger.debug(f"PATCH on url '{url}' => {res}")
        return res
        
    def post(self, path, data=None, json=None):
        url = self.__build_url(path=path)
        kwargs = self.__build_kwargs()
        
        logger.debug(f"Calling POST on url '{url}'\n    kwargs: {kwargs}\n    data: {data}\n    json: {json}")
        try:
            res = requests.post(url, data, json, **kwargs)
        except Exception as exc:
            raise TechnicalException(f"Failed to process PATCH '{url}' with arguments {kwargs}") from exc
        
        logger.debug(f"POST on url '{url}' => {res}")
        return res
        
    def put(self, path, data=None):
        url = self.__build_url(path=path)
        kwargs = self.__build_kwargs()
        
        logger.debug(f"Calling PUT on url '{url}'\n    kwargs: {kwargs}\n    data: {data}")
        try:
            res = requests.put(url, data, **kwargs)
        except Exception as exc:
            raise TechnicalException(f"Failed to process PATCH '{url}' with arguments {kwargs}") from exc
        
        logger.debug(f"PUT on url '{url}' => {res}")
        return res
        
    def __build_url(self, path):
        return f"{self.__url}/{path.lstrip('/')}" 
        
    def __build_kwargs(self):
        res = dict(self.__kwargs)
        if self.__headers:
            res['headers'] = self.__headers
        return res 
        
        
