# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_value.common.tables.value_table_manager import ValueTableManager
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_rest.api.rest.rest_client import RestClient
import json
import logging
from holado_rest.api.rest.rest_manager import RestManager
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools
from holado_core.common.tables.table_manager import TableManager

logger = logging.getLogger(__name__)


if RestClient.is_available():

    def __get_session_context():
        return SessionContext.instance()
    
    def __get_scenario_context():
        return __get_session_context().get_scenario_context()
    
    def __get_text_interpreter():
        return __get_scenario_context().get_text_interpreter()
    
    def __get_variable_manager():
        return __get_scenario_context().get_variable_manager()
    
        
    @Given(u"(?P<var_name>\w+) = new REST client")
    def step_impl(context, var_name):
        table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
        kwargs = ValueTableManager.convert_name_value_table_2_dict(table)
        
        manager = RestManager()
        client = manager.new_client(**kwargs)
    
        __get_variable_manager().register_variable(var_name, client)
        
    @Step(u"(?P<var_name>\w+) = response of (?P<request_method>delete|get|patch|post|put) (?P<path>'[^']*'|[^' ]+) \(REST client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, request_method, path, var_client):
        path = StepTools.evaluate_scenario_parameter(path)
        client = __get_variable_manager().get_variable_value(var_client)

        # Manage data as table
        data = None
        if hasattr(context, "table") and context.table is not None:
            if request_method in ['delete']:
                raise TechnicalException(f"Unexpected data for request method '{request_method}'")        
            table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
            data_obj = ValueTableManager.convert_name_value_table_2_json_object(table)
            data = json.dumps(data_obj)
            logger.trace(f"Transformed parameters to JSON is: {data}")
            
        # Manage data as json text
        text = BehaveStepTools.get_step_multiline_text(context, raise_exception_if_none=False)
        if text is not None:
            if request_method in ['delete', 'get']:
                raise TechnicalException(f"Unexpected json text for request method '{request_method}'")
            if data is not None:
                raise TechnicalException(f"Data is already set to [{data}]")
            data = text
        
        if request_method == 'delete':        
            res = client.delete(path)
        elif request_method == 'get':
            res = client.get(path, data)
        elif request_method == 'patch':        
            res = client.patch(path, data)
        elif request_method == 'post':        
            res = client.post(path, data)
        elif request_method == 'put':        
            res = client.put(path, data)
        else:
            raise TechnicalException(f"Unmanaged request method '{request_method}'")
        
        if logger.isEnabledFor(logging.TRACE):  # @UndefinedVariable
            res_table = TableManager.convert_object_attributes_2_name_value_table(res)
            logger.debug(f"Response of {request_method} {path}:\n{res_table.represent(4)}")
        __get_variable_manager().register_variable(var_name, res)
        
    @When(u"(?P<var_name>\w+) = (?P<request_method>delete|get|patch|post|put) (?P<path>'[^']*'|[^' ]+) \(REST client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, request_method, path, var_client):
        client = __get_variable_manager().get_variable_value(var_client)

        execute_steps(format_step_with_context(context, 
                u"__RESPONSE__ = response of {request_method} {path} (REST client: {var_client})"
                    .format(request_method=request_method, path=path, var_client=var_client) ))
        
        response = __get_variable_manager().get_variable_value("__RESPONSE__")
        res = client.response_result(response)
        
        __get_variable_manager().register_variable(var_name, res)
        
        
    
    
