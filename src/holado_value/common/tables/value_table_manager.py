
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_value.common.tables.value_table_with_header import ValueTableWithHeader
from holado_core.common.tables.table_row import TableRow
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.tables.table_manager import TableManager
from holado_core.common.tables.comparators.string_table_row_comparator import StringTableRowComparator
import re
from holado_value.common.tools.value_types import ValueTypes
from holado_json.ipc import json
from holado.common.context.session_context import SessionContext
from holado_core.common.tools.tools import Tools

logger = logging.getLogger(__name__)


class ValueTableManager(TableManager):
    
 
    @classmethod
    def convert_name_value_table_2_dict(cls, table, with_original_value_content = False):
        logger.trace(f"Converting Name/Value table to dict (table = {table})")

        # Verify table structure
        ValueTableManager.verify_table_is_name_value_table(table)
        
        res = {}
        for row in table.rows:
            logger.trace("Converting row (%s)", row)
            
            name = row.get_cell(0).value
            if with_original_value_content:
                value = row.get_cell(1).content
            else:
                value = row.get_cell(1).value
            
            if name in res:
                raise FunctionalException("Name '{}' appears several times in table".format(name))
            else:
                res[name] = value
                
        logger.debug(f"Converting Name/Value table to dict (table = {table}) => {res}")
        return res
        
    @classmethod
    def convert_name_value_table_2_list_and_dict(cls, table, with_original_value_content = False):
        logger.trace(f"Converting Name/Value table to list and dict (table = {table})")

        # Verify table structure
        ValueTableManager.verify_table_is_name_value_table(table)
        
        res_list = []
        res_dict = {}
        for row in table.rows:
            logger.trace("Converting row (%s)", row)
            
            name = row.get_cell(0).value
            if with_original_value_content:
                value = row.get_cell(1).content
            else:
                value = row.get_cell(1).value
            
            if name and len(name) > 0:
                if name in res_dict:
                    raise FunctionalException("Name '{}' appears several times in table".format(name))
                else:
                    res_dict[name] = value
            else:
                res_list.append(value)
                
        logger.debug(f"Converting Name/Value table to list and dict (table = {table}) => {res_list} ; {res_dict}")
        return [res_list, res_dict]
 
    @classmethod
    def convert_name_value_table_2_json_object(cls, table):
        logger.trace(f"Converting Name/Value table to json object (table = {table})")

        # Verify table structure
        ValueTableManager.verify_table_is_name_value_table(table)
        
        res = {}
        for row in table.rows:
            logger.trace("Converting row (%s)", row)
            
            if row.get_cell(1).value_type in [ValueTypes.NotApplicable, ValueTypes.Merged]:
                pass
            elif row.get_cell(1).value_type in [ValueTypes.String, ValueTypes.DynamicString, ValueTypes.UniqueString]:
                name = row.get_cell(0).value
                value = row.get_cell(1).value
                
                logger.trace(f"Adding for name '{name}' string value [{value}] (type: {type(value)})")
                cls.__fill_json_dict(res, name, value)
            else:
                name = row.get_cell(0).value
                value = row.get_cell(1).value
                value = json.convert_to_json(value)
                
                logger.trace(f"Adding for name '{name}' json value [{value}] (type: {type(value)})")
                cls.__fill_json_dict(res, name, value)
                
        logger.debug(f"Converting Name/Value table to json object (table = {table}) => {res}")
        return res
 
    @classmethod
    def __fill_json_dict(cls, json_res, name, value):
        if not isinstance(json_res, dict):
            raise TechnicalException(f"Type '{type(json_res)}' is not a dict")
        
        names = name.split(".", maxsplit=1)
        name0 = names[0]
        name1 = names[1] if len(names) > 1 else None
        
        # manage lists
        m = re.match("^([^[]+)\[(.*)\]$", name0)
        if m:
            li_name = m.group(1)
            index = int(m.group(2))

            if li_name not in json_res:
                json_res[li_name] = []
            cls.__fill_json_list(json_res[li_name], index, name1, value)
            return
        
        if name1 is None:
            cls.__fill_json_dict_entry(json_res, name0, value)
        else:
            if name0 not in json_res:
                json_res[name0] = {}
            cls.__fill_json_dict(json_res[name0], name1, value)
 
    @classmethod
    def __fill_json_dict_entry(cls, json_res, name, value):
        if name in json_res:
            raise FunctionalException(f"Name '{name}' is already set")
        else:
            json_res[name] = value
 
    @classmethod
    def __fill_json_list(cls, json_res, index, name, value):
        if not isinstance(json_res, list):
            raise TechnicalException(f"Type '{type(json_res)}' is not a list")
        
        # Prepare list with missing elements
        if index + 1 > len(json_res):
            for _ in range(len(json_res), index + 1):
                json_res.append(None)
        
        if name is None:
            if json_res[index] is None:
                json_res[index] = value
            else:        
                raise FunctionalException("List has already an element at index {}".format(index))
        else:
            if json_res[index] is None:
                json_res[index] = {}
            cls.__fill_json_dict(json_res[index], name, value)
            

    @classmethod
    def convert_x_name_value_table_2_list_x_values(cls, table, x_name, with_original_value_content = False):
        logger.trace(f"Converting {x_name}/Name/Value table to list of {x_name} and values dict (table = {table})")

        # Verify table structure
        ValueTableManager.verify_table_is_x_name_value_table(table, x_name)
        
        x_list = []
        res_dict = {}
        for row in table.rows:
            logger.trace("Converting row (%s)", row)

            x = row.get_cell(0).value
            name = row.get_cell(1).value
            if with_original_value_content:
                value = row.get_cell(2).content
            else:
                value = row.get_cell(2).value
            
            if x not in res_dict:
                res_dict[x] = {}
                x_list.append(x)
            res_dict[x][name] = value
                
        res = [{x_name:x, 'values':res_dict[x]} for x in x_list]
        
        logger.debug(f"Converting {x_name}/Name/Value table to list of {x_name} and values dict (table = {table}) => {res}")
        return res
        
    @classmethod
    def set_object_attributes_according_name_value_table(cls, obj, table):
        # Verify table structure
        ValueTableManager.verify_table_is_name_value_table(table)
        
        for row in table.rows:
            setattr(obj, row.get_cell(0).value, row.get_cell(1).value)
        
    @classmethod
    def verify_table_is_value_table(cls, table):
        # Compare headers
        if not isinstance(table, ValueTableWithHeader):
            raise TechnicalException("Table is expected of type ValueTableWithHeader (obtained type: {})".format(type(table)))
        if len(table.header) != 1:
            raise FunctionalException("A Value table must have one column")
        
        # Build expected header
        expected = TableRow(cells_content=["Value"])
        
        # Verify header content
        comparator = StringTableRowComparator()
        comparator.equals(expected, table.header)
        
    @classmethod
    def verify_table_is_name_value_table(cls, table):
        # Compare headers
        if not isinstance(table, ValueTableWithHeader):
            raise TechnicalException("Table is expected of type ValueTableWithHeader (obtained type: {})".format(type(table)))
        if len(table.header) != 2:
            raise FunctionalException("A Name/Value table must have two columns")
        
        # Build expected header
        expected = TableRow(cells_content=["Name", "Value"])
        
        # Verify header content
        comparator = StringTableRowComparator()
        comparator.equals(expected, table.header)
        
    @classmethod
    def verify_table_is_x_name_value_table(cls, table, x_name):
        # Compare headers
        if not isinstance(table, ValueTableWithHeader):
            raise TechnicalException("Table is expected of type ValueTableWithHeader (obtained type: {})".format(type(table)))
        if len(table.header) != 3:
            raise FunctionalException("A {}/Name/Value table must have 3 columns".format(x_name))
        
        # Build expected header
        expected = TableRow(cells_content=[x_name, "Name", "Value"])
        
        # Verify header content
        comparator = StringTableRowComparator()
        comparator.equals(expected, table.header)
        
    @classmethod
    def verify_table_is_x_table(cls, table, *x_names, raise_exception = True):
        # Compare headers
        if not isinstance(table, ValueTableWithHeader):
            raise TechnicalException("Table is expected of type ValueTableWithHeader (obtained type: {})".format(type(table)))
        
        # Build expected header
        expected = TableRow(cells_content=x_names)
        
        # Verify header content
        comparator = StringTableRowComparator()
        return comparator.equals(expected, table.header, raise_exception = raise_exception)
    
    @classmethod
    def represent_table(cls, table, indent=0):
        res_list = []
        
        if cls.is_table_with_header(table):
            res_list.append("| " + " | ".join(table.header.cells_content) + " |")
        
        for row in table.rows:
            res_list.append("| " + " | ".join(row.cells_content) + " |")
            
        return Tools.indent_string(indent, "\n".join(res_list))
    
