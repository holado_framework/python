
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################



from holado_value.common.tables.value_table import ValueTable
from holado_value.common.tables.value_table_with_header import ValueTableWithHeader
from holado_core.common.tables.table_row import TableRow
from holado_core.common.tables.converters.table_converter import TableConverter


class ValueTableConverter(TableConverter):
    
    @classmethod
    def convert_table_2_value_table(cls, table):
        res = ValueTable()
        for row in table:
            cells_content = [f"'{cc}'" if isinstance(cc, str) else f"{cc}" for cc in row.cells_content]
            res.add_row(cells_content=cells_content)
        return res
    
    @classmethod
    def convert_table_with_header_2_value_table_with_header(cls, table):
        res = ValueTableWithHeader()
        res.header = TableRow(cells_content=table.header.cells_content)
        for row in table:
            cells_content = [f"'{cc}'" if isinstance(cc, str) else f"{cc}" for cc in row.cells_content]
            res.add_row(cells_content=cells_content)
        return res
        
    @classmethod
    def convert_table_with_header_to_dict_list(cls, table):
        index_by_name = table.get_column_indexes_by_string_content()

        res = []
        for row in table.rows:
            new_dict = {name: row.get_cell(index).value for name, index in index_by_name.items()}
            res.append(new_dict)
            
        return res
    
        