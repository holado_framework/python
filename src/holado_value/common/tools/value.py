
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import str
from holado_core.common.exceptions.technical_exception import TechnicalException
from holado_core.common.tools.tools import Tools
from holado.common.context.session_context import SessionContext
import logging
from holado_value.common.tools.value_types import ValueTypes
from holado_scripting.common.tools.expression_evaluator import ExpressionEvaluator
from functools import total_ordering

logger = logging.getLogger(__name__)


@total_ordering
class Value(object):
    
    def __init__(self, original_value):
        """
        @summary: Constructor
        @param original_value: Original value - can be a string or any object
        """
        self.__value_original = original_value
        self.__type = None;
        self.__value = None;
        self.__value_to_eval = None;
        
        self.__extract_value_information()
        if logger.isEnabledFor(logging.TRACE):  # @UndefinedVariable
            logger.trace(f"New Value: value_original=[{self.__value_original}] ; type=[{self.value_type.name}] ; value=[{self.__value}] ; value_to_eval=[{self.__value_to_eval}]")
        
    def _verify_valid_compared_object(self, other, raise_exception=True):
        res = isinstance(other, Value)
        if not res and raise_exception:
            return TechnicalException(f"Unmanaged comparison between types '{self.__class__.__name__}' and '{other.__class__.__name__}'")
        
    def __eq__(self, other):
        self._verify_valid_compared_object(other)
        if isinstance(other, Value):
            return (self.value == other.value)
        else:
            return TechnicalException(f"Unmanaged comparison between types '{self.__class__.__name__}' and '{other.__class__.__name__}'")
    
    def __lt__(self, other):
        self._verify_valid_compared_object(other)
        if isinstance(other, Value):
            return (self.value < other.value)
        else:
            return TechnicalException(f"Unmanaged comparison between types '{self.__class__.__name__}' and '{other.__class__.__name__}'")

    @property
    def string_value(self):
        value = self.value
        
        if value is None:
            return ExpressionEvaluator.NONE_SYMBOL
        else:
            return str(value)
    
    @property
    def value_original(self):
        """
        @summary: Return the original value
        """
        return self.__value_original
    
    @property
    def value_type(self):
        return self.__type
    
    @property
    def value_before_eval(self):
        """
        @summary: Return the value before evaluation
        """
        return self.__value_to_eval
    
    @property
    def value(self):
        """
        @summary: Return the value, after evaluation if needed
        """
        if self.__value_to_eval is not None:
            return self.__get_expression_evaluator().evaluate_expression_of_information(self.__type, self.__value_to_eval)
        else:
            return self.__value
    
    def represent(self, indent = 0, do_evaluation = False):
        res_list = []
        
        res_list.append(Tools.get_indent_string(indent))
        
        if self.__type in [ValueTypes.String, ValueTypes.DynamicString, ValueTypes.UniqueString]:
            if do_evaluation:
                res_list.append("'")
                res_list.append(self.string_value)
                res_list.append("'")
            else:
                res_list.append(str(self.value_original))
        elif self.__type in [ValueTypes.Boolean, ValueTypes.Integer, ValueTypes.Float, ValueTypes.Null, ValueTypes.NotApplicable, ValueTypes.Symbol, ValueTypes.Generic]:
            if do_evaluation:
                res_list.append(self.string_value)
            else:
                res_list.append(str(self.value_original))
        else:
            raise TechnicalException("Unmanaged value type '{}'".format(self.__type.name))
        
        return "".join(res_list)
        
    def __extract_value_information(self):
        self.__type, self.__value, self.__value_to_eval = self.__get_expression_evaluator().extract_expression_information(self.value_original)
            
    def __get_scenario_context(self):
        return SessionContext.instance().get_scenario_context()

    def __get_expression_evaluator(self):
        return self.__get_scenario_context().get_expression_evaluator()     
            
