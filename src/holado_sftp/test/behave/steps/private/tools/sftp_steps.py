# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
import os.path
import logging

logger = logging.getLogger(__name__)


def __get_session_context():
    return SessionContext.instance()

def __get_scenario_context():
    return __get_session_context().get_scenario_context()

def __get_text_interpreter():
    return __get_scenario_context().get_text_interpreter()

def __get_variable_manager():
    return __get_scenario_context().get_variable_manager()

def __get_key_path():
    here = os.path.abspath(os.path.dirname(__file__))
    resources_path = os.path.normpath(os.path.join(here, "..", "..", "..", "..", "..", "..", "..", "resources"))
    return os.path.join(resources_path, "ssh", "id_rsa")

@Given(u"(?P<var_name>\w+) = start internal sFTP server")
def step_impl(context, var_name):
    root_path = __get_session_context().report_manager.current_scenario_report.get_path("sftpserver")
    key_path = __get_key_path()
    
    execute_steps(u"""
        Given {var_name} = new sFTP server
            | Name        | Value                  |
            | 'name'      | 'internal sFTP server' |
            | 'root_path' | '{root_path}'          |
            | 'host'      | 'localhost'            |
            | 'port'      | 8022                   |
            | 'level'     | 'INFO'                 |
            | 'keyfile'   | '{keyfile}'            |
        When start sFTP server {var_name}
        """.format(var_name=var_name, root_path=root_path, keyfile=key_path) )
    
@Given(u"(?P<var_name>\w+) = new internal sFTP client")
def step_impl(context, var_name):
    client_path = __get_session_context().report_manager.current_scenario_report.get_path("sftpclient")
    log_path = __get_session_context().report_manager.current_scenario_report.get_path("logs/sftpclient.log")
    key_path = __get_key_path()

    __get_session_context().path_manager.makedirs(client_path, is_directory = True)

    execute_steps(u"""
        Given {var_name} = new sFTP client
            | Name              | Value           |
            | 'host'            | 'localhost'     |
            | 'port'            | 8022            |
            | 'private_key'     | '{private_key}' |
            | 'cnopts.hostkeys' | None            |
            | 'cnopts.log'      | '{log_path}'    |
            | 'local_path'      | '{client_path}' |
        """.format(var_name=var_name, private_key=key_path, log_path=log_path, client_path=client_path) )
