# -*- coding: utf-8 -*-

#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################


from holado_test.scenario.step_tools import StepTools
from holado.common.context.session_context import SessionContext
from holado_test.behave.behave import *
from holado_value.common.tables.value_table_manager import ValueTableManager
from holado_core.common.exceptions.functional_exception import FunctionalException
from holado_sftp.tools.sftp.sftp_client import SFTPClient
from holado_core.common.exceptions.technical_exception import TechnicalException
import logging
from holado_test.behave.scenario.behave_step_tools import BehaveStepTools

logger = logging.getLogger(__name__)


if SFTPClient.is_available():

    def __get_session_context():
        return SessionContext.instance()
    
    def __get_scenario_context():
        return __get_session_context().get_scenario_context()
    
    def __get_variable_manager():
        return __get_scenario_context().get_variable_manager()
    
        
    @Given(u"(?P<var_name>\w+) = new sFTP client")
    def step_impl(context, var_name):
        table = BehaveStepTools.convert_step_table_2_value_table_with_header(context.table)
        kwargs = ValueTableManager.convert_name_value_table_2_dict(table)
        
        try:
            client = SFTPClient()
            client.connect(**kwargs)
        except Exception as exc:
            raise FunctionalException(str(exc)) from exc

        __get_variable_manager().register_variable(var_name, client)
        
    @Step(u"execute \[(?P<command>.+)\] \(sFTP client: (?P<var_client>\w+)\)")
    def step_impl(context, command, var_client):
        """
        Execute any method in class pysftp.Connection (cf https://pysftp.readthedocs.io/en/release_0.2.9/pysftp.html).
        An exception is raised if the method returns a result.
        """
        # command = StepTools.evaluate_scenario_parameter(command)
        command = StepTools.evaluate_string_parameter(command)
        client = __get_variable_manager().get_variable_value(var_client)  # @UnusedVariable
        
        result = eval("client.connection." + command)
        if result:
            raise TechnicalException(f"Unexpected result to command [{command}]: {result}")
        
    @Step(u"(?P<var_name>\w+) = result of \[(?P<command>.+)\] \(sFTP client: (?P<var_client>\w+)\)")
    def step_impl(context, var_name, command, var_client):
        """
        Execute any method in class pysftp.Connection (cf https://pysftp.readthedocs.io/en/release_0.2.9/pysftp.html).
        The result is stored in given variable.
        """
        # command = StepTools.evaluate_scenario_parameter(command)
        command = StepTools.evaluate_string_parameter(command)
        client = __get_variable_manager().get_variable_value(var_client)  # @UnusedVariable
        
        result = eval("client.connection." + command)
        
        __get_variable_manager().register_variable(var_name, result)
        


    
