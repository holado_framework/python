
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import object
import logging
from holado.common.handlers.enums import ObjectState
from holado.common.tools.gc_manager import GcManager

logger = None

def initialize_logger():
    global logger
    logger = logging.getLogger(__name__)


class Object(object):
    def __init__(self, name=None):
        self.__name = name
        self.__obj_state = ObjectState.Created

    @property
    def name(self):
        if self.__name:
            return self.__name
        else:
            return str(self)

    @property
    def object_state(self):
        return self.__obj_state
    
    @object_state.setter
    def object_state(self, state):
        logger.trace(f"State changed for object '{self.name}' (type: {type(self)}): {self.__obj_state} -> {state}")
        self.__obj_state = state


class DeleteableObject(Object):
    """
    Add to a class deleting features:the possibility to be deleted
        - Self deleting actions
        - Register functions to call on delete
    """
    
    def __init__(self, name=None):
        super().__init__(name)
        
        self.__on_delete_funcs = []
        self.__on_delete_gc_collect = False
    
    def __del__(self):
        try:
            self.delete_object()
        except Exception as exc:
            if "Python is likely shutting down" in str(exc):
                # Simply return
                return
            else:
                raise exc
        
    @property
    def on_delete_call_gc_collect(self):
        return self.__on_delete_gc_collect
        
    @on_delete_call_gc_collect.setter
    def on_delete_call_gc_collect(self, do_call):
        self.__on_delete_gc_collect = do_call
        
    def on_delete_call_func(self, func):
        self.__on_delete_funcs.append(func)
        
    def delete_object(self):
        if self.object_state in [ObjectState.Deleting, ObjectState.Deleted]:
            return False
        
        logger.debug(f"Deleting object '{self.name}' (type: {type(self)})...")
        self.object_state = ObjectState.Deleting
        
        # Run on_delete functions
        for func in self.__on_delete_funcs:
            try:
                func()
            except:
                logger.exception(f"[{self.name}] Error while running on delete function '{func}'")
                
        self._delete_object()
        
        # Call garbage collector
        if self.__on_delete_gc_collect:
            GcManager.collect()
        
        self.object_state = ObjectState.Deleted
        logger.debug(f"Deleted object '{self.name}' (type: {type(self)})")
        
        return True

    def _delete_object(self):
        """
        Implement this method for specific delete actions
        """
        pass
    
    
    
