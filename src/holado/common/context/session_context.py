
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

from builtins import super
from _datetime import datetime
import logging
import os.path
from holado.common.context.context import Context


logger = logging

def initialize_logger():
    global logger
    logger = logging.getLogger(__name__)


class SessionContext(Context):
    TSessionContext = None
    
    # Singleton management
    __instance = None
    
    @staticmethod
    def instance() -> TSessionContext:
        if SessionContext.__instance is None:
            SessionContext.__instance = SessionContext.TSessionContext()
            # print(f"Created session context of type {SessionContext.TSessionContext}")
            # import traceback
            # print("".join(traceback.format_list(traceback.extract_stack())))
        return SessionContext.__instance
    
    
    def __init__(self, name="Session"):
        super().__init__(name)
        
        self.__with_session_path = None
        
        from holado.common.context.service_manager import ServiceManager
        self.__service_manager = ServiceManager(self)
        
    def configure(self, session_kwargs=None):
        """
        Override this method to configure the session context before new session creation.
        It is usually used to register new services.
        """
        if session_kwargs is None:
            session_kwargs = {}
            
        self.__with_session_path = session_kwargs.get("with_session_path", True)
    
    def initialize(self, session_kwargs=None):
        """
        Override this method to initialize the session context after its configuration and new session creation.
        """
        pass
        
    @property
    def behave_context(self):
        return self.get_behave_context(raise_exception=True)

    @property
    def services(self):
        return self.__service_manager
    
    @property
    def with_session_path(self):
        return self.__with_session_path

    def new_session(self, session_kwargs=None):
        # Report session
        report_path = None
        if self.with_session_path:
            # Create new report path for this session
            name = "session_{}".format(datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))
            report_path = self.path_manager.get_reports_path(name)
            logger.info(f"Reports location: {report_path}")
            # print(f"Reports location: {report_path}")
        self.report_manager.new_session(report_path)
            
        # Logging configuration
        if self.with_session_path and SessionContext.instance().log_manager.in_file:
            log_filename = os.path.join(report_path, "logs", "report.log")
            self.path_manager.makedirs(log_filename)
            SessionContext.instance().log_manager.on_console = False
            SessionContext.instance().log_manager.set_root_log_file(log_filename)
            SessionContext.instance().log_manager.set_config()
    
    def before_all(self, behave_context):
        self.set_object("behave_context", behave_context, raise_if_already_set=False)
        
        self.report_manager.before_all()
    
    def after_all(self):
        self.report_manager.after_all()

    def get_behave_context(self, raise_exception=True):
        if not self.has_object("behave_context"):
            if raise_exception:
                from holado_core.common.exceptions.technical_exception import TechnicalException
                raise TechnicalException("Behave context is not defined")
            else:
                return None
        return self.get_object("behave_context")

    def before_feature(self, feature):
        logger.debug("Before feature [{}]".format(feature.name))
        if self.has_feature_context():
            from holado_core.common.exceptions.technical_exception import TechnicalException
            raise TechnicalException("A feature context is already defined")
        
        # Feature context
        from holado_test.common.context.feature_context import FeatureContext
        self.set_object("feature_context", FeatureContext(feature))
        
        # Report
        try:
            self.report_manager.before_feature(feature)
        except:
            logger.exception("Error while updating report before feature")
        
        # Logs
        logger.info("="*150)
        logger.info("Feature [{}]".format(feature.name))
        
    def after_feature(self, feature):
        if not self.has_feature_context():
            from holado_core.common.exceptions.technical_exception import TechnicalException
            raise TechnicalException("No feature context is defined")

        logger.debug("After feature [{}]".format(feature.name))
        
        # End feature context
        self.get_feature_context().end()
        
        # Report
        try:
            self.report_manager.after_feature(feature)
        except:
            logger.exception("Error while updating report after feature")
        
        logger.debug("Deleting context of feature [{}]".format(feature.name))
        self.__delete_feature_context()
        
        logger.debug("Finished feature [{}]".format(feature.name))
        
    def has_feature_context(self):
        return self.has_object("feature_context")
    
    def get_feature_context(self):
        return self.get_object("feature_context")
    
    def __delete_feature_context(self):
        self.get_feature_context().delete_object()
        self.remove_object("feature_context")
    
    def before_scenario(self, scenario):
        from holado_test.common.context.scenario_context import ScenarioContext
        
        logger.debug("Before scenario [{}]".format(scenario.name))
        if not self.has_feature_context():
            from holado_core.common.exceptions.technical_exception import TechnicalException
            raise TechnicalException("No feature context is defined")

        scenario_context = ScenarioContext(scenario)
        self.get_feature_context().add_scenario(scenario_context)
        
        # Set variable with scenario context instance
        self.get_scenario_context().get_variable_manager().register_variable("SCENARIO_CONTEXT", self.get_scenario_context())
        
        # Report
        try:
            self.report_manager.before_scenario(scenario)
        except:
            logger.exception("Error while updating report before scenario")

        logger.info("-"*150)
        logger.info("Scenario [{}]".format(scenario.name))

        
    def after_scenario(self, scenario):
        from holado_core.common.exceptions.technical_exception import TechnicalException
        
        logger.debug("After scenario [{}]".format(scenario.name))
        if not self.has_feature_context():
            raise TechnicalException("No feature context is defined")
        if not self.has_scenario_context():
            raise TechnicalException("No scenario context is defined")

        self.get_scenario_context().end()
            
        # Post processes
        logger.debug("Post processing for scenario [{}]".format(scenario.name))
        self.get_scenario_context().scope_manager.reset_scope_level()
        self.get_scenario_context().do_post_processes()
        
        # Report
        try:
            self.report_manager.after_scenario(scenario)
        except:
            logger.exception("Error while updating report after scenario")
        
        # Delete scenario context
        logger.debug("Deleting context of scenario [{}]".format(scenario.name))
        self.__delete_scenario_context()
        
        # Remove all threads
        logger.debug("Interrupting and unregistering all threads for scenario [{}]".format(scenario.name))
        from holado_core.common.threading.threadsmanager import ThreadsManager
        ThreadsManager.interrupt_all_threads()
        ThreadsManager.unregister_all_threads(keep_alive=False)
        
        logger.debug("Finished scenario [{}]".format(scenario.name))
        
    def has_scenario_context(self):
        return self.has_feature_context() and self.get_feature_context().has_scenario
    
    def get_scenario_context(self):
        return self.get_feature_context().current_scenario
    
    def __delete_scenario_context(self):
        self.get_scenario_context().delete_object()
        
    def before_step(self, step):
        from holado_test.common.context.step_context import StepContext
        from holado_core.common.exceptions.technical_exception import TechnicalException

        if not self.has_feature_context():
            raise TechnicalException("No feature context is defined")
        if not self.has_scenario_context():
            raise TechnicalException("No scenario context is defined")

        # Manage step context
        step_context = StepContext(step)
        self.get_scenario_context().scope_manager.set_step_context(step_context)
        
        # Update scenario context
        step_level = self.get_scenario_context().scope_manager.scope_level("steps")
        if step_level == 0:
            self.get_scenario_context().add_step(step_context)
        
        # Manage scope in define
        if self.get_scenario_context().block_manager.is_in_define \
                and (self.get_scenario_context().block_manager.is_in_sub_define         # needed to add in define the "end define" steps of sub-define \
                     or step.name not in ["end for", "end while", "end function"]):     # needed to not add in define the "end define" steps of current define
            from holado_test.behave.behave import format_step
            step_str = format_step(step)
            self.get_scenario_context().block_manager.scope_in_define.add_steps(step_str)
            
            # Set step status
            step_context.status = "defined"
        
        # Report
        try:
            self.report_manager.before_step(step_context, step, step_level)
        except:
            logger.exception("Error while updating report before step")
    
    def after_step(self, step):
        from holado_core.common.exceptions.technical_exception import TechnicalException
        
        if not self.has_feature_context():
            raise TechnicalException("No feature context is defined")
        if not self.has_scenario_context():
            raise TechnicalException("No scenario context is defined")

        # Manage step context
        step_context = self.get_scenario_context().scope_manager.get_step_context()
        step_context.end()
        if step_context.status is None:
            step_context.status = step.status.name
        
        # Report
        try:
            step_level = self.get_scenario_context().scope_manager.scope_level("steps")
            self.report_manager.after_step(step_context, step, step_level)
        except:
            logger.exception("Error while updating report after step")
    
    def has_step_context(self):
        return self.has_feature_context() and self.get_feature_context().has_scenario and self.get_scenario_context().has_step
    
    def get_step_context(self):
        return self.get_scenario_context().current_step
        



SessionContext.TSessionContext = SessionContext
