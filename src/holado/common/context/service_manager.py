
#################################################
# HolAdo (Holistic Automation do)
#
# (C) Copyright 2021-2023 by Eric Klumpp
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#################################################

import logging
from builtins import object

logger = None

def initialize_logger():
    global logger
    logger = logging.getLogger(__name__)


class ServiceManager(object):
    """
    Manage services of a context
    """
    
    def __init__(self, context):
        self.__context = context
        self.__services = {}
        
    def register_service(self, name, type_service=None, initialize_service=None, service_instance=None, override=False, raise_if_exist=True, raise_if_instantiated=True):
        """
        Register a new service.
        
        A property with service name is automatically added to context.
        When the property is called the first time, the service is instantiated by calling "m = type_service()".
        After instantiation, it will be initialized by calling "initialize_service(m)".
        Initialization is typically used to inject dependencies, and process initialize actions needing these dependencies.
        
        Notes:
            It is possible to override an already registered service by passing "override=True".
            Use lambda to define "type_service" when service "__init__" method has arguments.
            Use lambda to define "initialize_service" when service "initialize" method has arguments.
            It is possible to replace an existing property that is not a service by service property by passing "raise_if_exist=False".
            It is also possible to register directly an instance of the service with parameter "service_instance"
        """
        from holado_core.common.exceptions.technical_exception import TechnicalException
        logger_ = logger if logger else logging
        
        logger_.trace(f"Registering service '{name}'...")
        
        if not override and name in self.__services:
            raise TechnicalException(f"A service '{name}' is already registered")
        if raise_if_exist and hasattr(self.__context, name) and name not in self.__services:
            raise TechnicalException(f"Context contains already an attribute '{name}'")
        if not override and raise_if_instantiated and self.__context.has_object(name):
            raise TechnicalException(f"The service '{name}' is already instantiated and initialized")
        
        # Register the service
        if self.__context.has_object(name):
            self.__context.remove_object(name)
        if service_instance:
            self.__context.set_object(name, service_instance)
        elif type_service:
            self.__services[name] = (type_service, initialize_service)
        else:
            raise TechnicalException(f"To register service '{name}', at least 'type_service' or 'service_instance' must be defined")
        
        # Add dedicated property in context
        @property
        def service_property(_):
            return self.get_service(name)
        setattr(self.__context.__class__, name, service_property)
        setattr(self.__class__, name, service_property)
        
        logger_.debug(f"Registered service '{name}'")
        # print(f"Registered service '{name}'")
    
    def get_service(self, name):
        """
        Get a service instance.
        If not already created, the instance is created according its registration and stored in context.
        """
        from holado_core.common.exceptions.technical_exception import TechnicalException
        logger_ = logger if logger else logging
        
        logger_.trace(f"Getting service '{name}'...")
        if not self.__context.has_object(name):
            logger_.debug(f"Creating service '{name}'...")
            if name in self.__services:
                service = self.__services[name]
            else:
                raise TechnicalException(f"Unregistered service '{name}'")
            
            logger_.trace(f"Instantiating service '{name}'...")
            service_inst = service[0]()
            self.__context.set_object(name, service_inst)
            logger_.debug(f"Service '{name}' is instantiated and stored in context")
            
            if service[1] is not None:
                logger_.trace(f"Initializing service '{name}'...")
                service[1](service_inst)
            logger_.debug(f"Created service '{name}'")
        return self.__context.get_object(name)
    


